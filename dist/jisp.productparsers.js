var Jisp;
(function (Jisp) {
    'use strict';

    function init(parser) {
        parser.url = Jisp.getAttribute('link[rel="canonical"]', 'href');
        if (!parser.url) {
            parser.url = document.location.href;
        } else {
            if ((parser.url.indexOf('/') === 0) && (parser.url.indexOf('//') !== 0)) {
                parser.url = document.location.protocol + '//' + document.location.hostname + parser.url;
            }

            if (parser.url.indexOf('//') === 0) {
                parser.url = document.location.protocol + '//' + parser.url.substring(2);
            }
        }
        parser.title = document.title;

        var product = Jisp.getProductElement();
        parser.isProductPage = (product) ? true : false;

        parser.except = false;
        parser.imageUrl = null;
        parser.placeholderImageUrl = null;
        parser.priceAmount = null;
        parser.priceCurrency = null;
        parser.brandNameText = null;
        parser.categoryNameText = null;
        parser.productName = null;
    }
    Jisp.init = init;

    function getResult(parser) {
        var except = Jisp.getExcept(parser);
        return {
            except: except, // don't delete for backward compatibility
            productType: (except) ? 2 : 1,
            url: parser.url,
            title: (parser.productName) ? parser.productName : parser.title,
            imageUrl: (parser.imageUrl) ? parser.imageUrl : null,
            placeholderImageUrl: parser.placeholderImageUrl,
            price: (!except) ? (((parser.priceCurrency == '£') || (parser.priceCurrency == 'GBP')) ? parser.priceAmount : null) : null,
            currency: (!except) ? (((parser.priceCurrency == '£') || (parser.priceCurrency == 'GBP')) ? '£' : null) : null,
            websiteBrand: parser.brandNameText,
            websiteCategory: parser.categoryNameText
        };
    }
    Jisp.getResult = getResult;

    function getProductElement() {
        return document.querySelector('[itemtype="http://schema.org/Product"], [itemtype="https://schema.org/Product"]');
    }
    Jisp.getProductElement = getProductElement;

    function isExistsElement(selector) {
        var element = document.querySelector(selector);
        return (element) ? true : false;
    }
    Jisp.isExistsElement = isExistsElement;

    function getProductName(product) {
        var productName = null;

        var productElement = $('[itemtype="http://schema.org/Product"] [itemprop="name"]:not(:empty), [itemtype="https://schema.org/Product"] [itemprop="name"]:not(:empty)');
        if (productElement){
            productElement = productElement.first()[0];

            if (productElement) {
                productName = productElement.innerText;
            }
        }

        if (!productName) {
            productElement = $('[itemtype="http://schema.org/Product"] [itemprop="name"], [itemtype="https://schema.org/Product"] [itemprop="name"]');
            if (productElement) {
                productElement = productElement.first()[0];
            }

            if (productElement) {
                productName = productElement.getAttribute('content');
            }
        }

        if (!productName) {
            productName = Jisp.getMetaProperty(document, "og:title");
        }

        return (productName) ? productName.trim() : null;
    }
    Jisp.getProductName = getProductName;

    function getImage(product) {
        var imageUrl = Jisp.getMetaName(document, "twitter:image");

        if (!imageUrl) {
            imageUrl = Jisp.getMetaProperty(document, "og:image");
        }

        if (!imageUrl) {
            imageUrl = Jisp.getMetaName(document, "og:image");
        }

        if (!imageUrl) {
            var image = $('[itemprop="image"][src]:not([src=""])');
            if (image) {
                image = image.first()[0];

                if (image) {
                    imageUrl = image.getAttribute('src');
                }
            }
        }

        if (imageUrl) {
            imageUrl = imageUrl.replace("http:///https://", "https://");

            if ((imageUrl.indexOf('/') === 0) && (imageUrl.indexOf('//') !== 0)) {
                imageUrl = document.location.protocol + '//' + document.location.hostname + imageUrl;
            }

            if (imageUrl.indexOf('//') === 0) {
                imageUrl = document.location.protocol + '//' + imageUrl.substring(2);
            }
        }

        return (imageUrl) ? imageUrl.replace("http:///https://", "https://") : null;
    }
    Jisp.getImage = getImage;

    function getPrice(product) {
        var priceText = null;

        var priceElement = $('[itemtype="http://schema.org/Product"] [itemprop="price"]:not(:empty), [itemtype="https://schema.org/Product"] [itemprop="price"]:not(:empty)');
        if (priceElement) {
            priceElement = priceElement.first()[0];

            if (priceElement) {
                priceText = priceElement.innerText;
            }
        }

        if (!priceText) {
            priceText = Jisp.getMetaProperty(document, "og:price:amount");
        }

        if (!priceText) {
            priceText = Jisp.getMetaProperty(document, "product:price:amount");
        }

        if (!priceText) {
            priceText = Jisp.getMetaName(document, "product:price:amount");
        }

        if (!priceText) {
            priceText = Jisp.getMeta(document, 'itemprop="price"');
        }
        return (priceText) ? priceText.trim() : null;
    }
    Jisp.getPrice = getPrice;

    function getCurrency(product) {
        var currencyText = Jisp.getMeta(document, 'itemprop="priceCurrency"');

        if (!currencyText) {
            currencyText = Jisp.getMetaProperty(document, "og:price:currency");
        }

        if (!currencyText) {
            currencyText = Jisp.getMetaName(document, "product:price:currency");
        }

        if (!currencyText) {
            currencyText = Jisp.getAttribute('[itemprop="priceCurrency"]', 'content');
        }
        return (currencyText) ? currencyText.trim() : null;
    }
    Jisp.getCurrency = getCurrency;

    function extractPrice(priceInnerText) {
        if (priceInnerText) {
            var priceValue = priceInnerText.replace('£', '').replace(',', '').trim();
            var priceParsed = parseFloat(priceValue);
            if (!isNaN(priceParsed)) {
                return priceParsed;
            }
        }
        return null;
    }
    Jisp.extractPrice = extractPrice;

    function extractCurrency(priceInnerText) {
        if (priceInnerText) {
            if ((priceInnerText.indexOf('£') >= 0) || (priceInnerText.indexOf('GBP') >= 0)) {
                return '£';
            }
        }
        return null;
    }
    Jisp.extractCurrency = extractCurrency;

    function getBrand(product) {
        var brand = null;

        var brandElement = $('[itemtype="http://schema.org/Product"] [itemprop="brand"] [itemprop="name"]:not(:empty), [itemtype="https://schema.org/Product"] [itemprop="brand"] [itemprop="name"]:not(:empty)');
        if (brandElement) {
            brandElement = brandElement.first()[0];

            if (brandElement) {
                brand = brandElement.innerText;
            }
        }
        
        if (!brand) {
            var brandElementNotProd = $('[itemprop="brand"] [itemprop="name"]:not(:empty)');
            if (brandElementNotProd) {
                brandElementNotProd = brandElementNotProd.first()[0];

                if (brandElementNotProd) {
                    brand = brandElementNotProd.innerText;
                }
            }
        }

        if (!brand) {
            var brandElementAttr = $('[itemprop="brand"] [itemprop="name"]');
            if (brandElementAttr) {
                brandElementAttr = brandElementAttr.first()[0];

                if (brandElementAttr) {
                    brand = brandElementAttr.getAttribute('content');
                }
            }
        }

        if (!brand) {
            brand = Jisp.getInnerText('[itemprop="brand"]');
        }

        if (!brand) {
            brand = Jisp.getMetaProperty(document, "brand");
        }
        return (brand) ? brand.trim() : null;
    }
    Jisp.getBrand = getBrand;

    function getBreadCrumbs(product) {

        var breadCrumbAll = Jisp.getInnerTextArr('[itemprop="breadcrumb"] a', ['Home']);
        if ((!breadCrumbAll) || (breadCrumbAll.length == 0)) {
            breadCrumbAll = Jisp.getInnerTextArr('.breadcrumb a', ['Home']);
        }

        if ((!breadCrumbAll) || (breadCrumbAll.length == 0)) {
            breadCrumbAll = Jisp.getInnerTextArr('#breadcrumb a', ['Home']);
        }

        if ((!breadCrumbAll) || (breadCrumbAll.length == 0)) {
            breadCrumbAll = Jisp.getInnerTextArr('[itemtype="http://data-vocabulary.org/Breadcrumb"] a, [itemtype="https://data-vocabulary.org/Breadcrumb"] a', ['Home']);
        }
        return breadCrumbAll;
    }
    Jisp.getBreadCrumbs = getBreadCrumbs;

    function extractCategoryName(breadCrumbs) {
        if (breadCrumbs) {
            return breadCrumbs.join(' > ');
        }
        else {
            return null;
        }
    }
    Jisp.extractCategoryName = extractCategoryName;

    function getExcept(parser) {
        //return parser.except || !((parser.priceCurrency == '£') || (parser.priceCurrency == 'GBP')) || (parser.priceAmount == null);
        return parser.except || (!parser.isProductPage);
    }
    Jisp.getExcept = getExcept;

    function getMeta(document, selector) {
        var metas = $('meta[' + selector + ']', document);
        if (metas) {
            for (var i = 0; i < metas.length; i++) {
                var metaValue = metas[i].getAttribute('content');
                if (metaValue) {
                    return metaValue.trim();
                }
            }
        }
        return null;
    }
    Jisp.getMeta = getMeta;

    function getMetaProperty(document, property) {
        return getMeta(document, 'property="' + property + '"');
    }
    Jisp.getMetaProperty = getMetaProperty;

    function getMetaName(document, name) {
        return getMeta(document, 'name="' + name + '"');
    }
    Jisp.getMetaName = getMetaName;

    function getInnerText(selector) {
        var elements = $(selector);
        if (elements) {
            for (var i = 0; i < elements.length; i++) {
                var elementInnerText = elements[i].innerText;
                if (elementInnerText) {
                    return elementInnerText.trim();
                }
            }
        }
        return null;
    }
    Jisp.getInnerText = getInnerText;

    function getInnerTextArr(selector, filter) {
        var elementsAll = [];
        var elements = $(selector);
        if (elements) {
            for (var i = 0; i < elements.length; i++) {
                var elementInnerText = elements[i].innerText;
                if (elementInnerText) {
                    elementsAll.push(elementInnerText.trim());
                }
            }
        }

        if (filter) {
            filter = filter.filter(function (name) {
                return (name);
            }).map(function (name) {
                return name.toUpperCase();
            });

            elementsAll = elementsAll.filter(function (name) {
                return filter.indexOf(name.toUpperCase()) == -1;
            });
        }

        return elementsAll;
    }
    Jisp.getInnerTextArr = getInnerTextArr;

    function getAttribute(selector, attr) {
        var elements = $(selector);
        if (elements) {
            for (var i = 0; i < elements.length; i++) {
                var attrValue = elements[i].getAttribute(attr);
                if (attrValue) {
                    return attrValue.trim();
                }
            }
        }
        return null;
    }
    Jisp.getAttribute = getAttribute;

    function ignoreSharpUrl(url) {
        if (url) {
            var sharpIndex = url.indexOf('#');
            if (sharpIndex > 0) {
                return url.substring(0, sharpIndex);
            }
        }
        return url;
    }
    Jisp.ignoreSharpUrl = ignoreSharpUrl;

    function ignoreQueryUrl(url) {
        if (url) {
            var queryIndex = url.indexOf('?');
            if (queryIndex > 0) {
                return url.substring(0, queryIndex);
            }
        }
        return url;
    }
    Jisp.ignoreQueryUrl = ignoreQueryUrl;

    function addProtocolToUrl(url) {
        if ((url) && (url.indexOf("//") == 0)) {
            url = document.location.protocol + url;
        }
        return url;
    }
    Jisp.addProtocolToUrl = addProtocolToUrl;
})(Jisp || (Jisp = {}));
window.parser_amazon_co_uk = new (function () {
    Jisp.init(this);

    this.placeholderImageUrl = "https://jisp.com/Content/img/placeholders/amazon.co.uk.png";

    this.ready = function () {
        var image = this.getImage(document);
        if (image) {
            return (image.trim().indexOf('data:') != 0);
        }
        else {
            return false;
        }
    };

    this.checkChanges = function () {
        return null;
    };

    this.getProductName = function(product) {
        var productNames = $('#productTitle', product);

        if (productNames.length == 0) {
            productNames = $('#title_feature_div', product);
        }

        if (productNames.length == 0) {
            productNames = $('#btAsinTitle', product);
        }

        if (this.isProductPage) {
            if (productNames.length == 0) {
                productNames = $('h1', product);
            }
        }

        for (var i = 0; i < productNames.length; i++) {
            var productNameTextFinded = productNames[i].innerText;
            if (productNameTextFinded) {
                return productNameTextFinded;
            }
        }

        return null;
    };

    this.getImage = function(product) {

        var images = $('.image.selected img', product);

        if (images.length == 0) {
            images = $('#landingImage', product);
        }

        if (images.length == 0) {
            images = $('#main-image', product);
        }

        if (images.length == 0) {
            //http://www.amazon.co.uk/gp/product/111894691X/ref=s9_hps_bw_g14_i4?pf_rd_m=A3P5ROKL5A1OLE&pf_rd_s=merchandised-search-4&pf_rd_r=0X44GYJ0NMWHJJNWGC4Y&pf_rd_t=101&pf_rd_p=823142447&pf_rd_i=266239
            images = $('#imgBlkFront', product);
        }

        if (images.length == 0) {
            images = $('#ppd-left img', product);
        }

        if (images.length == 0) {
            images = $('#js-masrw-main-image', product);
        }

        //gift cards pages https://www.amazon.co.uk/Thank-You-Teacher-Printable-Amazon-co-uk/dp/B00DW8FYI0/ref=sr_1_27?s=gift-cards&ie=UTF8&qid=1469716559&sr=1-27
        if (images.length == 0) {
            images = $('#gc-live-preview-Designs img', product);
        }

        //ebook pages https://www.amazon.co.uk/Secret-1-Bestselling-Author-ebook/dp/B01D57H430/ref=tmm_kin_swatch_0?_encoding=UTF8&qid=&sr=
        if (images.length == 0) {
            images = $('#ebooks-img-canvas img', product);
        }

        //https://www.amazon.co.uk/dp/B00ESJDS62/ref=twister_B00ESJDR9A?_encoding=UTF8&psc=1
        if (images.length == 0) {
            images = $('#zeroes-coin-image', product);
        }

        //https://www.amazon.co.uk/Hateful-Eight-Samuel-L-Jackson/dp/B01E0AI0RU
        if (images.length == 0) {
            images = $('.dp-img-bracket img', product);
        }

        //if (Array.isArray(images)) {
            for (var i = 0; i < images.length; i++) {
                var findedImageUrl = images[i].getAttribute('src');
                if (findedImageUrl) {
                    return findedImageUrl;
                }
            }
        //} else {
        //    if (images) {
        //        if (typeof images['getAttribute'] === 'function') {
        //            var oneImageUrl = images.getAttribute('src');
        //            if (oneImageUrl) {
        //                return oneImageUrl;
        //            }
        //        }

        //        //var oneImageUrl = images.getAttribute('src');
        //        //if (oneImageUrl) {
        //        //    return oneImageUrl;
        //        //}
        //    }
        //}

        return null;
    };

    this.getPrice = function(product) {
        var prices = $('#priceblock_dealprice', product);

        if (prices.length == 0) {
            prices = $('#priceblock_ourprice', product);
        }

        if (prices.length == 0) {
            prices = $('#priceblock_saleprice', product);
        }
        if (prices.length == 0) {
            prices = $('#actualPriceValue', product);

            for (var i = 0; i < prices.length; i++) {
                var priceTextFinded = prices[i].innerText;
                if (priceTextFinded) {
                    return priceTextFinded;
                }
            }
        }
        if (prices.length == 0) {
            prices = $('.a-color-price', $('[data-feature-name="olp"]', product));
        }

        if (prices.length == 0) {
            //http://www.amazon.co.uk/gp/product/111894691X/ref=s9_hps_bw_g14_i4?pf_rd_m=A3P5ROKL5A1OLE&pf_rd_s=merchandised-search-4&pf_rd_r=0X44GYJ0NMWHJJNWGC4Y&pf_rd_t=101&pf_rd_p=823142447&pf_rd_i=266239
            prices = $('.a-color-price', $('#buybox', product));

            for (var i = 0; i < prices.length; i++) {
                var priceTextFinded = prices[i].innerText;
                if (priceTextFinded) {
                    return priceTextFinded.replace(/<p.*>[\w\W]{1,}(.*?)[\w\W]{1,}<\/p>/gi, "");
                }
            }
        }

        if (prices.length == 0) {
            //https://www.amazon.co.uk/Samsung-Galaxy-32GB-SIM-Free-Smartphone/dp/B01C5OIIF2/ref=sr_1_7?s=telephone&ie=UTF8&qid=1473068615&sr=1-7
            prices = $('#unqualifiedBuyBox .a-color-price');
        }

        //http://www.amazon.co.uk/gp/product/B009O1P1YQ/ref=s9_al_bw_g75_i2?pf_rd_m=A3P5ROKL5A1OLE&pf_rd_s=merchandised-search-5&pf_rd_r=156GN1VA9FXHC6VRVW1S&pf_rd_t=101&pf_rd_p=535038447&pf_rd_i=60036031


        if (prices.length == 0) {
            prices = $('#priceMessagingToBuy', product);
        }

        if (prices.length == 0) {
            prices = $('#priceBlock .banjoPrice', product);
        }

        if (prices.length == 0) {
            prices = $('#MediaMatrix li.selected .a-color-price', product);
        }

        if (prices.length == 0) {
            prices = $('#guild-buybox-container .guild_priceblock_ourprice', product);
        }

        for (var i = 0; i < prices.length; i++) {
            var priceTextFinded = prices[i].innerText;
            if (priceTextFinded) {
                return priceTextFinded;
            }
        }

        return null;
    };


    this.getBrand = function(product) {
        var brandNameTextArr = [];
        var brands = $('#brand', product);
        for (var i = 0; i < brands.length; i++) {
            var brandNameInner = brands[i].innerText;

            if (brandNameInner) {
                brandNameTextArr.push(brandNameInner);
            }
        }

        return brandNameTextArr.join(',');
    };

    this.getBreadCrumbs = function(product) {
        var breadCrumbAll = [];
        var breadCrumbs = $('#wayfinding-breadcrumbs_container', product);

        if (breadCrumbs.length > 0) {
            var backLink = $('a#breadcrumb-back-link', breadCrumbs);

            if (backLink.length > 0) {
                breadCrumbs = null;
            } else {
                breadCrumbs = $('a', breadCrumbs);
            }
        } else {
            breadCrumbs = null;
        }

        if (breadCrumbs == null) {
            if (Jisp.isExistsElement('#dmusic_buybox_container') ||
                Jisp.isExistsElement('[data-feature-name="dmusicTracklist"]')) {
                breadCrumbAll.push('Music Downloads');
                return breadCrumbAll;
            }
        }

        if (breadCrumbs == null) {
            // find breadcrumbs http://www.amazon.co.uk/gp/product/B006Y7DBHC/ref=s9_hps_bw_g201_i2?pf_rd_m=A3P5ROKL5A1OLE&pf_rd_s=merchandised-search-4&pf_rd_r=1HG53H6N3TW8VEMM5R65&pf_rd_t=101&pf_rd_p=669514967&pf_rd_i=60041031
            breadCrumbs = $('div.bucket', product);

            var breadCrumbElement = null;

            var ids = [
                "quickPromoBucketContent", "fallbacksession-sims-feature", "A9AdsMiddleBoxTop", "sims_fbt", "vtpsims",
                "productDescription"
            ];

            for (var i = 0; i < breadCrumbs.length; i++) {
                if (ids.indexOf(breadCrumbs[i].id) >= 0) {
                    continue;
                }

                var nestedElements = $('div.content > ul > li > a', breadCrumbs[i]);
                if (nestedElements.length > 0) {
                    breadCrumbElement = breadCrumbs[i];
                    break;
                }
            }

            if (breadCrumbElement != null) {
                breadCrumbs = $('div.content > ul > li', breadCrumbElement).first();
                breadCrumbs = $('a', breadCrumbs);
            } else {
                breadCrumbs = null;
            }
        }

        if (breadCrumbs == null) {
            //http://www.amazon.co.uk/CROSSHATCH-PLIXXIE-JACKET-PADDED-DESIGNER/dp/B018VPSY2Q/ref=sr_1_2?s=clothing&ie=UTF8&qid=1457624889&sr=1-2&keywords=coat
            breadCrumbs = $('a', $('#browse_feature_div', product));
        }

        if (breadCrumbs != null) {
            for (var i = 0; i < breadCrumbs.length; i++) {
                var breadCrumbInner = breadCrumbs[i].innerText;

                if (breadCrumbInner) {
                    breadCrumbAll.push(breadCrumbInner.trim());
                }
            }
        }

        return breadCrumbAll;
    };

    this.getPageInfo = function () {

        var amazonProductID = (this.url) ? this.url.match(/\/dp\/[\d+,\w+]{10}/) : null;
        if (!(amazonProductID)) {
            amazonProductID = (document.location.href) ? document.location.href.match(/\/dp\/[\d+,\w+]{10}/) : null;
        }
        if (!(amazonProductID)) {
            amazonProductID = (document.location.href) ? document.location.href.match(/\/product\/[\d+,\w+]{10}/) : null;
        }
        if (!(amazonProductID)) {

            var productCodeElement = $('input[name="a"]');
            var productCodeInMobile = (productCodeElement) ? productCodeElement.attr('value') : null;
            var isProductPageInMobile = (productCodeInMobile) ? /\d/.test(productCodeInMobile) : false;


            var elementExists = Jisp.isExistsElement('#dp-container');
            amazonProductID = (document.location.href && (elementExists || isProductPageInMobile)) ? document.location.href.match(/\/[\d+,\w+]{10}/) : null;
        }
        this.isProductPage = (amazonProductID) ? /\d/.test(amazonProductID) : false;

        if (!this.isProductPage) {
            this.url = document.location.href;
        }

        this.productName = this.getProductName(document);

        this.imageUrl = this.getImage(document);
        if (this.imageUrl) {
            this.imageUrl = this.imageUrl.trim();
        }

        var priceInnerText = this.getPrice(document);
        this.priceAmount = Jisp.extractPrice(priceInnerText);
        this.priceCurrency = Jisp.extractCurrency(priceInnerText);

        var brandName = this.getBrand(document);
        if (brandName) {
            brandName = brandName.trim();
            if (brandName.indexOf('<') < 0) {
                this.brandNameText = brandName;
            }
        }

        var breadCrumbs = this.getBreadCrumbs(document);
        this.categoryNameText = Jisp.extractCategoryName(breadCrumbs);

        return Jisp.getResult(this);
    };
})();
window.parser_argos_co_uk = new (function () {
    Jisp.init(this);
    this.url = Jisp.ignoreSharpUrl(this.url);

    this.placeholderImageUrl = "https://jisp.com/Content/img/placeholders/argos.co.uk.png";

    this.ready = function () {
        return true;
    };

    this.checkChanges = function () {
        return null;
    };

    this.getImage = function() {
        var images = $('.image #main img').filter(function() {
            return $(this).css('display') == 'block';
        });

        if (images.length == 0) {
            images = $('.product-info-container #main img').filter(function() {
                return $(this).css('display') == 'block';
            });
        }

        if (images.length == 0) {
            images = $('.media-player-colosseum .media-player-items li.active img');
        }

        for (var i = 0; i < images.length; i++) {
            var findedImageUrl = images[i].getAttribute('src');
            if (findedImageUrl) {
                return findedImageUrl;
            }
        }
        return null;
    };

    this.getPageInfo = function () {

        this.isProductPage = Jisp.isExistsElement('[itemtype="http://schema.org/Product"].pdp-main, [itemtype="https://schema.org/Product"].pdp-main');
        
        this.productName = Jisp.getInnerText('#pdpProduct h1'); 
        if(!this.productName){
            this.productName = Jisp.getInnerText('[data-el="pdp-product-title"]');
        }

        if (!this.productName) {
            this.productName = Jisp.getInnerText('.product-name-main [itemprop="name"]');
        }

        this.imageUrl = this.getImage();
        if (this.imageUrl && this.imageUrl.indexOf('//') === 0) {
            this.imageUrl = 'http:' + this.imageUrl;
        }
        
        var priceInnerText = Jisp.getInnerText('#pdpPricing .price');
        if (!priceInnerText) {
            priceInnerText = Jisp.getInnerText('[data-el="pdp-price"]');
        }
        if (!priceInnerText) {
            priceInnerText = Jisp.getAttribute('[itemtype="http://schema.org/Product"] .pdp-pricing-module [itemprop="price"], [itemtype="https://schema.org/Product"] .pdp-pricing-module [itemprop="price"]', 'content');
        }

        this.priceAmount = Jisp.extractPrice(priceInnerText);

        this.priceCurrency = Jisp.extractCurrency(priceInnerText);
        if (!this.priceCurrency) {
            var currencyValue = Jisp.getAttribute('[itemtype="http://schema.org/Product"] .pdp-pricing-module [itemprop="priceCurrency"], [itemtype="https://schema.org/Product"] .pdp-pricing-module [itemprop="priceCurrency"]', 'content');
            this.priceCurrency = Jisp.extractCurrency(currencyValue);
        }

        this.brandNameText = Jisp.getInnerText('.pdp-view-brand-main');
        if (!this.brandNameText) {
            this.brandNameText = Jisp.getInnerText('[itemprop="brand"]');
        }

        var breadCrumbs = Jisp.getInnerTextArr('#breadcrumb a', ['Home']);
        if (breadCrumbs.length == 0) {
            breadCrumbs = Jisp.getInnerTextArr('.breadcrumb a', []);
        }
        this.categoryNameText = Jisp.extractCategoryName(breadCrumbs);

        return Jisp.getResult(this);
    };
})();
window.parser_asos_com = new (function () {
    Jisp.init(this);

    this.placeholderImageUrl = "https://jisp.com/Content/img/placeholders/asos.com.png";

    this.ready = function () {
        var priceInnerText = Jisp.getInnerText('#asos-product #product-price [data-id="current-price"]');
        var productNameText = Jisp.getInnerText('#asos-product #aside-content h1');
        return !!priceInnerText && priceInnerText.length > 0 && (!!productNameText);
    };

    this.checkChanges = function () {
        return null;
    };

    this.getPageInfo = function() {

        this.isProductPage = Jisp.isExistsElement('#asos-product');

        this.productName = Jisp.getInnerText('#asos-product #aside-content h1');

        this.imageUrl = Jisp.getAttribute('#product-gallery .gallery-image', 'src');
        if (!this.imageUrl) {
            this.imageUrl = Jisp.getMetaProperty(document, "og:image");
        }

        if (this.imageUrl && this.imageUrl.indexOf('//') === 0) {
            this.imageUrl = document.location.protocol + '//' + this.imageUrl.substring(2);
        }

        var priceInnerText = Jisp.getInnerText('#asos-product #product-price [data-id="current-price"]');
        this.priceAmount = Jisp.extractPrice(priceInnerText);
        this.priceCurrency = Jisp.extractCurrency(priceInnerText);

        var brandInnerText = Jisp.getInnerText('.brand-description a');
        if (brandInnerText) {
            this.brandNameText = brandInnerText.trim();
        }

        var breadCrumbs = Jisp.getInnerTextArr('#breadcrumb li a', ['Home']);
        if (breadCrumbs != null) {
            var crumbs = breadCrumbs.map(function(name) {
                if ((name) && (name.indexOf('Search results for') === 0)) {
                    return null;
                }

                return name;
            });

            breadCrumbs = crumbs;
        }
        this.categoryNameText = Jisp.extractCategoryName(breadCrumbs);


        return Jisp.getResult(this);
    };
})();
window.parser_boden_co_uk = new (function () {
    Jisp.init(this);

    this.placeholderImageUrl = "https://jisp.com/Content/img/placeholders/boden.co.uk.png";

    this.ready = function () {
        return true;
    };

    this.checkChanges = function () {
        return null;
    };

    this.getPageInfo = function() {

        this.isProductPage = Jisp.isExistsElement('#pm-product');

        var product = Jisp.getProductElement();
        if (product) {
            this.productName = Jisp.getInnerText('#pm-productDetail [itemprop="name"]');
            if (!this.productName) {
                this.productName = Jisp.getMetaProperty(document, "og:title");
            }

            this.imageUrl = Jisp.getAttribute('[itemprop="image"][src]:not([src=""])', 'src');
            if (!this.imageUrl) {
                this.imageUrl = Jisp.getImage(document);
            }

            var priceInnerText = Jisp.getInnerText('#pm-product #pm-productPrices .pdpNowPrice');
            this.priceAmount = Jisp.extractPrice(priceInnerText);
            this.priceCurrency = Jisp.extractCurrency(priceInnerText);

            this.brandNameText = Jisp.getMeta(document, 'itemprop="brand"');

            var breadCrumbs = Jisp.getInnerTextArr('#breadcrumb a', ['Home']);
            this.categoryNameText = Jisp.extractCategoryName(breadCrumbs);
        }
        else {
            this.except = true;
        }

        return Jisp.getResult(this);
    };
})();
window.parser_boohoo_com = new (function () {
    Jisp.init(this);

    this.placeholderImageUrl = "https://jisp.com/Content/img/placeholders/boohoo.com.png";

    this.ready = function () {
        return true;
    };

    this.checkChanges = function () {
        return null;
    };

    this.getPageInfo = function() {

        var product = Jisp.getProductElement();
        if (product) {
            this.productName = Jisp.getProductName(product);

            this.imageUrl = Jisp.getAttribute('#productdetail-image img', 'src');
            if (!this.imageUrl) {
                this.imageUrl = Jisp.getMetaProperty(document, "og:image");
            }

            if ((this.imageUrl) && (this.imageUrl.indexOf("//") == 0)){
                this.imageUrl = "http:" + this.imageUrl;
            }

            var priceInnerText = Jisp.getPrice(product);
            this.priceAmount = Jisp.extractPrice(priceInnerText);
            this.priceCurrency = Jisp.extractCurrency(priceInnerText); //<meta itemprop="priceCurrency" content="GBP">

            this.brandNameText = Jisp.getInnerText('.brand-shops');
            
            var breadCrumbs = Jisp.getInnerTextArr('.categorytree a:not(.crumbtrail-home)', []);
            this.categoryNameText = Jisp.extractCategoryName(breadCrumbs); 
        }
        else {
            this.except = true;
        }

        return Jisp.getResult(this);
    };
})();
window.parser_boots_com = new (function () {
    Jisp.init(this);

    this.url = Jisp.ignoreSharpUrl(this.url);

    this.placeholderImageUrl = "https://jisp.com/Content/img/placeholders/boots.com.png";

    this.ready = function () {
        var image = this.getImage();
        return image != null;
    };

    this.checkChanges = function () {
        return null;
    };

    this.getBrand = function (product) {
        var brandNameText = null;
        var brands = $('.brandBox .brandLogo img[alt]', product);
        for (var i = 0; i < brands.length; i++) {
            brandNameText = brands[i].alt;
        }

        if (brandNameText == null) {
            brands = $('#brandHeader a[href]', product);
            for (var i = 0; i < brands.length; i++) {
                
                var brandUrl = brands[i].href;  // /en/Wahl/
                if (brandUrl) {
                    var brandParts = brandUrl.split('/');
                    for (var j = 0; j < brandParts.length; j++) {
                        if(brandParts[j])
                        {
                            brandNameText = brandParts[j];
                        }
                    }
                }
            }
        }

        return brandNameText;
    }

    this.getImage = function () {
        var image = null;

        var img = document.querySelectorAll('.s7thumb[state="selected"]')[0];
        if (img) {
            var style = window.getComputedStyle(img);

            var imgUrl = null;

            for (var i = 0; i < style.length; i++) {
                var prop = style[i];
                if (prop === 'background-image') {
                    imgUrl = style.getPropertyValue(prop);
                    break;
                }
            }

            if ((imgUrl) && (imgUrl !== 'undefined')) {
                imgUrl = String(imgUrl);

                //startsWith not supported in phantomjs
                if (imgUrl.substr(0, 'url("'.length) === 'url("') {
                    imgUrl = imgUrl.substr(5).replace('")', '');
                    image = imgUrl;
                }

                if (!image) {
                    if (imgUrl.substr(0, 'url('.length) === 'url(') {
                        imgUrl = imgUrl.substr(4).replace(')', '');
                        image = imgUrl;
                    }
                }
            }
        }

        if (image) {
            if (/[^\s]*&wid=[\d]*&hei=[\d]*/.test(image)) {
                image = image.replace(/&wid=[\d]*&hei=[\d]*/, "&wid=2000&hei=2000");
            }
        }

        return image;
    }

    this.getPageInfo = function () {

        this.isProductPage = Jisp.isExistsElement('#estore_productpage_template_container') ||
        (Jisp.isExistsElement('.productDetails') && (document.location.hostname === 'm.boots.com'));

        if (!this.isProductPage) {
            this.url = document.location.href;
        }

        this.productName = Jisp.getInnerText('#estore_productpage_template_container [itemprop="name"]');

        this.imageUrl = this.getImage();

        var priceInnerText = Jisp.getInnerText('#PDP_productPrice'); 
        this.priceAmount = Jisp.extractPrice(priceInnerText);
        this.priceCurrency = Jisp.extractCurrency(priceInnerText);

        this.brandNameText = this.getBrand(document);

        var breadCrumbs = Jisp.getInnerTextArr('#widget_breadcrumb a', ['Home']);
        this.categoryNameText = Jisp.extractCategoryName(breadCrumbs);

        if (!this.except) {
            this.url = Jisp.ignoreQueryUrl(this.url);
        }

        return Jisp.getResult(this);
    };
})();
window.parser_currys_co_uk = new (function () {
    Jisp.init(this);
    this.url = Jisp.ignoreSharpUrl(this.url);

    this.placeholderImageUrl = "https://jisp.com/Content/img/placeholders/currys.co.uk.png";

    this.title = null;
    var titleElement = document.getElementsByTagName("title");
    if ((titleElement) && titleElement.length > 0) {
        titleElement = document.getElementsByTagName("title")[0];
        if (titleElement) {
            this.title = titleElement.innerText;
        }
    }

    this.ready = function () {
        return true;
    };

    this.checkChanges = function () {
        return null;
    };

    this.getImage = function () {
        var imageUrl = null;

        var image = $('#product-main .swiper-slide-active .product-image');
        if (!image) {
            image = $('[itemprop="image"][src]:not([src=""])');
        }

        if (image) {
            image = image.first()[0];
        }

        if (image) {
            imageUrl = image.getAttribute('src');
        }

        if (!imageUrl) {
            imageUrl = Jisp.getMetaName(document, "twitter:image");
        }

        if (!imageUrl) {
            imageUrl = Jisp.getMetaProperty(document, "og:image");
        }

        if (!imageUrl) {
            imageUrl = Jisp.getMetaName(document, "og:image");
        }

        return (imageUrl) ? imageUrl : null;
    };

    this.getPageInfo = function() {

        var product = Jisp.getProductElement();

        this.isProductPage = (product) || Jisp.isExistsElement('.product-page');

        if (!product) {
            product = document;
        }

        this.productName = Jisp.getMetaProperty(document, "og:title");

        this.imageUrl = this.getImage(product);

        var priceInnerText = Jisp.getPrice(product);
        if (!priceInnerText) {
            priceInnerText = Jisp.getInnerText('.product-page [data-key="current-price"]');
        }
        this.priceAmount = Jisp.extractPrice(priceInnerText);
        var currencyInnerText = Jisp.extractCurrency(priceInnerText);
        if(!currencyInnerText){
            currencyInnerText = Jisp.extractCurrency(Jisp.getCurrency(product));
        }
        this.priceCurrency = currencyInnerText;

        this.brandNameText = Jisp.getBrand(product);

        var breadCrumbs = Jisp.getBreadCrumbs(document);
        this.categoryNameText = Jisp.extractCategoryName(breadCrumbs);

        return Jisp.getResult(this);
    };
})();
window.parser_currys_com = new (function () {
    Jisp.init(this);

    this.ready = function () {
        return true;
    };

    this.checkChanges = function () {
        return null;
    };

    this.getPageInfo = function () {

        var product = Jisp.getProductElement();
        if (!product) {
            product = document;
        }

        this.productName = Jisp.getInnerText('.PCContentDesc');

        this.imageUrl = Jisp.getAttribute('.PCITableImage img', 'src');

        var priceInnerText = Jisp.getInnerText('.PCContentRow1 .PCContentYourPrc');
        this.priceAmount = Jisp.extractPrice(priceInnerText);
        var currencyInnerText = '£';
        if (!currencyInnerText) {
            currencyInnerText = Jisp.extractCurrency(Jisp.getCurrency(product));
        }
        this.priceCurrency = currencyInnerText;

        this.brandNameText = Jisp.getBrand(product);

        var breadCrumbs = Jisp.getInnerTextArr('.NavBarTable td.NavBarBar a');
        this.categoryNameText = Jisp.extractCategoryName(breadCrumbs);

        return Jisp.getResult(this);
    };
})();
window.parser_dailymotion_com = new (function () {
    Jisp.init(this);

    this.ready = function () {
        return true;
    };

    this.checkChanges = function () {
        return null;
    };

    this.getPageInfo = function() {

        var product = Jisp.getProductElement();
        if (!product) {
            product = document;
        }

        this.productName = Jisp.getInnerText('#video_title'); //Jisp.getProductName(product);

        this.imageUrl = Jisp.getImage(product);

        var priceInnerText = Jisp.getPrice(product);
        this.priceAmount = Jisp.extractPrice(priceInnerText);
        var currencyInnerText = Jisp.extractCurrency(priceInnerText);
        if(!currencyInnerText){
            currencyInnerText = Jisp.extractCurrency(Jisp.getCurrency(product));
        }
        this.priceCurrency = currencyInnerText;

        this.brandNameText = Jisp.getBrand(product);

        var breadCrumbs = Jisp.getBreadCrumbs(document);
        this.categoryNameText = Jisp.extractCategoryName(breadCrumbs);

        var videoUrl = Jisp.getAttribute('meta[name="twitter:player"]', 'value');

        return Jisp.getResult(this);
    };
})();
window.parser_debenhams_com = new (function () {
    Jisp.init(this);

    this.placeholderImageUrl = "https://jisp.com/Content/img/placeholders/debenhams.com.png";

    this.ready = function () {
        return true;
    };

    this.checkChanges = function () {
        return null;
    };

    this.getBreadCrumbs = function (product) {
        var breadCrumbAll = Jisp.getInnerTextArr('#breadcrumb_cat a');

        if (breadCrumbAll.length == 0)
        {
            var categoryText = Jisp.getMetaProperty(product, "category");
            if (categoryText) {
                var arr = categoryText.split('|');
                breadCrumbAll.push(arr[0]);
            }

            var subcategoryText = Jisp.getMetaProperty(product, "subcategory");
            if (subcategoryText) {
                var arr = subcategoryText.split('|');
                breadCrumbAll.push(arr[0]);
            }
        }
            
        return breadCrumbAll;
    }

    this.getPageInfo = function () {

        var typeName = Jisp.getAttribute('[property="og:type"]', 'content');
        typeName = (typeName) ? typeName.toLowerCase() : "";
        if (typeName === "product") {
            this.isProductPage = true;

            this.productName = Jisp.getMetaProperty(document, "short_description");

            this.imageUrl = Jisp.getAttribute('#prodImg img', 'src');
            if (!this.imageUrl) {
                this.imageUrl = Jisp.getImage(document);
            }
            
            var priceInnerText = Jisp.getMetaName(document, 'twitter:data1');
            if (priceInnerText) {
                priceInnerText = priceInnerText.toLowerCase().replace('now', '');
            }
            this.priceAmount = Jisp.extractPrice(priceInnerText);
            var currencyInnerText = Jisp.getAttribute('[itemprop="priceCurrency"]', 'content');
            this.priceCurrency = Jisp.extractCurrency(currencyInnerText);
            
            this.brandNameText = Jisp.getMetaProperty(document, "brand");

            var breadCrumbs = this.getBreadCrumbs(document);
            this.categoryNameText = Jisp.extractCategoryName(breadCrumbs);
        }
        else {
            this.except = true;
        }

        return Jisp.getResult(this);
    };
})();
window.parser_defaultParser= new (function () {
    this.ready = function () {
        return true;
    };

    this.checkChanges = function () {
        return null;
    };

    this.getPageInfo = function() {
        var url = document.location.href;
        var title = document.getElementsByTagName("title")[0].innerHTML;
        var imageUrl = 'https://www.jisp.com/Content/images/jisp-logo@2x.png';

        return {
            except: true,
            url: url,
            title: title,
            imageUrl: imageUrl,
            price: null,
            currency: null,
            websiteBrand: null,
            websiteCategory: null
        };
    };
})();
window.parser_defaultProductParser = new (function () {
    Jisp.init(this);

    this.ready = function () {
        return true;
    };

    this.checkChanges = function () {
        return null;
    };

    this.getPageInfo = function() {

        var product = Jisp.getProductElement();
        if (!product) {
            product = document;
        }

        this.productName = Jisp.getProductName(product);

        this.imageUrl = Jisp.getImage(product);

        var priceInnerText = Jisp.getPrice(product);
        this.priceAmount = Jisp.extractPrice(priceInnerText);
        var currencyInnerText = Jisp.extractCurrency(priceInnerText);
        if(!currencyInnerText){
            currencyInnerText = Jisp.extractCurrency(Jisp.getCurrency(product));
        }
        this.priceCurrency = currencyInnerText;

        this.brandNameText = Jisp.getBrand(product);

        var breadCrumbs = Jisp.getBreadCrumbs(document);
        this.categoryNameText = Jisp.extractCategoryName(breadCrumbs);

        return Jisp.getResult(this);
    };
})();
window.parser_diy_com = new (function () {
    Jisp.init(this);

    if (!this.isProductPage) {
        this.url = document.location.href;
    }

    this.placeholderImageUrl = "https://jisp.com/Content/img/placeholders/diy.com.png";

    this.ready = function () {
        return true;
    };

    this.checkChanges = function () {
        return null;
    };

    this.getBrand = function () {
        var elements = $('[aria-labelledby="product-details"] .media-body table th').filter(function () {
            return $(this).text().toLowerCase().indexOf('Brand'.toLowerCase()) >= 0;
        });
        elements = elements.next();
        for (var i = 0; i < elements.length; i++) {
            var elementInnerText = elements[i].innerText;
            if (elementInnerText) {
                return elementInnerText;
            }
        }
        return null;
    }

    this.getPageInfo = function() {

        var product = Jisp.getProductElement();
        if (product) {
            this.productName = Jisp.getProductName(product);

            this.imageUrl = Jisp.getMetaProperty(document, "og:image");

            var priceInnerText = Jisp.getPrice(product); //<meta itemprop="priceCurrency" content="GBP">
            console.log(priceInnerText);
            this.priceAmount = Jisp.extractPrice(priceInnerText);
            this.priceCurrency = Jisp.extractCurrency(priceInnerText);

            this.brandNameText = this.getBrand();
            
            var breadCrumbs = Jisp.getInnerTextArr('[itemprop="breadcrumb"] a', ['Home']);
            breadCrumbs = breadCrumbs.map(function (name) {
                return name.replace('[...]', '').trim();
            });
            this.categoryNameText = Jisp.extractCategoryName(breadCrumbs); 
        }
        else {
            this.except = true;
        }

        return Jisp.getResult(this);
    };
})();
window.parser_ebay_co_uk = new (function () {
    Jisp.init(this);
    this.url = Jisp.ignoreSharpUrl(this.url);
    if (this.url) {
        if (this.url.substring(this.url.length - 1) == '?') {
            this.url = this.url.substring(0, this.url.length - 1);
        }
    }

    this.placeholderImageUrl = "https://jisp.com/Content/img/placeholders/ebay.co.uk.png";
    
    this.ready = function () {
        return true;
    };

    this.checkChanges = function () {
        return null;
    };

    this.getProductName = function(product) {
        var productNames = $('#itemTitle', product);

        if (productNames.length == 0) {
            productNames = $('[itemprop="name"].product-title');
        }

        if (productNames.length == 0) {
            productNames = $('[itemprop="name"]', product);
        }

        for (var i = 0; i < productNames.length; i++) {
            var productNameInnerText = productNames[i].innerText;
            if (productNameInnerText) {
                return productNameInnerText;
            }
        }

        return null;
    };

    this.getPrice = function(product) {

        var prices = $('[itemprop="price"]', product);
        if (prices.length == 0) {
            prices = $('#mm-saleDscPrc', product);
        }
        if (prices) {
            for (var i = 0; i < prices.length; i++) {
                var priceInnerText = prices[i].innerText;
                if (priceInnerText) {
                    return priceInnerText;
                }
            }
        }

        return Jisp.getAttribute(
            '[itemtype="http://schema.org/Product"] [itemtype="http://schema.org/Offer"] .offer-details-wrapper [itemprop="price"], [itemtype="https://schema.org/Product"] [itemtype="https://schema.org/Offer"] .offer-details-wrapper [itemprop="price"]',
            'content');
    };

    this.getBrand = function(product) {
        var brandNameTextArr = [];
        var brands = $('[itemprop="name"]', $('[itemprop="brand"]', product));
        for (var i = 0; i < brands.length; i++) {
            brandNameTextArr.push(brands[i].innerText);
        }

        if (brandNameTextArr.length == 0) {
            brands = $('td.attrLabels:contains("Brand:")', $('.itemAttr', document)).next();

            for (var i = 0; i < brands.length; i++) {
                brandNameTextArr.push(brands[i].innerText);
            }
        }

        return brandNameTextArr.join(',');
    };

    this.getBreadCrumbs = function(product) {
        var breadCrumbAll = [];
        var breadCrumbs = $('[itemprop="name"]',
            '[itemprop="item"]',
            '[itemprop="itemListElement"]',
            '[itemtype="http://schema.org/Breadcrumblist"]',
            product);

        if (breadCrumbs.length == 0) {
            breadCrumbs = $('[itemprop="name"]',
                '[itemprop="item"]',
                '[itemprop="itemListElement"]',
                '[itemtype="https://schema.org/Breadcrumblist"]',
                product);
        }

        if (breadCrumbs.length == 0) {
            breadCrumbs = $('#NavigationPanel table a.thrd', product);
        }

        for (var i = 0; i < breadCrumbs.length; i++) {
            breadCrumbAll.push(breadCrumbs[i].innerText);
        }

        return breadCrumbAll;
    };

    this.getImage = function(product) {
        var imageUrl = null;

        var image = $('[itemprop="image"][src]:not([src=""])').first()[0];
        if (image) {
            imageUrl = image.getAttribute('src');
        }

        if (!imageUrl) {
            imageUrl = Jisp.getMetaName(document, "twitter:image");
        }

        if (!imageUrl) {
            imageUrl = Jisp.getAttribute('[itemtype="http://schema.org/Product"] #w4-w0 img, [itemtype="https://schema.org/Product"] #w4-w0 img', 'src');
        }

        if (!imageUrl) {
            imageUrl = Jisp.getAttribute('[itemtype="http://schema.org/Product"] img, [itemtype="https://schema.org/Product"] img', 'src');
        }

        if (!imageUrl) {
            imageUrl = Jisp.getMetaProperty(document, "og:image");
        }

        if (!imageUrl) {
            imageUrl = Jisp.getMetaName(document, "og:image");
        }

        return (imageUrl) ? imageUrl : null;
    };

    this.getPageInfo = function() {

        var product = null;

        var products = $('[itemtype="http://schema.org/Product"], [itemtype="https://schema.org/Product"]');
        if (products) {
            if (products.length == 1) {
                product = products[0];
            }
        }

        if (product) {
            this.isProductPage = true;

            var productNameText = this.getProductName(product);
            if (productNameText) {
                this.productName = productNameText.replace(/<span.*>[\w\W]{1,}(.*?)[\w\W]{1,}<\/span>/gi, "");
            }

            this.imageUrl = this.getImage(product);

            var priceInnerText = this.getPrice(product);
            this.priceAmount = Jisp.extractPrice(priceInnerText);
            this.priceCurrency = Jisp.extractCurrency(priceInnerText);
            if (!this.priceCurrency) {
                this.priceCurrency = Jisp.getAttribute('[itemtype="http://schema.org/Product"] [itemtype="http://schema.org/Offer"] .offer-details-wrapper [itemprop="priceCurrency"]', 'content');
            }
            if (!this.priceCurrency) {
                this.priceCurrency = Jisp.getAttribute('[itemtype="https://schema.org/Product"] [itemtype="https://schema.org/Offer"] .offer-details-wrapper [itemprop="priceCurrency"]', 'content');
            }

            this.brandNameText = this.getBrand(product);

            var breadCrumbs = this.getBreadCrumbs(product);
            this.categoryNameText = Jisp.extractCategoryName(breadCrumbs);
        }
        else {
            this.except = true;
        }

        return Jisp.getResult(this);
    };
})();
window.parser_ebuyer_com = new (function () {
    Jisp.init(this);

    this.placeholderImageUrl = "https://jisp.com/Content/img/placeholders/ebuyer.com.png";

    this.ready = function () {
        return true;
    };

    this.checkChanges = function () {
        return null;
    };

    this.getPageInfo = function() {

        this.isProductPage = this.isProductPage ||
            (Jisp.isExistsElement('.product-details') && (document.location.hostname === 'm.ebuyer.com'));

        if (!this.isProductPage) {
            this.url = document.location.href;
        }

        var product = Jisp.getProductElement();
        if (product) {
            this.productName = Jisp.getInnerText('.product-titlea');
            if (!this.productName) {
                this.productName = Jisp.getInnerText('.product-title');
            }

            var image = $('#gallery-carousel .item.active .js-carousel-img');
            if (image) {
                image = image.first()[0];

                if (image) {
                    this.imageUrl = image.getAttribute('src');
                }
            }

            if (!this.imageUrl) {
                image = $('[itemprop="image"][src]:not([src=""])');
                if (image) {
                    image = image.first()[0];

                    if (image) {
                        this.imageUrl = image.getAttribute('src');
                    }
                }
            }

            if (this.imageUrl) {
                if (this.imageUrl.indexOf('//') === 0) {
                    this.imageUrl = document.location.protocol + '//' + this.imageUrl.substring(2);
                }
            }

            if (!this.imageUrl) {
                this.imageUrl = Jisp.getMetaName(document, "og:image");
            }
            
            var priceInnerText = Jisp.getPrice(product);
            this.priceAmount = Jisp.extractPrice(priceInnerText);
            var currencyInnerText = Jisp.getMeta(document, 'itemprop="priceCurrency"');
            this.priceCurrency = Jisp.extractCurrency(currencyInnerText);

            this.brandNameText = Jisp.getAttribute('[itemprop="brand"] [itemprop="name"]', 'content');
            
            //var breadCrumbs = [];
            //breadCrumbs.push(Jisp.getMeta(document, 'itemprop="category"'));
            var breadCrumbs = Jisp.getInnerTextArr('[itemtype="http://schema.org/BreadcrumbList"] [itemtype="http://schema.org/ListItem"] [itemprop="name"], [itemtype="https://schema.org/BreadcrumbList"] [itemtype="https://schema.org/ListItem"] [itemprop="name"]', []);
            this.categoryNameText = Jisp.extractCategoryName(breadCrumbs); 
        }
        else {
            this.except = true;
        }

        return Jisp.getResult(this);
    };
})();
window.parser_en_wikipedia_org = new (function () {
    Jisp.init(this);

    this.ready = function () {
        return true;
    };

    this.checkChanges = function () {
        return null;
    };

    this.getPageInfo = function() {

        var product = Jisp.getProductElement();
        if (!product) {
            product = document;
        }

        this.productName = Jisp.getInnerText('#firstHeading');
        if (!this.productName) {
            this.productName = Jisp.getProductName(product);
        }

        this.imageUrl = Jisp.getAttribute('#bodyContent .thumb .image img, #bodyContent .vcard .image img, #bodyContent .infobox .image img, #bodyContent .vcard img[usemap]', 'src');
        
        if (!this.imageUrl) {
            this.imageUrl = Jisp.getImage(product);
        }

        if (this.imageUrl) {
            if (this.imageUrl.indexOf('//') === 0) {
                this.imageUrl ='https:' + this.imageUrl;
            }
        }

        // set default image to icon
        if (!this.imageUrl) {
            this.imageUrl = 'https://en.wikipedia.org/static/images/project-logos/enwiki.png';
        }

        var priceInnerText = Jisp.getPrice(product);
        this.priceAmount = Jisp.extractPrice(priceInnerText);
        var currencyInnerText = Jisp.extractCurrency(priceInnerText);
        if(!currencyInnerText){
            currencyInnerText = Jisp.extractCurrency(Jisp.getCurrency(product));
        }
        this.priceCurrency = currencyInnerText;

        this.brandNameText = Jisp.getBrand(product);

        var breadCrumbs = Jisp.getBreadCrumbs(document);
        this.categoryNameText = Jisp.extractCategoryName(breadCrumbs);

        return Jisp.getResult(this);
    };
})();
window.parser_halfords_com = new (function () {
    Jisp.init(this);

    this.placeholderImageUrl = "https://jisp.com/Content/img/placeholders/halfords.com.png";

    this.ready = function () {
        return true;
    };

    this.checkChanges = function () {
        return null;
    };

    this.getPageInfo = function () {

        this.isProductPage = Jisp.isExistsElement('#pdpMirakl');

        this.productName = Jisp.getMetaProperty(document, "og:title");

        this.imageUrl = Jisp.getAttribute('.amp-visible .amp-image-container .amp-main-img', 'src');
        if (!this.imageUrl) {
            this.imageUrl = Jisp.getMetaProperty(document, "og:image");
        }

        var priceInnerText = Jisp.getInnerText('.hproduct .price');
        this.priceAmount = Jisp.extractPrice(priceInnerText);
        this.priceCurrency = Jisp.extractCurrency(priceInnerText);
        
        this.brandNameText = Jisp.getInnerText('.hproduct .brand');

        var breadCrumbs = [];
        breadCrumbs.push(Jisp.getInnerText('.hproduct .category'));
        this.categoryNameText = Jisp.extractCategoryName(breadCrumbs);

        return Jisp.getResult(this);
    };
})();
window.parser_hm_com = new (function () {
    Jisp.init(this);

    this.placeholderImageUrl = "https://jisp.com/Content/img/placeholders/hm.com.png";

    this.ready = function () {
        return true;
    };

    this.checkChanges = function () {
        return null;
    };

    this.getPageInfo = function () {

        this.isProductPage = Jisp.isExistsElement('.product-detail') || 
            ((document.location.hostname === 'm2.hm.com') && Jisp.isExistsElement('.product-detail-meta'));

        if (document.location.hostname === 'm2.hm.com') {
            if (this.isProductPage) {
                if (document.location.href) {
                    this.url = document.location.href.replace('m2.hm.com/m', 'www2.hm.com');
                }
            } else {
                this.url = document.location.href;
            }
        } else {
            if (!this.isProductPage) {
                this.url = document.location.href;
            }

            this.url = Jisp.ignoreSharpUrl(this.url);
            if (this.url.indexOf('/') === 0) {
                this.url = document.location.protocol + '//' + document.location.hostname + this.url;
            }
        }

        this.productName = Jisp.getInnerText('.product-detail .product-item-headline');
        
        if (!this.productName) {
            this.productName = Jisp.getMetaProperty(document, "og:title");
        }

        this.imageUrl = Jisp.getAttribute('.product-detail-main-image-image', 'src');
        if (!this.imageUrl) {
            this.imageUrl = Jisp.getMetaProperty(document, "og:image");
        }
        this.imageUrl = Jisp.addProtocolToUrl(this.imageUrl);

        var priceInnerText = Jisp.getInnerText('.price .price-value');
        this.priceAmount = Jisp.extractPrice(priceInnerText);
        this.priceCurrency = Jisp.extractCurrency(priceInnerText);

        this.brandNameText = 'H&M';

        var breadCrumbs = Jisp.getBreadCrumbs(document);
        this.categoryNameText = Jisp.extractCategoryName(breadCrumbs);

        return Jisp.getResult(this);
    };
})();
window.parser_homebase_co_uk = new (function () {
    Jisp.init(this);
    this.url = Jisp.ignoreSharpUrl(this.url);

    this.placeholderImageUrl = "https://jisp.com/Content/img/placeholders/homebase.co.uk.png";

    this.ready = function () {
        return true;
    };

    this.checkChanges = function () {
        return null;
    };

    this.getPageInfo = function() {

        var product = Jisp.getProductElement();
        if (!product) {
            product = document;
        }

        this.productName = Jisp.getProductName(product);

        if (this.isProductPage) {
            var findedImageUrl = Jisp.getAttribute('#main img.cycle-slide-active', 'src');
            if (!findedImageUrl) {
                findedImageUrl = Jisp.getImage(product);
            }

            if (findedImageUrl) {
                if (findedImageUrl.indexOf("//") == 0) {
                    findedImageUrl = "http:" + findedImageUrl;
                }
            }
            this.imageUrl = findedImageUrl;
        }

        var priceInnerText = Jisp.getPrice(product);
        this.priceAmount = Jisp.extractPrice(priceInnerText);
        var currencyInnerText = Jisp.extractCurrency(priceInnerText);
        if(!currencyInnerText){
            currencyInnerText = Jisp.extractCurrency(Jisp.getCurrency(product));
        }
        this.priceCurrency = currencyInnerText;

        this.brandNameText = Jisp.getBrand(product);

        var breadCrumbs = Jisp.getBreadCrumbs(document);
        this.categoryNameText = Jisp.extractCategoryName(breadCrumbs);

        return Jisp.getResult(this);
    };
})();
window.parser_houseoffraser_co_uk = new (function () {
    Jisp.init(this);

    this.placeholderImageUrl = "https://jisp.com/Content/img/placeholders/houseoffraser.co.uk.png";

    this.ready = function () {
        return true;
    };

    this.checkChanges = function () {
        return null;
    };

    this.getBreadCrumbs = function (product) {
        var breadCrumbAll = [];
        var breadCrumbs = $('.hof-breadcrumbs [itemprop="breadcrumb"]', product);

        for (var i = 0; i < breadCrumbs.length; i++) {
            if ((breadCrumbs[i].id == 'back-to-results-breadcrumb') || (breadCrumbs[i].id == 'homepage-breadcrumb')) {
                continue;
            }

            breadCrumbAll.push(breadCrumbs[i].innerText);
        }

        return breadCrumbAll;
    }

    this.getPageInfo = function () {

        var product = Jisp.getProductElement();
        if (product) {

            this.productName = Jisp.getMetaProperty(document, "og:title");

            var img = document.querySelectorAll('[itemtype="http://schema.org/Product"] .main-carousel .magnifier, [itemtype="https://schema.org/Product"] .main-carousel .magnifier')[0];
            if (img) {
                var style = window.getComputedStyle(img);
                var imgUrl = style.getPropertyValue('background');
                if (imgUrl) {
                    imgUrl = imgUrl.trim();
                    var start = imgUrl.indexOf('url("');
                    if (start > 0) {
                        imgUrl = imgUrl.substr(start + 5);
                        var end = imgUrl.indexOf('")');
                        if (end > 0) {
                            imgUrl = imgUrl.substr(0, end);
                            if (imgUrl) {
                                this.imageUrl = imgUrl;
                            }
                        }
                    }
                }
            }

            if (!this.imageUrl) {
                this.imageUrl = Jisp.getAttribute('[itemtype="http://schema.org/Product"] .main-carousel .featuredProductImage', 'src');
            }

            if (!this.imageUrl) {
                this.imageUrl = Jisp.getAttribute('[itemtype="https://schema.org/Product"] .main-carousel .featuredProductImage', 'src');
            }

            if (!this.imageUrl) {
                this.imageUrl = Jisp.getMetaProperty(document, "og:image");
            }

            var priceInnerText = Jisp.getAttribute('[itemprop="price"]', 'content');
            this.priceAmount = Jisp.extractPrice(priceInnerText);
            var currencyInnerText = Jisp.getAttribute('[itemprop="priceCurrency"]', 'content');
            this.priceCurrency = Jisp.extractCurrency(currencyInnerText);

            this.brandNameText = Jisp.getInnerText('.hof-breadcrumbs [itemprop="breadcrumb"]');

            var breadCrumbs = this.getBreadCrumbs(document);
            this.categoryNameText = Jisp.extractCategoryName(breadCrumbs);
        }
        else {
            this.except = true;
        }

        return Jisp.getResult(this);
    };
})();
window.parser_ikea_com = new (function () {
    Jisp.init(this);
    this.url = Jisp.ignoreSharpUrl(this.url);

    this.placeholderImageUrl = "https://jisp.com/Content/img/placeholders/ikea.com.png";

    this.ready = function () {
        return true;
    };

    this.checkChanges = function () {
        return null;
    };

    this.getBreadCrumbs = function (product) {
        var breadCrumbAll = [];

        var categoryText = Jisp.getMetaName(product, "IRWStats.categoryLocal");
        if (categoryText) {
            breadCrumbAll.push(categoryText);
        }

        var subcategoryText = Jisp.getMetaName(product, "IRWStats.subCategoryLocal");
        if (subcategoryText) {
            breadCrumbAll.push(subcategoryText);
        }

        var chapterText = Jisp.getMetaName(product, "IRWStats.chapterLocal");
        if (chapterText) {
            breadCrumbAll.push(chapterText);
        }

        return breadCrumbAll;
    }

    this.getPageInfo = function() {
        this.isProductPage = this.isProductPage; //&& Jisp.isExistsElement('#productInfoWrapper1');

        var product = Jisp.getProductElement();
        if (this.isProductPage) {
            var name = Jisp.getInnerText('#productNameProdInfo');
            var type = Jisp.getInnerText('#productTypeProdInfo');
            this.productName = ((name) ? name + ' ' : '') + ((type) ? type : '');
            if (!this.productName) {
                this.productName = Jisp.getInnerText('#schemaProductName');
            }

            if (!this.productName) {
                var productDescr = Jisp.getInnerText('[itemtype="http://schema.org/Product"] [itemprop="name"] .pie-description, [itemtype="https://schema.org/Product"] [itemprop="name"] .pie-description');
                var productBrand = Jisp.getInnerText('[itemtype="http://schema.org/Product"] [itemprop="name"] .global-name, [itemtype="https://schema.org/Product"] [itemprop="name"] .global-name');
                this.productName = (productDescr || '') + ' ' + (productBrand || '');
                //this.productName = Jisp.getInnerText('[itemtype="http://schema.org/Product"] .global-name');
            }

            if (this.productName) {
                this.productName = this.productName.replace(/\n/g, "").replace(/\t/g, "");
            }

            this.imageUrl = Jisp.getAttribute('#productImg', 'src');
            if (!this.imageUrl) {
                var images = $('[itemtype="http://schema.org/Product"] [data-carousel="true"] li[aria-hidden="false"] img, [itemtype="https://schema.org/Product"] [data-carousel="true"] li[aria-hidden="false"] img').filter(function () {
                    return ($(this).css('display') == 'inline') && ($(this).attr("width") > 1);
                });

                for (var i = 0; i < images.length; i++) {
                    var findedImageUrl = images[i].getAttribute('src');
                    if (findedImageUrl) {
                        this.imageUrl = findedImageUrl;
                    }
                }
            }

            if (!this.imageUrl) {
                this.imageUrl = Jisp.getImage();
            }

            if (this.imageUrl) {
                if (this.imageUrl.indexOf("/") == 0) {
                    this.imageUrl = document.location.origin + this.imageUrl;
                }
            }
      
            var priceInnerText = Jisp.getPrice(product);
            this.priceAmount = Jisp.extractPrice(priceInnerText);
            this.priceCurrency = Jisp.extractCurrency(priceInnerText);

            if (!this.priceCurrency) {
                var currencyInnerText = Jisp.getCurrency(product);
                this.priceCurrency = Jisp.extractCurrency(currencyInnerText);
            }

            this.brandNameText = Jisp.getBrand(product);
            if (!this.brandNameText) {
                this.brandNameText = Jisp.getMetaName(document, 'manufacturer');
            }

            var breadCrumbs = Jisp.getInnerTextArr('[itemtype="http://schema.org/BreadcrumbList"] [itemtype="http://schema.org/ListItem"] [itemprop="url"], [itemtype="http://schema.org/BreadcrumbList"] [itemtype="http://schema.org/ListItem"] [itemprop="url"]', ['home']);
            this.categoryNameText = Jisp.extractCategoryName(breadCrumbs);
        }
        else {
            this.except = true;
        }

        return Jisp.getResult(this);
    };
})();
window.parser_imdb_com = new (function () {
    Jisp.init(this);

    this.ready = function () {
        return true;
    };

    this.checkChanges = function () {
        return null;
    };

    this.getPageInfo = function() {

        var product = Jisp.getProductElement();
        if (!product) {
            product = document;
        }

        this.productName = Jisp.getProductName(product);

        this.imageUrl = Jisp.getImage(product);

        var priceInnerText = Jisp.getPrice(product);
        this.priceAmount = Jisp.extractPrice(priceInnerText);
        var currencyInnerText = Jisp.extractCurrency(priceInnerText);
        if(!currencyInnerText){
            currencyInnerText = Jisp.extractCurrency(Jisp.getCurrency(product));
        }
        this.priceCurrency = currencyInnerText;

        this.brandNameText = Jisp.getBrand(product);

        var breadCrumbs = Jisp.getBreadCrumbs(document);
        this.categoryNameText = Jisp.extractCategoryName(breadCrumbs);

        var pageId = Jisp.getAttribute('meta[property="pageId"]', 'content');
        var videoUrl = "http://www.imdb.com/videoembed/" + pageId;

        return Jisp.getResult(this);
    };
})();
window.parser_jdsports_co_uk = new (function () {
    Jisp.init(this);

    this.placeholderImageUrl = "https://jisp.com/Content/img/placeholders/jdsports.co.uk.png";

    this.ready = function () {
        return true;
    };

    this.checkChanges = function () {
        return null;
    };

    this.getPageInfo = function () {

        this.isProductPage = Jisp.isExistsElement('#details') || Jisp.isExistsElement('#productRight') 
            || Jisp.isExistsElement('#productPage');

        this.productName = Jisp.getInnerText('[data-e2e="product-name"]');
        if (!this.productName) {
            this.productName = Jisp.getInnerText('#itemTitles [itemprop="name"]');
        }

        this.imageUrl = Jisp.getAttribute('#gallery .owl-stage .owl-item.active img', 'src');
        if (!this.imageUrl) {
            this.imageUrl = Jisp.getMetaProperty(document, "og:image");
        }

        var priceInnerText = Jisp.getMetaName(document, "twitter:data1");
        this.priceAmount = Jisp.extractPrice(priceInnerText);
        this.priceCurrency = Jisp.extractCurrency(priceInnerText);
        if (!this.priceCurrency) {
            var priceCurrencyInnerText = Jisp.getInnerText('[itemprop="price"]');
            this.priceCurrency = Jisp.extractCurrency(priceCurrencyInnerText);
        }

        this.brandNameText = Jisp.getInnerText('.brand');
        if (!this.brandNameText) {
            this.brandNameText = Jisp.getAttribute('.brandLogo', 'alt');
        }

        var breadCrumbs = Jisp.getInnerTextArr('.breadcrumbs a:not(.home):not(.current)', []);
        if (breadCrumbs.length == 0) {
            breadCrumbs = Jisp.getInnerTextArr('[itemtype="http://schema.org/BreadcrumbList"] [itemprop="itemListElement"] a, [itemtype="https://schema.org/BreadcrumbList"] [itemprop="itemListElement"] a', ['Home'])
        }
        this.categoryNameText = Jisp.extractCategoryName(breadCrumbs);

        this.except = Jisp.getExcept(this);
        this.imageUrl = this.except ? null : this.imageUrl;

        return Jisp.getResult(this);
    };
})();
window.parser_jisp_com = new (function () {
    Jisp.init(this);

    this.ready = function () {
        var imageUrl = Jisp.getMetaProperty(document, "og:image");
        return (!!imageUrl);
    };

    this.checkChanges = function () {
        return null;
    };

    this.getPageInfo = function() {

        var product = Jisp.getProductElement();
        if (!product) {
            product = document;
        }

        if ((this.url.indexOf("shopper/dashboard/item") >= 0) ||
            (this.url.indexOf("shopper/offers") >= 0) ||
            (this.url.indexOf("shopper/coupons") >= 0) ||
            (this.url.indexOf("shopper/storeproducts") >= 0) ||
            (this.url.indexOf("shopper/videos") >= 0) ||
            (this.url.indexOf("shopper/infotags") >= 0)) {
            this.productName = Jisp.getMetaProperty(document, "og:title");
            if (!this.productName) {
                this.productName = Jisp.getMetaProperty(document, "og:description");
            }

            this.imageUrl = Jisp.getMetaProperty(document, "og:image");
        } else {
            this.productName = Jisp.getProductName(product);

            this.imageUrl = Jisp.getImage(product);
        }

        var priceInnerText = Jisp.getPrice(product);
        this.priceAmount = Jisp.extractPrice(priceInnerText);
        var currencyInnerText = Jisp.extractCurrency(priceInnerText);
        if (!currencyInnerText) {
            currencyInnerText = Jisp.extractCurrency(Jisp.getCurrency(product));
        }
        this.priceCurrency = currencyInnerText;

        this.brandNameText = Jisp.getBrand(product);

        var breadCrumbs = Jisp.getBreadCrumbs(document);
        this.categoryNameText = Jisp.extractCategoryName(breadCrumbs);

        return Jisp.getResult(this);
    };
})();
window.parser_johnlewis_com = new (function () {
    Jisp.init(this);
    if (!this.isProductPage) {
        this.url = document.location.href;
    }
    this.url = Jisp.ignoreSharpUrl(this.url);

    this.placeholderImageUrl = "https://jisp.com/Content/img/placeholders/johnlewis.com.png";

    this.ready = function () {
        return true;
    };

    this.checkChanges = function () {
        return null;
    };

    this.getBreadCrumbs = function(product) {
        var breadCrumbAll = [];
        var breadCrumbs = $('ol > li > a', $('[itemprop="breadcrumb"]', product));

        for (var i = 0; i < breadCrumbs.length; i++) {

            if ((breadCrumbs[i].href == "/") || (breadCrumbs[i].innerText == "Home Page")) {
                continue;
            }

            var breadCrumbInnerText = breadCrumbs[i].innerText;
            if (breadCrumbInnerText) {

                if ($(breadCrumbs[i]).parent().hasClass("last")) {
                    //if (breadCrumbInnerText.indexOf('View all ') === 0) {
                    breadCrumbInnerText = breadCrumbInnerText.replace('View all ', '');
                    //}
                }

                breadCrumbAll.push(breadCrumbInnerText);
            }
        }

        return breadCrumbAll;
    };

    this.getPageInfo = function() {

        var product = Jisp.getProductElement();
        if (product) {

            this.productName = Jisp.getInnerText('#prod-title [itemprop="name"]');

            var imageInnerText = Jisp.getAttribute('[itemprop="image"]', 'content');

            if (!imageInnerText) {
                var imageInnerTextElement = $('#prod-media-player .carousel .image').filter(function() {
                    return $(this).css('display') === 'list-item';
                })[0];
                if (imageInnerTextElement) {
                    var imgElement = $('img', $(imageInnerTextElement))[0];
                    if (imgElement) {
                        var attrValue = imgElement.getAttribute('src');
                        if (attrValue) {
                            imageInnerText = attrValue.trim();
                        }
                    }
                }
            }

            if (!imageInnerText) {
                imageInnerText = Jisp.getAttribute('[itemprop="image"]', 'src');
            }

            if (imageInnerText) {
                if (imageInnerText.indexOf("//") == 0) {
                    imageInnerText = "http:" + imageInnerText;
                }

                this.imageUrl = imageInnerText;
            }

            var priceInnerText = Jisp.getPrice(product);
            if (!priceInnerText) {
                priceInnerText = Jisp.getInnerText('#prod-price .price');
            }
            this.priceAmount = Jisp.extractPrice(priceInnerText);
            if ((!this.priceAmount) && (priceInnerText)) {
                priceInnerText = priceInnerText.toLowerCase();
                var nowIndex = priceInnerText.indexOf('now');
                if (nowIndex >= 0) {
                    priceInnerText = priceInnerText.substring(nowIndex + 3);
                    this.priceAmount = Jisp.extractPrice(priceInnerText);
                }
            }
            this.priceCurrency = Jisp.extractCurrency(priceInnerText);
            if (!this.priceCurrency) {
                priceInnerText = Jisp.getInnerText('#prod-price .price');
                this.priceCurrency = Jisp.extractCurrency(priceInnerText);
            }

            this.brandNameText = Jisp.getBrand(product);
            
            var breadCrumbs = this.getBreadCrumbs(document);
            this.categoryNameText = Jisp.extractCategoryName(breadCrumbs); 
        }
        else {
            this.except = true;
        }

        return Jisp.getResult(this);
    };
})();
window.parser_littlewoords_com = new (function () {
    Jisp.init(this);

    this.placeholderImageUrl = "https://jisp.com/Content/img/placeholders/littlewoords.com.png";

    this.ready = function () {
        var image = Jisp.getAttribute('.amp-selected img.amp-just-image', 'src');
        return (image != null);
    };

    this.checkChanges = function () {
        return null;
    };

    this.getBrand = function () {
        var brandName = null;
        var brands = $('.productHeading');
        for (var i = 0; i < brands.length; i++) {
            var brandNameInnerHTML = brands[i].innerHTML;

            if (brandNameInnerHTML) {
                brandName = brandNameInnerHTML.replace(/<span.*>[\w\W]{1,}(.*?)[\w\W]{1,}<\/span>/gi, "");
            }
        }

        return brandName;
    }

    this.getPageInfo = function() {

        var product = Jisp.getProductElement();
        if (!product) {
            product = document;
        }

        this.productName = Jisp.getMetaName(document, "twitter:title");

        this.imageUrl = Jisp.getAttribute('.amp-selected img.amp-just-image', 'src');

        var priceInnerText = Jisp.getPrice(product);
        this.priceAmount = Jisp.extractPrice(priceInnerText);
        var currencyInnerText = Jisp.extractCurrency(priceInnerText);
        if(!currencyInnerText){
            currencyInnerText = Jisp.extractCurrency(Jisp.getCurrency(product));
        }
        this.priceCurrency = currencyInnerText;

        this.brandNameText = this.getBrand();

        var breadCrumbs = Jisp.getInnerTextArr('#breadcrumb a', ['Home', 'Next']);
        this.categoryNameText = Jisp.extractCategoryName(breadCrumbs);

        return Jisp.getResult(this);
    };
})();
window.parser_lookupbubbles_com = new (function () {
    Jisp.init(this);

    this.ready = function () {
        return true;
    };

    this.checkChanges = function () {
        return null;
    };

    this.getPageInfo = function() {

        var product = Jisp.getProductElement();
        if (!product) {
            product = document;
        }

        this.productName = Jisp.getProductName(product);

        this.imageUrl = Jisp.getImage(product);

        var priceInnerText = Jisp.getPrice(product);
        this.priceAmount = Jisp.extractPrice(priceInnerText);
        var currencyInnerText = Jisp.extractCurrency(priceInnerText);
        if(!currencyInnerText){
            currencyInnerText = Jisp.extractCurrency(Jisp.getCurrency(product));
        }
        this.priceCurrency = currencyInnerText;

        this.brandNameText = Jisp.getBrand(product);

        var breadCrumbs = Jisp.getInnerTextArr('[itemtype="http://www.data-vocabulary.org/Breadcrumb"] [itemprop="url"] [itemprop="title"]', []);
        this.categoryNameText = Jisp.extractCategoryName(breadCrumbs);

        return Jisp.getResult(this);
    };
})();
window.parser_mandmdirect_com = new (function () {
    Jisp.init(this);
    this.url = Jisp.ignoreSharpUrl(this.url);

    this.placeholderImageUrl = "https://jisp.com/Content/img/placeholders/mandmdirect.com.png";

    this.ready = function () {
        return true;
    };

    this.checkChanges = function () {
        return null;
    };

    this.getPageInfo = function () {

        this.isProductPage = Jisp.isExistsElement('#mainProductDetails');

        this.productName = Jisp.getAttribute('meta[data-productname]', 'data-productname');

        this.imageUrl = Jisp.getAttribute('#zoompd', 'src');
        
        var priceInnerText = Jisp.getAttribute('meta[data-sellingprice]', 'data-sellingprice');
        this.priceAmount = Jisp.extractPrice(priceInnerText);
        var currencyInnerText = Jisp.getAttribute('meta[data-currency]', 'data-currency');
        this.priceCurrency = Jisp.extractCurrency(currencyInnerText);

        this.brandNameText = Jisp.getAttribute('meta[data-brand]', 'data-brand');
        
        var breadCrumbs = [];
        breadCrumbs.push(Jisp.getAttribute('meta[data-cat]', 'data-cat'));
        breadCrumbs.push(Jisp.getAttribute('meta[data-subcat]', 'data-subcat'));
        this.categoryNameText = Jisp.extractCategoryName(breadCrumbs);

        return Jisp.getResult(this);
    };
})();
window.parser_maplin_co_uk = new (function () {
    Jisp.init(this);
    
    this.placeholderImageUrl = "https://jisp.com/Content/img/placeholders/maplin.co.uk.png";

    this.ready = function () {
        return true;
    };

    this.checkChanges = function () {
        return null;
    };

    this.getPageInfo = function() {

        var product = Jisp.getProductElement();
        if (product) {
            this.productName = Jisp.getProductName(product);

            this.imageUrl = Jisp.getImage(product);

            var priceInnerText = Jisp.getMeta(document, 'itemprop="price"');
            this.priceAmount = Jisp.extractPrice(priceInnerText);
            var currencyInnerText = Jisp.getMeta(document, 'itemprop="priceCurrency"');
            this.priceCurrency = Jisp.extractCurrency(currencyInnerText);

            this.brandNameText = Jisp.getBrand(product);//not found on page

            var breadCrumbs = Jisp.getInnerTextArr('[itemtype="http://data-vocabulary.org/Breadcrumb"]:not([class="active"]) a [itemprop="title"]', ['Home']);
            this.categoryNameText = Jisp.extractCategoryName(breadCrumbs); 
        }
        else {
            this.except = true;
        }

        return Jisp.getResult(this);
    };
})();
window.parser_marksandspecner_com = new (function () {
    Jisp.init(this);
    this.url = Jisp.ignoreSharpUrl(this.url);

    this.placeholderImageUrl = "https://jisp.com/Content/img/placeholders/marksandspecner.com.png";

    this.ready = function () {
        var image = this.getImage();
        return image != null;
    };

    this.checkChanges = function () {
        var productName = this.getProductName();

        return productName;
    };

    this.getProductName = function () {

        var productName = Jisp.getInnerText('.product-title [itemprop="name"]');
        if (!productName) {
            productName = Jisp.getInnerText('.product-title [itemprop="PDPUrl"]');
        }

        return productName;
    }

    this.getBrand = function () {
        var elements = $('[data-brand]');
        for (var i = 0; i < elements.length; i++) {
            var elementInnerText = elements[i].getAttribute('data-brand');
            if (elementInnerText) {
                return elementInnerText;
            }
        }
        return null;
    }

    this.getImage = function () {
        var imageInnerText = null;

        var image = $('.s7staticimage img[src]:not([src=""])');
        if (image) {
            image = image.first()[0];
        }

        if (!image) {
            image = $('#product-detail-page img');
            if (image) {
                image = image.first()[0];
            }
        }

        if (image) {
            imageInnerText = image.getAttribute('src');
        }

        return imageInnerText;
    }
    
    this.getPageInfo = function() {

        var product = $('#product-detail-page[itemtype="http://schema.org/Product"], #product-detail-page[itemtype="https://schema.org/Product"]')[0];
        if (product) {
            this.isProductPage = true;
            this.productName = this.getProductName();

            this.imageUrl = this.getImage();

            if (!this.imageUrl) {
                this.imageUrl = Jisp.getMetaProperty(document, "og:image");
            }

            if (this.imageUrl) {
                if (this.imageUrl.indexOf("//") == 0) {
                    this.imageUrl = "http:" + this.imageUrl;
                }
            }

            if (this.imageUrl) {
                if (/[^\s]*&wid=[\d]*&hei=[\d]*/.test(this.imageUrl)) {
                    this.imageUrl = this.imageUrl.replace(/&wid=[\d]*&hei=[\d]*/, "&wid=2000&hei=2000");
                }
            }

            var priceInnerText = Jisp.getPrice(product);
            if (!priceInnerText) {
                priceInnerText = Jisp.getAttribute('[itemtype="http://schema.org/Product"] [itemprop="price"], [itemtype="https://schema.org/Product"] [itemprop="price"]', "content");
            }
            if (!priceInnerText) {
                priceInnerText = Jisp.getInnerText('[itemtype="http://schema.org/Product"] [data-mapping="price"], [itemtype="https://schema.org/Product"] [data-mapping="price"]');
            }
            if (!priceInnerText) {
                priceInnerText = Jisp.getAttribute('[itemtype="http://schema.org/Product"] [data-mapping="price"], [itemtype="https://schema.org/Product"] [data-mapping="price"]', "data-value");
            }
            
            this.priceAmount = Jisp.extractPrice(priceInnerText);
            this.priceCurrency = Jisp.extractCurrency(priceInnerText);
            if (!this.priceCurrency) {
                var currencyInnerText = Jisp.getAttribute('[itemtype="http://schema.org/Product"] [itemprop="priceCurrency"], [itemtype="https://schema.org/Product"] [itemprop="priceCurrency"]', "content");
                this.priceCurrency = Jisp.extractCurrency(currencyInnerText);
            }

            this.brandNameText = this.getBrand();
            
            var breadCrumbs = Jisp.getInnerTextArr('.breadcrumb a', ['Home']);
            this.categoryNameText = Jisp.extractCategoryName(breadCrumbs); 
        }
        else {
            this.except = true;
        }

        return Jisp.getResult(this);
    };
})();
window.parser_missguided_co_uk = new (function () {
    Jisp.init(this);

    this.placeholderImageUrl = "https://jisp.com/Content/img/placeholders/missguided.co.uk.png";

    this.ready = function () {
        return true;
    };

    this.checkChanges = function () {
        return null;
    };

    this.getPageInfo = function () {

        this.isProductPage = Jisp.isExistsElement('[itemtype="http://schema.org/Product"].product-view, [itemtype="https://schema.org/Product"].product-view');

        var product = Jisp.getProductElement();
        if (!product || !this.isProductPage) {
            product = document;
        }

        this.productName = Jisp.getProductName(product);

        this.imageUrl = Jisp.getAttribute('.product-images-list__item--active img[title]', 'src');
        if (!this.imageUrl) {
            this.imageUrl = Jisp.getImage(product);
        }

        var priceInnerText = Jisp.getPrice(product);
        this.priceAmount = Jisp.extractPrice(priceInnerText);
        var currencyInnerText = Jisp.extractCurrency(priceInnerText);
        if (!currencyInnerText) {
            currencyInnerText = Jisp.extractCurrency(Jisp.getCurrency(product));
        }
        this.priceCurrency = currencyInnerText;

        this.brandNameText = Jisp.getBrand(product);

        var breadCrumbs = Jisp.getBreadCrumbs(document);
        this.categoryNameText = Jisp.extractCategoryName(breadCrumbs);

        return Jisp.getResult(this);
    };
})();
window.parser_newlook_com = new (function () {
    Jisp.init(this);

    this.placeholderImageUrl = "https://jisp.com/Content/img/placeholders/newlook.com.png";

    this.ready = function () {
        return true;
    };

    this.checkChanges = function () {
        return null;
    };

    this.getPageInfo = function () {

        this.isProductPage = Jisp.isExistsElement('#productDetailForm');

        this.productName = Jisp.getMetaProperty(document, "og:title");

        this.imageUrl = Jisp.getAttribute('.main-image', 'src');
        if (!this.imageUrl) {
            this.imageUrl = Jisp.getMetaProperty(document, "og:image");
        }
        if (this.imageUrl && this.imageUrl.indexOf('//') === 0) {
            this.imageUrl = 'http:' + this.imageUrl;
        }

        var priceInnerText = Jisp.getMetaProperty(document, "og:price:amount");
        this.priceAmount = Jisp.extractPrice(priceInnerText);
        var currencyInnerText = Jisp.getMetaProperty(document, "og:price:currency");
        this.priceCurrency = Jisp.extractCurrency(currencyInnerText);

        this.brandNameText = Jisp.getBrand(document);
        if (!this.brandNameText) {
            this.brandNameText = 'New Look';
        }

        var breadCrumbs = Jisp.getInnerTextArr('.breadcrumb a', ['Back', 'Home']);
        this.categoryNameText = Jisp.extractCategoryName(breadCrumbs);


        return Jisp.getResult(this);
    };
})();
window.parser_next_co_uk = new (function () {
    Jisp.init(this);

    this.placeholderImageUrl = "https://jisp.com/Content/img/placeholders/next.co.uk.png";

    this.ready = function () {
        var breadCrumbs = this.getBreadCrumbs(document);

        var priceInnerText = Jisp.getInnerText('.ProductDetail article.Selected .Price');
        if (!priceInnerText) {
            priceInnerText = this.getPrice(document);
        }
        return breadCrumbs.length > 0 && (!!priceInnerText);
    };

    this.checkChanges = function () {
        return null;
    };

    this.getPrice = function (product) {

        var prices = $('.Price', product);

        for (var i = 0; i < prices.length; i++) {

            if ($(prices[i]).parent().hasClass("StyleCopy")) {
                var priceTextFinded = prices[i].innerText;
                if (priceTextFinded) {
                    return priceTextFinded;
                }
            }  
        }

        var price = Jisp.getInnerText('.selectedPrice .price');
        if (price) {
            return price;
        }

        price = Jisp.getInnerText('[ng-app="modularApp"] .price strong');
        if (price) {
            return price;
        }

        return null;
    }

    this.getBreadCrumbs = function (product) {
        var breadCrumbAll = [];
        var breadCrumbs = $('a', $('ul.Breadcrumbs', $(".BreadcrumbNavigation", product)));

        for (var i = 0; i < breadCrumbs.length; i++) {
            var breadCrumbInner = breadCrumbs[i].innerText;

            if (breadCrumbInner) {
                breadCrumbInner = breadCrumbInner.trim();

                if ($(breadCrumbs[i]).parent().hasClass("bcHome")) {
                    continue;
                }

                //+2 filters
                if (breadCrumbInner.indexOf('+') == 0) {
                    continue;
                }

                if (breadCrumbInner.indexOf('Page ') < 0) {
                    breadCrumbAll.push(breadCrumbInner.replace(/"/g, ''));
                }
            }
        }

        return breadCrumbAll;
    }

    this.getPageInfo = function () {
        this.isProductPage = Jisp.isExistsElement('.ProductDetail');

        this.productName = Jisp.getInnerText('.ProductDetail article.Selected .Title h2');

        if (!this.productName) {
            this.productName = Jisp.getInnerText('.Title h1');
        }
        
        if (!this.productName) {
            this.productName = Jisp.getInnerText('.productDetails .details h2');
        }

        this.imageUrl = Jisp.getAttribute('#ShotView img', 'src');
        if (!this.imageUrl) {
            this.imageUrl = Jisp.getAttribute('.productDetails .images img', 'src');
        }

        var priceInnerText = Jisp.getInnerText('.ProductDetail article.Selected .Price');
        if (!priceInnerText) {
            priceInnerText = this.getPrice(document);
        }
        this.priceAmount = Jisp.extractPrice(priceInnerText);
        this.priceCurrency = Jisp.extractCurrency(priceInnerText);

        this.brandNameText = Jisp.getBrand(document);

        var breadCrumbs = this.getBreadCrumbs(document);
        this.categoryNameText = Jisp.extractCategoryName(breadCrumbs);

        return Jisp.getResult(this);
    };
})();
window.parser_notonthehighstreet_com = new (function () {
    Jisp.init(this);

    this.url = Jisp.ignoreSharpUrl(this.url);

    this.placeholderImageUrl = "https://jisp.com/Content/img/placeholders/notonthehighstreet.com.png";

    this.ready = function () {
        return true;
    };

    this.checkChanges = function () {
        return null;
    };

    this.getPageInfo = function () {

        this.isProductPage = Jisp.isExistsElement('#product');

        this.productName = Jisp.getMetaProperty(document, "og:title");
        if (!this.productName) {
            this.productName = Jisp.getInnerText('.product_title');
        }

        this.imageUrl = Jisp.getAttribute('.product_main_image_container img.shown_image', 'src');
        if (!this.imageUrl) {
            this.imageUrl = Jisp.getMetaProperty(document, "og:image");
        }

        var priceInnerText = Jisp.getAttribute('#price .currency_GBP', 'data-current-price');
        if (!priceInnerText) {
            priceInnerText = Jisp.getMetaName(document, "product:price:amount");
        }

        this.priceAmount = Jisp.extractPrice(priceInnerText);
        var currencyInnerText = Jisp.getMetaName(document, "product:price:currency");
        if (!currencyInnerText) {
            currencyInnerText = Jisp.getInnerText('#price .currency_GBP .price_unit');
        }
        this.priceCurrency = Jisp.extractCurrency(currencyInnerText);

        this.brandNameText = Jisp.getInnerText('#product_header .by a');

        var breadCrumbs = [];//Jisp.getInnerTextArr('[itemprop="breadcrumb"] a', ['HOMEPAGE']);
        this.categoryNameText = Jisp.extractCategoryName(breadCrumbs); 

        return Jisp.getResult(this);
    };
})();
window.parser_pcworld_co_uk = new (function () {
    Jisp.init(this);

    this.placeholderImageUrl = "https://jisp.com/Content/img/placeholders/pcworld.co.uk.png";

    this.ready = function () {
        return true;
    };

    this.checkChanges = function () {
        return null;
    };

    this.getImage = function () {
        var imageUrl = null;

        var image = $('#product-main .swiper-slide-active .product-image');
        if (!image) {
            image = $('[itemprop="image"][src]:not([src=""])');
        }

        if (image) {
            image = image.first()[0];
        }

        if (image) {
            imageUrl = image.getAttribute('src');
        }

        if (!imageUrl) {
            imageUrl = Jisp.getMetaName(document, "twitter:image");
        }

        if (!imageUrl) {
            imageUrl = Jisp.getMetaProperty(document, "og:image");
        }

        if (!imageUrl) {
            imageUrl = Jisp.getMetaName(document, "og:image");
        }

        return (imageUrl) ? imageUrl : null;
    };

    this.getPageInfo = function() {

        var product = Jisp.getProductElement();

        this.isProductPage = (product) || Jisp.isExistsElement('.product-page');

        if (!product) {
            product = document;
        }

        this.productName = Jisp.getMetaProperty(document, "og:title");

        this.imageUrl = this.getImage(product);

        var priceInnerText = Jisp.getPrice(product);
        if (!priceInnerText) {
            priceInnerText = Jisp.getInnerText('.product-page [data-key="current-price"]');
        }
        this.priceAmount = Jisp.extractPrice(priceInnerText);
        var currencyInnerText = Jisp.extractCurrency(priceInnerText);
        if(!currencyInnerText){
            currencyInnerText = Jisp.extractCurrency(Jisp.getCurrency(product));
        }
        this.priceCurrency = currencyInnerText;

        this.brandNameText = Jisp.getBrand(product);

        var breadCrumbs = Jisp.getBreadCrumbs(document);
        this.categoryNameText = Jisp.extractCategoryName(breadCrumbs);

        return Jisp.getResult(this);
    };
})();
window.parser_riverisland_com = new (function () {
    Jisp.init(this);
    if (document.location.hostname == 'eu.riverisland.com') {
        if (this.url) {
            this.url = this.url.replace('eu.riverisland.com', 'www.riverisland.com');
        }
    }

    this.placeholderImageUrl = "https://jisp.com/Content/img/placeholders/riverisland.com.png";

    this.ready = function () {
        return true;
    };

    this.checkChanges = function () {
        return null;
    };

    this.getPrice = function () {
        var priceText = null;

        var prices = $('.bundle-parent .price .sale');
        for (var i = 0; i < prices.length; i++) {
            var priceTextFinded = prices[i].innerHTML;
            if (priceTextFinded) {
                priceText = priceTextFinded.replace(/<span.*>[\w\W]{1,}(.*?)[\w\W]{1,}<\/span>/gi, "").trim();
            }
        }

        if (!priceText) {
            priceText = Jisp.getInnerText('.bundle-parent .price');
        }

        if (!priceText) {
            prices = $('.product-details-container .right-side .price .sale')

            for (var i = 0; i < prices.length; i++) {
                var priceTextFinded = prices[i].innerHTML;
                if (priceTextFinded) {
                    priceText = priceTextFinded.replace(/<span.*>[\w\W]{1,}(.*?)[\w\W]{1,}<\/span>/gi, "").trim();
                }
            }
        }

        if (!priceText) {
            prices = $('.product-details-container .right-side .price span');

            for (var i = 0; i < prices.length; i++) {
                var priceTextFinded = prices[i].innerHTML;
                if (priceTextFinded) {
                    priceText = priceTextFinded.replace(/<span.*>[\w\W]{1,}(.*?)[\w\W]{1,}<\/span>/gi, "").trim();
                }
            }
        }

        return priceText;
    }

    this.getPageInfo = function () {

        this.isProductPage = Jisp.isExistsElement('.product-details-container');

        if (!this.isProductPage) {
            this.url = document.location.href;
        }

        this.productName = Jisp.getMetaProperty(document, "og:title");

        this.imageUrl = Jisp.getAttribute('.main-image-container img', 'src');
        //if (!this.imageUrl) {
        //    this.imageUrl = Jisp.getImage(document);
        //}

        var priceInnerText = this.getPrice();
        this.priceAmount = Jisp.extractPrice(priceInnerText);
        this.priceCurrency = Jisp.extractCurrency(priceInnerText);

        this.brandNameText = 'River Island';

        var breadCrumbs = Jisp.getInnerTextArr('.breadcrumbs a', ['Home']);
        this.categoryNameText = Jisp.extractCategoryName(breadCrumbs);

        return Jisp.getResult(this);
    };
})();
window.parser_screwfix_com = new (function () {
    Jisp.init(this);

    this.placeholderImageUrl = "https://jisp.com/Content/img/placeholders/screwfix.com.png";

    this.ready = function () {
        return true;
    };

    this.checkChanges = function () {
        return null;
    };

    this.getPrice = function (product) {
        var prices = $('[itemprop="price"]', product);

        for (var i = 0; i < prices.length; i++) {
            var productNameTextFinded = prices[i].getAttribute('content');
            if (productNameTextFinded) {
                return productNameTextFinded;
            }
        }

        return null;
    }

    this.getCurrency = function () {
        var elements = $('[itemprop="priceCurrency"]');
        for (var i = 0; i < elements.length; i++) {
            var elementInnerText = elements[i].getAttribute('content');
            if (elementInnerText) {
                return elementInnerText.trim();
            }
        }
        return null;
    }

    this.getBrand = function () {
        var elements = $('#product_selling_attributes_table th').filter(function () {
            return $(this).text().toLowerCase().indexOf('Brand'.toLowerCase()) >= 0;
        });
        elements = elements.next();

        if (elements.length > 0) {
            for (var i = 0; i < elements.length; i++) {
                var elementInnerText = elements[i].innerText;
                if (elementInnerText) {
                    return elementInnerText;
                }
            }
        }

        elements = $('.brand');
        if (elements.length > 0) {
            var elementInnerText = elements[0].getAttribute('alt');
            if (elementInnerText) {
                return elementInnerText;
            }
        }
        
        return null;
    }

    this.getPageInfo = function() {

        var product = Jisp.getProductElement();
        if (product) {
            this.productName = Jisp.getProductName(product);

            this.imageUrl = Jisp.getAttribute('#product-slider-for .slick-active img', 'src');
            if (!this.imageUrl) {
                this.imageUrl = Jisp.getMetaProperty(document, "og:image");
            }

            var priceInnerText = this.getPrice(document);
            this.priceAmount = Jisp.extractPrice(priceInnerText);  
            var currencyInnerText = this.getCurrency();
            this.priceCurrency = Jisp.extractCurrency(currencyInnerText);

            this.brandNameText = this.getBrand();
            
            var breadCrumbs = Jisp.getInnerTextArr('[itemtype="http://data-vocabulary.org/Breadcrumb"] a', ['Home']);
            this.categoryNameText = Jisp.extractCategoryName(breadCrumbs); 
        }
        else {
            this.except = true;
        }

        return Jisp.getResult(this);
    };
})();
window.parser_selfridges_com = new (function () {
    Jisp.init(this);

    var linkUrl = Jisp.getAttribute('link[rel="alternate"][hreflang="en-GB"]', 'href');
    if (!linkUrl) {
        linkUrl = Jisp.getAttribute('link[rel="canonical"]', 'href');
    }
    this.url = (linkUrl) ? linkUrl : this.url;

    this.placeholderImageUrl = "https://jisp.com/Content/img/placeholders/selfridges.com.png";

    this.ready = function () {
        return true;
    };

    this.checkChanges = function () {
        return null;
    };

    this.getImage = function () {
        var imageUrl = null;
        var image = $('[itemprop="image"][src]:not([src=""])');
        if (image) {
            image = image.first()[0];
        }

        if (image) {
            imageUrl = image.getAttribute('src');
        }

        if (!imageUrl) {
            imageUrl = Jisp.getMetaName(document, "twitter:image");
        }

        if (!imageUrl) {
            imageUrl = Jisp.getMetaProperty(document, "og:image");
        }

        if (!imageUrl) {
            imageUrl = Jisp.getMetaName(document, "og:image");
        }

        return (imageUrl) ? imageUrl : null;
    };

    this.getPageInfo = function() {

        var product = Jisp.getProductElement();
        if (product) {
            
            var priceInnerText = Jisp.getPrice(product);
            this.priceAmount = Jisp.extractPrice(priceInnerText);

            if (this.priceAmount == null) {
                priceInnerText = Jisp.getInnerText('.price');
                this.priceAmount = Jisp.extractPrice(priceInnerText);
            }

            var currencyInnerText = Jisp.extractCurrency(priceInnerText);
            if (!currencyInnerText) {
                currencyInnerText = Jisp.extractCurrency(Jisp.getAttribute('[itemprop="priceCurrency"]', 'content'));
            }

            this.priceCurrency = currencyInnerText;
        }
        else {
            product = document;
        }

        this.productName = Jisp.getProductName(product);

        this.imageUrl = this.getImage(product);

        this.brandNameText = Jisp.getBrand(product);

        var breadCrumbs = Jisp.getInnerTextArr('[itemprop="breadcrumb"] a', ['Home']);
        this.categoryNameText = Jisp.extractCategoryName(breadCrumbs);

        return Jisp.getResult(this);
    };
})();
window.parser_tesco_com = new (function () {
    Jisp.init(this);

    this.placeholderImageUrl = "https://jisp.com/Content/img/placeholders/tesco.com.png";

    this.ready = function () {
        return true;
    };

    this.checkChanges = function () {
        return null;
    };

    this.getPageInfo = function () {

        var product = Jisp.getProductElement();
        if (!product) {
            product = document;
        }

        this.productName = Jisp.getProductName(product);

        var elements = $('#prodZoomView .s7staticimage img').not('#prodZoomView .s7staticimage img[style*="visibility: hidden"]');
        if (elements) {
            for (var i = 0; i < elements.length; i++) {
                var attrValue = elements[i].getAttribute('src');
                if (attrValue) {
                    this.imageUrl = attrValue.trim();
                }
            }
        }

        if (!this.imageUrl) {
            this.imageUrl = Jisp.getImage(product);
        }

        var priceInnerText = Jisp.getPrice(product);
        this.priceAmount = Jisp.extractPrice(priceInnerText);
        var currencyInnerText = Jisp.extractCurrency(priceInnerText);
        if (!currencyInnerText) {
            currencyInnerText = Jisp.extractCurrency(Jisp.getCurrency(product));
        }
        this.priceCurrency = currencyInnerText;

        this.brandNameText = Jisp.getBrand(product);

        var breadCrumbs = Jisp.getBreadCrumbs(document);
        this.categoryNameText = Jisp.extractCategoryName(breadCrumbs);

        return Jisp.getResult(this);
    };
})();
window.parser_tkmaxx_com = new (function () {
    Jisp.init(this);
    this.url = Jisp.ignoreSharpUrl(this.url);

    this.placeholderImageUrl = "https://jisp.com/Content/img/placeholders/tkmaxx.com.png";

    this.ready = function () {
        return true;
    };

    this.checkChanges = function () {
        return null;
    };

    this.getPageInfo = function() {

        if (!this.isProductPage) {
            this.url = document.location.href;
        }

        var product = Jisp.getProductElement();
        if (product) {
            this.productName = Jisp.getProductName(product);

            this.imageUrl = Jisp.getAttribute('#productdetail-image img', 'src');
            if (!this.imageUrl) {
                this.imageUrl = Jisp.getMetaProperty(document, "og:image");
            }

            var priceInnerText = Jisp.getPrice(product);
            this.priceAmount = Jisp.extractPrice(priceInnerText);
            this.priceCurrency = Jisp.extractCurrency(priceInnerText);

            this.brandNameText = Jisp.getInnerText('.product-brand');
            
            var breadCrumbs = Jisp.getInnerTextArr('.crumbtrail a.crumbtrail-anchor', []);
            breadCrumbs = breadCrumbs.map(function (name) {
                return name.replace('View All:', '').trim();
            });
            this.categoryNameText = Jisp.extractCategoryName(breadCrumbs); 
        }
        else {
            this.except = true;
        }

        return Jisp.getResult(this);
    };
})();
window.parser_topshop_com = new (function () {
    Jisp.init(this);
    if (this.url == document.location.href) {
        var customCanonicalUrl = Jisp.getInnerText('#un_pdp_canonical');
        if (customCanonicalUrl) {
            this.url = customCanonicalUrl;
        }
    }

    this.placeholderImageUrl = "https://jisp.com/Content/img/placeholders/topshop.com.png";

    this.ready = function () {
        var ready = false;
        if (Jisp.isExistsElement('#cboxLoadedContent')) {
            var productName = Jisp.getInnerText('#cboxLoadedContent .product_name');
            var productPrice = Jisp.getInnerText('#cboxLoadedContent .product_price');
            ready = (!!productName) && (!!productPrice);
        } else {
            var breadCrumbs = Jisp.getInnerTextArr('[itemtype="http://data-vocabulary.org/Breadcrumb"] a', ['Home']);
            ready = (breadCrumbs.length > 0);
        }
        
        return ready;
    };

    this.checkChanges = function () {
        var productName = Jisp.getInnerText('#cboxLoadedContent .product_name');

        return productName;
    };

    this.getPageInfo = function () {

        var isModalWindow = Jisp.isExistsElement('#cboxLoadedContent');
        this.isProductPage = Jisp.isExistsElement('#product-detail') || isModalWindow;

        if (isModalWindow) {
            var productUrl = Jisp.getAttribute('#cboxLoadedContent #fullProductBtn a', 'href');
            if (productUrl)
                this.url = productUrl;
        }

        this.productName = Jisp.getInnerText('#cboxLoadedContent .product_name');
        if (!this.productName) {
            this.productName = Jisp.getMetaProperty(document, "og:description");
        }

        this.imageUrl = Jisp.getAttribute('.product_hero__image li.current img', 'src');
        if (!this.imageUrl) {
            this.imageUrl = Jisp.getMetaProperty(document, "og:image");
        }

        var priceInnerText = Jisp.getInnerText('#cboxLoadedContent .product_price');
        if (!priceInnerText) {
            priceInnerText = Jisp.getMetaProperty(document, "og:price:amount");
        }

        this.priceAmount = Jisp.extractPrice(priceInnerText);
        this.priceCurrency = Jisp.extractCurrency(priceInnerText);
        if (!this.priceCurrency) {
            var currencyInnerText = Jisp.getMetaProperty(document, "og:price:currency");
            this.priceCurrency = Jisp.extractCurrency(currencyInnerText);
        }

        this.brandNameText = Jisp.getBrand(document);

        var breadCrumbs = Jisp.getInnerTextArr('[itemtype="http://data-vocabulary.org/Breadcrumb"] a', ['Home']);
        this.categoryNameText = Jisp.extractCategoryName(breadCrumbs);

        return Jisp.getResult(this);
    };
})();
window.parser_twitter_com = new (function () {
    Jisp.init(this);

    this.ready = function () {
        var imageUrl = this.getImage();
        return (!!imageUrl);
    };

    this.checkChanges = function () {
        return null;
    };

    this.getProductName = function () {
        var iframe = $(".tweet iframe");
        if (iframe) {
            var img = iframe.contents().find('.TwitterCard-title');

            for (var i = 0; i < img.length; i++) {
                var attrValue = img[i].innerText;
                if (attrValue) {
                    return attrValue.trim();
                }
            }
        }

        var elements = $('.permalink .tweet .tweet-text');

        if ((!elements) || ((elements) && (elements.length === 0)))
        {
            elements = $('.Gallery .tweet-text');
        }

        if (elements) {
            for (var i = 0; i < elements.length; i++) {
                var elementInnerText = elements[i].innerHTML;
                if (elementInnerText.indexOf('<') >= 0) {
                    return elementInnerText.substr(0, elementInnerText.indexOf('<')).trim();
                }
            }
        }

        return null;
    };

    this.getImage = function () {
        var iframe = $(".tweet iframe");
        if (iframe) {
            var img = iframe.contents().find('.SummaryCard-image img');

            for (var i = 0; i < img.length; i++) {
                var attrValue = img[i].getAttribute('src');
                if (attrValue) {
                    return attrValue.trim();
                }
            }
        }

        var imgUrl = Jisp.getAttribute('.Gallery .Gallery-media img', 'src');

        if (!imgUrl) {
            imgUrl = Jisp.getAttribute('#permalink-overlay-body [data-element-context="platform_photo_card"] img', 'src');
        }

        return imgUrl;
    };

    this.getPageInfo = function() {

        var product = Jisp.getProductElement();
        if (!product) {
            product = document;
        }

        this.productName = this.getProductName();
        if (!this.productName) {
            this.productName = Jisp.getProductName(product);
        }

        this.imageUrl = this.getImage();
        if (!this.imageUrl) {
            this.imageUrl = Jisp.getImage(product);
        }

        var priceInnerText = Jisp.getPrice(product);
        this.priceAmount = Jisp.extractPrice(priceInnerText);
        var currencyInnerText = Jisp.extractCurrency(priceInnerText);
        if(!currencyInnerText){
            currencyInnerText = Jisp.extractCurrency(Jisp.getCurrency(product));
        }
        this.priceCurrency = currencyInnerText;

        this.brandNameText = Jisp.getBrand(product);

        var breadCrumbs = Jisp.getBreadCrumbs(document);
        this.categoryNameText = Jisp.extractCategoryName(breadCrumbs);

        return Jisp.getResult(this);
    };
})();
window.parser_very_co_uk = new (function () {
    Jisp.init(this);
    this.url = Jisp.ignoreSharpUrl(this.url);

    this.placeholderImageUrl = "https://jisp.com/Content/img/placeholders/very.co.uk.png";

    this.ready = function () {
        var image = this.getImageUrl();
        return (!!image);
    };

    this.checkChanges = function () {
        return null;
    };

    this.getImageUrl = function() {
        var imageUrl = Jisp.getAttribute('.amp-selected img.amp-just-image', 'src');
        if (!imageUrl) {
            var imageUrlEncoded = Jisp.getMetaProperty(document, "og:image");
            if (imageUrlEncoded) {
                imageUrl = decodeURIComponent(imageUrlEncoded);
            }
        }

        return imageUrl;
    };

    this.getBrand = function() {
        var brandNameTextArr = [];
        var brands = $('#moreFrom-brandStoreLink a');

        if (brands.length > 0) {
            for (var i = 0; i < brands.length; i++) {
                var brandNameInner = brands[i].innerText;

                if (brandNameInner) {
                    brandNameTextArr.push(brandNameInner.trim());
                }
            }
        } else {
            brands = $('.productHeading');
            if (brands.length > 0) {
                var brandNameInner = brands[0].innerHTML;
                if (brandNameInner) {
                    brandNameTextArr.push(brandNameInner.replace(/<span.*>[\w\W]{1,}(.*?)[\w\W]{1,}<\/span>/gi, "")
                        .trim());
                }
            }
        }

        return brandNameTextArr.join(',');
    };

    this.getPageInfo = function() {

        var product = Jisp.getProductElement();
        if (product) {
            this.productName = Jisp.getMetaProperty(document, "og:title");

            this.imageUrl = this.getImageUrl();

            var priceInnerText = Jisp.getMetaProperty(document, "product:price:amount");
            this.priceAmount = Jisp.extractPrice(priceInnerText);
            var currencyInnerText = Jisp.getMetaProperty(document, "og:price:currency");
            this.priceCurrency = Jisp.extractCurrency(currencyInnerText);

            this.brandNameText = this.getBrand();
            
            var breadCrumbs = Jisp.getInnerTextArr('[itemtype="http://schema.org/BreadcrumbList"] [itemtype="http://schema.org/ListItem"] [itemprop="item"] [itemprop="name"]', ['Home']);
            this.categoryNameText = Jisp.extractCategoryName(breadCrumbs); 
        }
        else {
            this.except = true;
        }

        return Jisp.getResult(this);
    };
})();
window.parser_vimeo_com = new (function () {
    Jisp.init(this);

    this.ready = function () {
        return true;
    };

    this.checkChanges = function () {
        return null;
    };

    this.getPageInfo = function() {

        var product = Jisp.getProductElement();
        if (!product) {
            product = document;
        }

        this.productName = Jisp.getProductName(product);

        this.imageUrl = Jisp.getImage(product);

        var priceInnerText = Jisp.getPrice(product);
        this.priceAmount = Jisp.extractPrice(priceInnerText);
        var currencyInnerText = Jisp.extractCurrency(priceInnerText);
        if(!currencyInnerText){
            currencyInnerText = Jisp.extractCurrency(Jisp.getCurrency(product));
        }
        this.priceCurrency = currencyInnerText;

        this.brandNameText = Jisp.getBrand(product);

        var breadCrumbs = Jisp.getBreadCrumbs(document);
        this.categoryNameText = Jisp.extractCategoryName(breadCrumbs);

        var videoUrl = Jisp.getAttribute('meta[name="twitter:player"]', 'content');

        return Jisp.getResult(this);
    };
})();
window.parser_youtube_com = new (function () {
    Jisp.init(this);

    this.ready = function () {
        return true;
    };

    this.checkChanges = function () {
        return null;
    };

    this.getPageInfo = function() {

        var product = Jisp.getProductElement();
        if (!product) {
            product = document;
        }

        this.productName = Jisp.getInnerText('#eow-title'); //Jisp.getProductName(product);

        this.imageUrl = Jisp.getAttribute('[itemprop="thumbnailUrl"]', 'href'); //Jisp.getImage(product);

        var priceInnerText = Jisp.getPrice(product);
        this.priceAmount = Jisp.extractPrice(priceInnerText);
        var currencyInnerText = Jisp.extractCurrency(priceInnerText);
        if(!currencyInnerText){
            currencyInnerText = Jisp.extractCurrency(Jisp.getCurrency(product));
        }
        this.priceCurrency = currencyInnerText;

        this.brandNameText = Jisp.getBrand(product);

        var breadCrumbs = Jisp.getBreadCrumbs(document);
        this.categoryNameText = Jisp.extractCategoryName(breadCrumbs);

        var videoUrl = Jisp.getAttribute('[itemprop="embedURL"]', 'href');
        

        return Jisp.getResult(this);
    };
})();
window.parser_zara_com = new (function () {
    Jisp.init(this);

    this.placeholderImageUrl = "https://jisp.com/Content/img/placeholders/zara.com.png";

    this.ready = function () {
        var imageUrl = Jisp.getAttribute('#main-images img', 'src');

        return ((imageUrl != null) && (imageUrl != '') && ( imageUrl.indexOf('data:') != 0));
    };

    this.checkChanges = function () {
        return null;
    };

    this.getProductName = function (selector) {
        var elements = $(selector);
        if (elements) {
            for (var i = 0; i < elements.length; i++) {
                var elementInnerText = elements[i].innerHTML;
                if (elementInnerText) {
                    return elementInnerText.trim().replace(/<span.*>[\w\W]{1,}(.*?)[\w\W]{1,}<\/span>/gi, "");
                }
            }
        }
        return null;
    }

    this.getPrice = function () {
        var elements = $('#product .right .price').not('#product .related-product .price');
        if (elements) {
            for (var i = 0; i < elements.length; i++) {
                var elementInnerText = elements[i].innerText;
                if (elementInnerText) {
                    return elementInnerText.trim().replace(',', '.').replace(String.fromCharCode(160), '');
                }
            }
        }
        return null;
    }

    this.getPageInfo = function () {

        var existsList = Jisp.isExistsElement('.product-list');
        var product = $('#product')[0];
        if (product && (!existsList)) {
            this.isProductPage = true;

            this.productName = this.getProductName('#product header h1');
            if (!this.productName) {
                this.productName = this.getProductName('#product ._bundle-detail-actions .bundle-item-description .name');
            }

            if (!this.productName) {
                this.productName = Jisp.getInnerText('#product header');
            }

            this.imageUrl = Jisp.getAttribute('#main-images img', 'src');
            if ((this.imageUrl) && (this.imageUrl.indexOf('data:') == 0)) {
                this.imageUrl = Jisp.getAttribute('#main-images a._seoImg', 'href');
                if ((this.imageUrl) && (this.imageUrl.indexOf('data:') == 0)) {
                    this.imageUrl = null;
                }
            }
            this.imageUrl = Jisp.addProtocolToUrl(this.imageUrl);

            var priceInnerText = this.getPrice();
            this.priceAmount = Jisp.extractPrice(priceInnerText);
            this.priceCurrency = Jisp.extractCurrency(priceInnerText);

            this.brandNameText = 'Zara';

            var breadCrumbs = Jisp.getInnerTextArr('[itemtype="http://data-vocabulary.org/Breadcrumb"] [itemprop="url"] [itemprop="title"]', ['ZARA', 'View all']);
            this.categoryNameText = Jisp.extractCategoryName(breadCrumbs);
        }
        else {
            this.except = true;
        }

        return Jisp.getResult(this);
    };
})();
function initJispParser () {

    var hostNames = [
                    "www.amazon.co.uk",
                    "www.ebay.co.uk",
                    "www.argos.co.uk",
                    "www.next.co.uk",
                    "www3.next.co.uk",
                    "www.asos.com",
                    "www.johnlewis.com",
                    "www.marksandspencer.com",
                    "www.currys.com",
                    "www.currys.co.uk",
                    "www.boots.com",
                    "www.debenhams.com",
                    "www.very.co.uk",
                    "www.ikea.com",
                    "www.newlook.com",
                    "www.houseoffraser.co.uk",
                    "www.diy.com",
                    "www.screwfix.com",
                    "www.notonthehighstreet.com",
                    "www2.hm.com",
                    "www.pcworld.co.uk",
                    "www.topshop.com",
                    "www.ebuyer.com",
                    "www.zara.com",
                    "www.jdsports.co.uk",
                    "www.littlewoods.com",
                    "www.lookupbubbles.com",
                    "www.boohoo.com",
                    "www.tkmaxx.com",
                    "www.selfridges.com",
                    "www.homebase.co.uk",
                    "www.maplin.co.uk",
                    "www.halfords.com",
                    "www.riverisland.com",
                    "www.tesco.com",
                    "www.missguided.co.uk",
                    "www.mandmdirect.com",
                    "www.boden.co.uk",
                    "en.wikipedia.org",
                    "www.youtube.com",
                    "vimeo.com",
                    "www.dailymotion.com",
                    "www.imdb.com",
                    "twitter.com",
                    "www.jisp.com"
    ];

    var aliases = {};
    aliases['m.topshop.com'] = 'www.topshop.com';
    aliases['eu.riverisland.com'] = 'www.riverisland.com';
    aliases['m2.hm.com'] = 'www2.hm.com';
    aliases['m.next.co.uk'] = 'www.next.co.uk';
    aliases['m.boots.com'] = 'www.boots.com';
    aliases['m.asos.com'] = 'www.asos.com';
    aliases['m.debenhams.com'] = 'www.debenhams.com';
    aliases['m.ikea.com'] = 'www.ikea.com';
    aliases['m.newlook.com'] = 'www.newlook.com';
    aliases['m.ebuyer.com'] = 'www.ebuyer.com';
    aliases['m.jdsports.co.uk'] = 'www.jdsports.co.uk';
    aliases['stage.jisp.com'] = 'www.jisp.com';
    aliases['shopper-stage.jisp.com'] = 'www.jisp.com';
    aliases['shopper.jisp.com'] = 'www.jisp.com';

    var hostname = document.location.hostname;
    hostname = aliases[hostname] || hostname;

    var host = (hostNames.indexOf(hostname) >= 0) ?
        hostname.replace('www.', '').replace('www2.', '').replace('www3.', '').replace(/\./g, '_') :
            'defaultProductParser';

    return window["parser_" + host] || window["parser_defaultProductParser"];
};

window.jisp_parser = initJispParser();