window.parser_jdsports_co_uk = new (function () {
    Jisp.init(this);

    this.placeholderImageUrl = "https://jisp.com/Content/img/placeholders/jdsports.co.uk.png";

    this.ready = function () {
        return true;
    };

    this.checkChanges = function () {
        return null;
    };

    this.getPageInfo = function () {

        this.isProductPage = Jisp.isExistsElement('#details') || Jisp.isExistsElement('#productRight') 
            || Jisp.isExistsElement('#productPage');

        this.productName = Jisp.getInnerText('[data-e2e="product-name"]');
        if (!this.productName) {
            this.productName = Jisp.getInnerText('#itemTitles [itemprop="name"]');
        }

        this.imageUrl = Jisp.getAttribute('#gallery .owl-stage .owl-item.active img', 'src');
        if (!this.imageUrl) {
            this.imageUrl = Jisp.getMetaProperty(document, "og:image");
        }

        var priceInnerText = Jisp.getMetaName(document, "twitter:data1");
        this.priceAmount = Jisp.extractPrice(priceInnerText);
        this.priceCurrency = Jisp.extractCurrency(priceInnerText);
        if (!this.priceCurrency) {
            var priceCurrencyInnerText = Jisp.getInnerText('[itemprop="price"]');
            this.priceCurrency = Jisp.extractCurrency(priceCurrencyInnerText);
        }

        this.brandNameText = Jisp.getInnerText('.brand');
        if (!this.brandNameText) {
            this.brandNameText = Jisp.getAttribute('.brandLogo', 'alt');
        }

        var breadCrumbs = Jisp.getInnerTextArr('.breadcrumbs a:not(.home):not(.current)', []);
        if (breadCrumbs.length == 0) {
            breadCrumbs = Jisp.getInnerTextArr('[itemtype="http://schema.org/BreadcrumbList"] [itemprop="itemListElement"] a, [itemtype="https://schema.org/BreadcrumbList"] [itemprop="itemListElement"] a', ['Home'])
        }
        this.categoryNameText = Jisp.extractCategoryName(breadCrumbs);

        this.except = Jisp.getExcept(this);
        this.imageUrl = this.except ? null : this.imageUrl;

        return Jisp.getResult(this);
    };
})();