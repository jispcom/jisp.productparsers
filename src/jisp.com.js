window.parser_jisp_com = new (function () {
    Jisp.init(this);

    this.ready = function () {
        var imageUrl = Jisp.getMetaProperty(document, "og:image");
        return (!!imageUrl);
    };

    this.checkChanges = function () {
        return null;
    };

    this.getPageInfo = function() {

        var product = Jisp.getProductElement();
        if (!product) {
            product = document;
        }

        if ((this.url.indexOf("shopper/dashboard/item") >= 0) ||
            (this.url.indexOf("shopper/offers") >= 0) ||
            (this.url.indexOf("shopper/coupons") >= 0) ||
            (this.url.indexOf("shopper/storeproducts") >= 0) ||
            (this.url.indexOf("shopper/videos") >= 0) ||
            (this.url.indexOf("shopper/infotags") >= 0)) {
            this.productName = Jisp.getMetaProperty(document, "og:title");
            if (!this.productName) {
                this.productName = Jisp.getMetaProperty(document, "og:description");
            }

            this.imageUrl = Jisp.getMetaProperty(document, "og:image");
        } else {
            this.productName = Jisp.getProductName(product);

            this.imageUrl = Jisp.getImage(product);
        }

        var priceInnerText = Jisp.getPrice(product);
        this.priceAmount = Jisp.extractPrice(priceInnerText);
        var currencyInnerText = Jisp.extractCurrency(priceInnerText);
        if (!currencyInnerText) {
            currencyInnerText = Jisp.extractCurrency(Jisp.getCurrency(product));
        }
        this.priceCurrency = currencyInnerText;

        this.brandNameText = Jisp.getBrand(product);

        var breadCrumbs = Jisp.getBreadCrumbs(document);
        this.categoryNameText = Jisp.extractCategoryName(breadCrumbs);

        return Jisp.getResult(this);
    };
})();