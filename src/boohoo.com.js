window.parser_boohoo_com = new (function () {
    Jisp.init(this);

    this.placeholderImageUrl = "https://jisp.com/Content/img/placeholders/boohoo.com.png";

    this.ready = function () {
        return true;
    };

    this.checkChanges = function () {
        return null;
    };

    this.getPageInfo = function() {

        var product = Jisp.getProductElement();
        if (product) {
            this.productName = Jisp.getProductName(product);

            this.imageUrl = Jisp.getAttribute('#productdetail-image img', 'src');
            if (!this.imageUrl) {
                this.imageUrl = Jisp.getMetaProperty(document, "og:image");
            }

            if ((this.imageUrl) && (this.imageUrl.indexOf("//") == 0)){
                this.imageUrl = "http:" + this.imageUrl;
            }

            var priceInnerText = Jisp.getPrice(product);
            this.priceAmount = Jisp.extractPrice(priceInnerText);
            this.priceCurrency = Jisp.extractCurrency(priceInnerText); //<meta itemprop="priceCurrency" content="GBP">

            this.brandNameText = Jisp.getInnerText('.brand-shops');
            
            var breadCrumbs = Jisp.getInnerTextArr('.categorytree a:not(.crumbtrail-home)', []);
            this.categoryNameText = Jisp.extractCategoryName(breadCrumbs); 
        }
        else {
            this.except = true;
        }

        return Jisp.getResult(this);
    };
})();