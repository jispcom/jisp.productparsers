window.parser_mandmdirect_com = new (function () {
    Jisp.init(this);
    this.url = Jisp.ignoreSharpUrl(this.url);

    this.placeholderImageUrl = "https://jisp.com/Content/img/placeholders/mandmdirect.com.png";

    this.ready = function () {
        return true;
    };

    this.checkChanges = function () {
        return null;
    };

    this.getPageInfo = function () {

        this.isProductPage = Jisp.isExistsElement('#mainProductDetails');

        this.productName = Jisp.getAttribute('meta[data-productname]', 'data-productname');

        this.imageUrl = Jisp.getAttribute('#zoompd', 'src');
        
        var priceInnerText = Jisp.getAttribute('meta[data-sellingprice]', 'data-sellingprice');
        this.priceAmount = Jisp.extractPrice(priceInnerText);
        var currencyInnerText = Jisp.getAttribute('meta[data-currency]', 'data-currency');
        this.priceCurrency = Jisp.extractCurrency(currencyInnerText);

        this.brandNameText = Jisp.getAttribute('meta[data-brand]', 'data-brand');
        
        var breadCrumbs = [];
        breadCrumbs.push(Jisp.getAttribute('meta[data-cat]', 'data-cat'));
        breadCrumbs.push(Jisp.getAttribute('meta[data-subcat]', 'data-subcat'));
        this.categoryNameText = Jisp.extractCategoryName(breadCrumbs);

        return Jisp.getResult(this);
    };
})();