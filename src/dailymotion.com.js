window.parser_dailymotion_com = new (function () {
    Jisp.init(this);

    this.ready = function () {
        return true;
    };

    this.checkChanges = function () {
        return null;
    };

    this.getPageInfo = function() {

        var product = Jisp.getProductElement();
        if (!product) {
            product = document;
        }

        this.productName = Jisp.getInnerText('#video_title'); //Jisp.getProductName(product);

        this.imageUrl = Jisp.getImage(product);

        var priceInnerText = Jisp.getPrice(product);
        this.priceAmount = Jisp.extractPrice(priceInnerText);
        var currencyInnerText = Jisp.extractCurrency(priceInnerText);
        if(!currencyInnerText){
            currencyInnerText = Jisp.extractCurrency(Jisp.getCurrency(product));
        }
        this.priceCurrency = currencyInnerText;

        this.brandNameText = Jisp.getBrand(product);

        var breadCrumbs = Jisp.getBreadCrumbs(document);
        this.categoryNameText = Jisp.extractCategoryName(breadCrumbs);

        var videoUrl = Jisp.getAttribute('meta[name="twitter:player"]', 'value');

        return Jisp.getResult(this);
    };
})();