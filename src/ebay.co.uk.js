window.parser_ebay_co_uk = new (function () {
    Jisp.init(this);
    this.url = Jisp.ignoreSharpUrl(this.url);
    if (this.url) {
        if (this.url.substring(this.url.length - 1) == '?') {
            this.url = this.url.substring(0, this.url.length - 1);
        }
    }

    this.placeholderImageUrl = "https://jisp.com/Content/img/placeholders/ebay.co.uk.png";
    
    this.ready = function () {
        return true;
    };

    this.checkChanges = function () {
        return null;
    };

    this.getProductName = function(product) {
        var productNames = $('#itemTitle', product);

        if (productNames.length == 0) {
            productNames = $('[itemprop="name"].product-title');
        }

        if (productNames.length == 0) {
            productNames = $('[itemprop="name"]', product);
        }

        for (var i = 0; i < productNames.length; i++) {
            var productNameInnerText = productNames[i].innerText;
            if (productNameInnerText) {
                return productNameInnerText;
            }
        }

        return null;
    };

    this.getPrice = function(product) {

        var prices = $('[itemprop="price"]', product);
        if (prices.length == 0) {
            prices = $('#mm-saleDscPrc', product);
        }
        if (prices) {
            for (var i = 0; i < prices.length; i++) {
                var priceInnerText = prices[i].innerText;
                if (priceInnerText) {
                    return priceInnerText;
                }
            }
        }

        return Jisp.getAttribute(
            '[itemtype="http://schema.org/Product"] [itemtype="http://schema.org/Offer"] .offer-details-wrapper [itemprop="price"], [itemtype="https://schema.org/Product"] [itemtype="https://schema.org/Offer"] .offer-details-wrapper [itemprop="price"]',
            'content');
    };

    this.getBrand = function(product) {
        var brandNameTextArr = [];
        var brands = $('[itemprop="name"]', $('[itemprop="brand"]', product));
        for (var i = 0; i < brands.length; i++) {
            brandNameTextArr.push(brands[i].innerText);
        }

        if (brandNameTextArr.length == 0) {
            brands = $('td.attrLabels:contains("Brand:")', $('.itemAttr', document)).next();

            for (var i = 0; i < brands.length; i++) {
                brandNameTextArr.push(brands[i].innerText);
            }
        }

        return brandNameTextArr.join(',');
    };

    this.getBreadCrumbs = function(product) {
        var breadCrumbAll = [];
        var breadCrumbs = $('[itemprop="name"]',
            '[itemprop="item"]',
            '[itemprop="itemListElement"]',
            '[itemtype="http://schema.org/Breadcrumblist"]',
            product);

        if (breadCrumbs.length == 0) {
            breadCrumbs = $('[itemprop="name"]',
                '[itemprop="item"]',
                '[itemprop="itemListElement"]',
                '[itemtype="https://schema.org/Breadcrumblist"]',
                product);
        }

        if (breadCrumbs.length == 0) {
            breadCrumbs = $('#NavigationPanel table a.thrd', product);
        }

        for (var i = 0; i < breadCrumbs.length; i++) {
            breadCrumbAll.push(breadCrumbs[i].innerText);
        }

        return breadCrumbAll;
    };

    this.getImage = function(product) {
        var imageUrl = null;

        var image = $('[itemprop="image"][src]:not([src=""])').first()[0];
        if (image) {
            imageUrl = image.getAttribute('src');
        }

        if (!imageUrl) {
            imageUrl = Jisp.getMetaName(document, "twitter:image");
        }

        if (!imageUrl) {
            imageUrl = Jisp.getAttribute('[itemtype="http://schema.org/Product"] #w4-w0 img, [itemtype="https://schema.org/Product"] #w4-w0 img', 'src');
        }

        if (!imageUrl) {
            imageUrl = Jisp.getAttribute('[itemtype="http://schema.org/Product"] img, [itemtype="https://schema.org/Product"] img', 'src');
        }

        if (!imageUrl) {
            imageUrl = Jisp.getMetaProperty(document, "og:image");
        }

        if (!imageUrl) {
            imageUrl = Jisp.getMetaName(document, "og:image");
        }

        return (imageUrl) ? imageUrl : null;
    };

    this.getPageInfo = function() {

        var product = null;

        var products = $('[itemtype="http://schema.org/Product"], [itemtype="https://schema.org/Product"]');
        if (products) {
            if (products.length == 1) {
                product = products[0];
            }
        }

        if (product) {
            this.isProductPage = true;

            var productNameText = this.getProductName(product);
            if (productNameText) {
                this.productName = productNameText.replace(/<span.*>[\w\W]{1,}(.*?)[\w\W]{1,}<\/span>/gi, "");
            }

            this.imageUrl = this.getImage(product);

            var priceInnerText = this.getPrice(product);
            this.priceAmount = Jisp.extractPrice(priceInnerText);
            this.priceCurrency = Jisp.extractCurrency(priceInnerText);
            if (!this.priceCurrency) {
                this.priceCurrency = Jisp.getAttribute('[itemtype="http://schema.org/Product"] [itemtype="http://schema.org/Offer"] .offer-details-wrapper [itemprop="priceCurrency"]', 'content');
            }
            if (!this.priceCurrency) {
                this.priceCurrency = Jisp.getAttribute('[itemtype="https://schema.org/Product"] [itemtype="https://schema.org/Offer"] .offer-details-wrapper [itemprop="priceCurrency"]', 'content');
            }

            this.brandNameText = this.getBrand(product);

            var breadCrumbs = this.getBreadCrumbs(product);
            this.categoryNameText = Jisp.extractCategoryName(breadCrumbs);
        }
        else {
            this.except = true;
        }

        return Jisp.getResult(this);
    };
})();