window.parser_maplin_co_uk = new (function () {
    Jisp.init(this);
    
    this.placeholderImageUrl = "https://jisp.com/Content/img/placeholders/maplin.co.uk.png";

    this.ready = function () {
        return true;
    };

    this.checkChanges = function () {
        return null;
    };

    this.getPageInfo = function() {

        var product = Jisp.getProductElement();
        if (product) {
            this.productName = Jisp.getProductName(product);

            this.imageUrl = Jisp.getImage(product);

            var priceInnerText = Jisp.getMeta(document, 'itemprop="price"');
            this.priceAmount = Jisp.extractPrice(priceInnerText);
            var currencyInnerText = Jisp.getMeta(document, 'itemprop="priceCurrency"');
            this.priceCurrency = Jisp.extractCurrency(currencyInnerText);

            this.brandNameText = Jisp.getBrand(product);//not found on page

            var breadCrumbs = Jisp.getInnerTextArr('[itemtype="http://data-vocabulary.org/Breadcrumb"]:not([class="active"]) a [itemprop="title"]', ['Home']);
            this.categoryNameText = Jisp.extractCategoryName(breadCrumbs); 
        }
        else {
            this.except = true;
        }

        return Jisp.getResult(this);
    };
})();