window.parser_halfords_com = new (function () {
    Jisp.init(this);

    this.placeholderImageUrl = "https://jisp.com/Content/img/placeholders/halfords.com.png";

    this.ready = function () {
        return true;
    };

    this.checkChanges = function () {
        return null;
    };

    this.getPageInfo = function () {

        this.isProductPage = Jisp.isExistsElement('#pdpMirakl');

        this.productName = Jisp.getMetaProperty(document, "og:title");

        this.imageUrl = Jisp.getAttribute('.amp-visible .amp-image-container .amp-main-img', 'src');
        if (!this.imageUrl) {
            this.imageUrl = Jisp.getMetaProperty(document, "og:image");
        }

        var priceInnerText = Jisp.getInnerText('.hproduct .price');
        this.priceAmount = Jisp.extractPrice(priceInnerText);
        this.priceCurrency = Jisp.extractCurrency(priceInnerText);
        
        this.brandNameText = Jisp.getInnerText('.hproduct .brand');

        var breadCrumbs = [];
        breadCrumbs.push(Jisp.getInnerText('.hproduct .category'));
        this.categoryNameText = Jisp.extractCategoryName(breadCrumbs);

        return Jisp.getResult(this);
    };
})();