var Jisp;
(function (Jisp) {
    'use strict';

    function init(parser) {
        parser.url = Jisp.getAttribute('link[rel="canonical"]', 'href');
        if (!parser.url) {
            parser.url = document.location.href;
        } else {
            if ((parser.url.indexOf('/') === 0) && (parser.url.indexOf('//') !== 0)) {
                parser.url = document.location.protocol + '//' + document.location.hostname + parser.url;
            }

            if (parser.url.indexOf('//') === 0) {
                parser.url = document.location.protocol + '//' + parser.url.substring(2);
            }
        }
        parser.title = document.title;

        var product = Jisp.getProductElement();
        parser.isProductPage = (product) ? true : false;

        parser.except = false;
        parser.imageUrl = null;
        parser.placeholderImageUrl = null;
        parser.priceAmount = null;
        parser.priceCurrency = null;
        parser.brandNameText = null;
        parser.categoryNameText = null;
        parser.productName = null;
    }
    Jisp.init = init;

    function getResult(parser) {
        var except = Jisp.getExcept(parser);
        return {
            except: except, // don't delete for backward compatibility
            productType: (except) ? 2 : 1,
            url: parser.url,
            title: (parser.productName) ? parser.productName : parser.title,
            imageUrl: (parser.imageUrl) ? parser.imageUrl : null,
            placeholderImageUrl: parser.placeholderImageUrl,
            price: (!except) ? (((parser.priceCurrency == '£') || (parser.priceCurrency == 'GBP')) ? parser.priceAmount : null) : null,
            currency: (!except) ? (((parser.priceCurrency == '£') || (parser.priceCurrency == 'GBP')) ? '£' : null) : null,
            websiteBrand: parser.brandNameText,
            websiteCategory: parser.categoryNameText
        };
    }
    Jisp.getResult = getResult;

    function getProductElement() {
        return document.querySelector('[itemtype="http://schema.org/Product"], [itemtype="https://schema.org/Product"]');
    }
    Jisp.getProductElement = getProductElement;

    function isExistsElement(selector) {
        var element = document.querySelector(selector);
        return (element) ? true : false;
    }
    Jisp.isExistsElement = isExistsElement;

    function getProductName(product) {
        var productName = null;

        var productElement = $('[itemtype="http://schema.org/Product"] [itemprop="name"]:not(:empty), [itemtype="https://schema.org/Product"] [itemprop="name"]:not(:empty)');
        if (productElement){
            productElement = productElement.first()[0];

            if (productElement) {
                productName = productElement.innerText;
            }
        }

        if (!productName) {
            productElement = $('[itemtype="http://schema.org/Product"] [itemprop="name"], [itemtype="https://schema.org/Product"] [itemprop="name"]');
            if (productElement) {
                productElement = productElement.first()[0];
            }

            if (productElement) {
                productName = productElement.getAttribute('content');
            }
        }

        if (!productName) {
            productName = Jisp.getMetaProperty(document, "og:title");
        }

        return (productName) ? productName.trim() : null;
    }
    Jisp.getProductName = getProductName;

    function getImage(product) {
        var imageUrl = Jisp.getMetaName(document, "twitter:image");

        if (!imageUrl) {
            imageUrl = Jisp.getMetaProperty(document, "og:image");
        }

        if (!imageUrl) {
            imageUrl = Jisp.getMetaName(document, "og:image");
        }

        if (!imageUrl) {
            var image = $('[itemprop="image"][src]:not([src=""])');
            if (image) {
                image = image.first()[0];

                if (image) {
                    imageUrl = image.getAttribute('src');
                }
            }
        }

        if (imageUrl) {
            imageUrl = imageUrl.replace("http:///https://", "https://");

            if ((imageUrl.indexOf('/') === 0) && (imageUrl.indexOf('//') !== 0)) {
                imageUrl = document.location.protocol + '//' + document.location.hostname + imageUrl;
            }

            if (imageUrl.indexOf('//') === 0) {
                imageUrl = document.location.protocol + '//' + imageUrl.substring(2);
            }
        }

        return (imageUrl) ? imageUrl.replace("http:///https://", "https://") : null;
    }
    Jisp.getImage = getImage;

    function getPrice(product) {
        var priceText = null;

        var priceElement = $('[itemtype="http://schema.org/Product"] [itemprop="price"]:not(:empty), [itemtype="https://schema.org/Product"] [itemprop="price"]:not(:empty)');
        if (priceElement) {
            priceElement = priceElement.first()[0];

            if (priceElement) {
                priceText = priceElement.innerText;
            }
        }

        if (!priceText) {
            priceText = Jisp.getMetaProperty(document, "og:price:amount");
        }

        if (!priceText) {
            priceText = Jisp.getMetaProperty(document, "product:price:amount");
        }

        if (!priceText) {
            priceText = Jisp.getMetaName(document, "product:price:amount");
        }

        if (!priceText) {
            priceText = Jisp.getMeta(document, 'itemprop="price"');
        }
        return (priceText) ? priceText.trim() : null;
    }
    Jisp.getPrice = getPrice;

    function getCurrency(product) {
        var currencyText = Jisp.getMeta(document, 'itemprop="priceCurrency"');

        if (!currencyText) {
            currencyText = Jisp.getMetaProperty(document, "og:price:currency");
        }

        if (!currencyText) {
            currencyText = Jisp.getMetaName(document, "product:price:currency");
        }

        if (!currencyText) {
            currencyText = Jisp.getAttribute('[itemprop="priceCurrency"]', 'content');
        }
        return (currencyText) ? currencyText.trim() : null;
    }
    Jisp.getCurrency = getCurrency;

    function extractPrice(priceInnerText) {
        if (priceInnerText) {
            var priceValue = priceInnerText.replace('£', '').replace(',', '').trim();
            var priceParsed = parseFloat(priceValue);
            if (!isNaN(priceParsed)) {
                return priceParsed;
            }
        }
        return null;
    }
    Jisp.extractPrice = extractPrice;

    function extractCurrency(priceInnerText) {
        if (priceInnerText) {
            if ((priceInnerText.indexOf('£') >= 0) || (priceInnerText.indexOf('GBP') >= 0)) {
                return '£';
            }
        }
        return null;
    }
    Jisp.extractCurrency = extractCurrency;

    function getBrand(product) {
        var brand = null;

        var brandElement = $('[itemtype="http://schema.org/Product"] [itemprop="brand"] [itemprop="name"]:not(:empty), [itemtype="https://schema.org/Product"] [itemprop="brand"] [itemprop="name"]:not(:empty)');
        if (brandElement) {
            brandElement = brandElement.first()[0];

            if (brandElement) {
                brand = brandElement.innerText;
            }
        }
        
        if (!brand) {
            var brandElementNotProd = $('[itemprop="brand"] [itemprop="name"]:not(:empty)');
            if (brandElementNotProd) {
                brandElementNotProd = brandElementNotProd.first()[0];

                if (brandElementNotProd) {
                    brand = brandElementNotProd.innerText;
                }
            }
        }

        if (!brand) {
            var brandElementAttr = $('[itemprop="brand"] [itemprop="name"]');
            if (brandElementAttr) {
                brandElementAttr = brandElementAttr.first()[0];

                if (brandElementAttr) {
                    brand = brandElementAttr.getAttribute('content');
                }
            }
        }

        if (!brand) {
            brand = Jisp.getInnerText('[itemprop="brand"]');
        }

        if (!brand) {
            brand = Jisp.getMetaProperty(document, "brand");
        }
        return (brand) ? brand.trim() : null;
    }
    Jisp.getBrand = getBrand;

    function getBreadCrumbs(product) {

        var breadCrumbAll = Jisp.getInnerTextArr('[itemprop="breadcrumb"] a', ['Home']);
        if ((!breadCrumbAll) || (breadCrumbAll.length == 0)) {
            breadCrumbAll = Jisp.getInnerTextArr('.breadcrumb a', ['Home']);
        }

        if ((!breadCrumbAll) || (breadCrumbAll.length == 0)) {
            breadCrumbAll = Jisp.getInnerTextArr('#breadcrumb a', ['Home']);
        }

        if ((!breadCrumbAll) || (breadCrumbAll.length == 0)) {
            breadCrumbAll = Jisp.getInnerTextArr('[itemtype="http://data-vocabulary.org/Breadcrumb"] a, [itemtype="https://data-vocabulary.org/Breadcrumb"] a', ['Home']);
        }
        return breadCrumbAll;
    }
    Jisp.getBreadCrumbs = getBreadCrumbs;

    function extractCategoryName(breadCrumbs) {
        if (breadCrumbs) {
            return breadCrumbs.join(' > ');
        }
        else {
            return null;
        }
    }
    Jisp.extractCategoryName = extractCategoryName;

    function getExcept(parser) {
        //return parser.except || !((parser.priceCurrency == '£') || (parser.priceCurrency == 'GBP')) || (parser.priceAmount == null);
        return parser.except || (!parser.isProductPage);
    }
    Jisp.getExcept = getExcept;

    function getMeta(document, selector) {
        var metas = $('meta[' + selector + ']', document);
        if (metas) {
            for (var i = 0; i < metas.length; i++) {
                var metaValue = metas[i].getAttribute('content');
                if (metaValue) {
                    return metaValue.trim();
                }
            }
        }
        return null;
    }
    Jisp.getMeta = getMeta;

    function getMetaProperty(document, property) {
        return getMeta(document, 'property="' + property + '"');
    }
    Jisp.getMetaProperty = getMetaProperty;

    function getMetaName(document, name) {
        return getMeta(document, 'name="' + name + '"');
    }
    Jisp.getMetaName = getMetaName;

    function getInnerText(selector) {
        var elements = $(selector);
        if (elements) {
            for (var i = 0; i < elements.length; i++) {
                var elementInnerText = elements[i].innerText;
                if (elementInnerText) {
                    return elementInnerText.trim();
                }
            }
        }
        return null;
    }
    Jisp.getInnerText = getInnerText;

    function getInnerTextArr(selector, filter) {
        var elementsAll = [];
        var elements = $(selector);
        if (elements) {
            for (var i = 0; i < elements.length; i++) {
                var elementInnerText = elements[i].innerText;
                if (elementInnerText) {
                    elementsAll.push(elementInnerText.trim());
                }
            }
        }

        if (filter) {
            filter = filter.filter(function (name) {
                return (name);
            }).map(function (name) {
                return name.toUpperCase();
            });

            elementsAll = elementsAll.filter(function (name) {
                return filter.indexOf(name.toUpperCase()) == -1;
            });
        }

        return elementsAll;
    }
    Jisp.getInnerTextArr = getInnerTextArr;

    function getAttribute(selector, attr) {
        var elements = $(selector);
        if (elements) {
            for (var i = 0; i < elements.length; i++) {
                var attrValue = elements[i].getAttribute(attr);
                if (attrValue) {
                    return attrValue.trim();
                }
            }
        }
        return null;
    }
    Jisp.getAttribute = getAttribute;

    function ignoreSharpUrl(url) {
        if (url) {
            var sharpIndex = url.indexOf('#');
            if (sharpIndex > 0) {
                return url.substring(0, sharpIndex);
            }
        }
        return url;
    }
    Jisp.ignoreSharpUrl = ignoreSharpUrl;

    function ignoreQueryUrl(url) {
        if (url) {
            var queryIndex = url.indexOf('?');
            if (queryIndex > 0) {
                return url.substring(0, queryIndex);
            }
        }
        return url;
    }
    Jisp.ignoreQueryUrl = ignoreQueryUrl;

    function addProtocolToUrl(url) {
        if ((url) && (url.indexOf("//") == 0)) {
            url = document.location.protocol + url;
        }
        return url;
    }
    Jisp.addProtocolToUrl = addProtocolToUrl;
})(Jisp || (Jisp = {}));