window.parser_riverisland_com = new (function () {
    Jisp.init(this);
    if (document.location.hostname == 'eu.riverisland.com') {
        if (this.url) {
            this.url = this.url.replace('eu.riverisland.com', 'www.riverisland.com');
        }
    }

    this.placeholderImageUrl = "https://jisp.com/Content/img/placeholders/riverisland.com.png";

    this.ready = function () {
        return true;
    };

    this.checkChanges = function () {
        return null;
    };

    this.getPrice = function () {
        var priceText = null;

        var prices = $('.bundle-parent .price .sale');
        for (var i = 0; i < prices.length; i++) {
            var priceTextFinded = prices[i].innerHTML;
            if (priceTextFinded) {
                priceText = priceTextFinded.replace(/<span.*>[\w\W]{1,}(.*?)[\w\W]{1,}<\/span>/gi, "").trim();
            }
        }

        if (!priceText) {
            priceText = Jisp.getInnerText('.bundle-parent .price');
        }

        if (!priceText) {
            prices = $('.product-details-container .right-side .price .sale')

            for (var i = 0; i < prices.length; i++) {
                var priceTextFinded = prices[i].innerHTML;
                if (priceTextFinded) {
                    priceText = priceTextFinded.replace(/<span.*>[\w\W]{1,}(.*?)[\w\W]{1,}<\/span>/gi, "").trim();
                }
            }
        }

        if (!priceText) {
            prices = $('.product-details-container .right-side .price span');

            for (var i = 0; i < prices.length; i++) {
                var priceTextFinded = prices[i].innerHTML;
                if (priceTextFinded) {
                    priceText = priceTextFinded.replace(/<span.*>[\w\W]{1,}(.*?)[\w\W]{1,}<\/span>/gi, "").trim();
                }
            }
        }

        return priceText;
    }

    this.getPageInfo = function () {

        this.isProductPage = Jisp.isExistsElement('.product-details-container');

        if (!this.isProductPage) {
            this.url = document.location.href;
        }

        this.productName = Jisp.getMetaProperty(document, "og:title");

        this.imageUrl = Jisp.getAttribute('.main-image-container img', 'src');
        //if (!this.imageUrl) {
        //    this.imageUrl = Jisp.getImage(document);
        //}

        var priceInnerText = this.getPrice();
        this.priceAmount = Jisp.extractPrice(priceInnerText);
        this.priceCurrency = Jisp.extractCurrency(priceInnerText);

        this.brandNameText = 'River Island';

        var breadCrumbs = Jisp.getInnerTextArr('.breadcrumbs a', ['Home']);
        this.categoryNameText = Jisp.extractCategoryName(breadCrumbs);

        return Jisp.getResult(this);
    };
})();