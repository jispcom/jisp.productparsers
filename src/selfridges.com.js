window.parser_selfridges_com = new (function () {
    Jisp.init(this);

    var linkUrl = Jisp.getAttribute('link[rel="alternate"][hreflang="en-GB"]', 'href');
    if (!linkUrl) {
        linkUrl = Jisp.getAttribute('link[rel="canonical"]', 'href');
    }
    this.url = (linkUrl) ? linkUrl : this.url;

    this.placeholderImageUrl = "https://jisp.com/Content/img/placeholders/selfridges.com.png";

    this.ready = function () {
        return true;
    };

    this.checkChanges = function () {
        return null;
    };

    this.getImage = function () {
        var imageUrl = null;
        var image = $('[itemprop="image"][src]:not([src=""])');
        if (image) {
            image = image.first()[0];
        }

        if (image) {
            imageUrl = image.getAttribute('src');
        }

        if (!imageUrl) {
            imageUrl = Jisp.getMetaName(document, "twitter:image");
        }

        if (!imageUrl) {
            imageUrl = Jisp.getMetaProperty(document, "og:image");
        }

        if (!imageUrl) {
            imageUrl = Jisp.getMetaName(document, "og:image");
        }

        return (imageUrl) ? imageUrl : null;
    };

    this.getPageInfo = function() {

        var product = Jisp.getProductElement();
        if (product) {
            
            var priceInnerText = Jisp.getPrice(product);
            this.priceAmount = Jisp.extractPrice(priceInnerText);

            if (this.priceAmount == null) {
                priceInnerText = Jisp.getInnerText('.price');
                this.priceAmount = Jisp.extractPrice(priceInnerText);
            }

            var currencyInnerText = Jisp.extractCurrency(priceInnerText);
            if (!currencyInnerText) {
                currencyInnerText = Jisp.extractCurrency(Jisp.getAttribute('[itemprop="priceCurrency"]', 'content'));
            }

            this.priceCurrency = currencyInnerText;
        }
        else {
            product = document;
        }

        this.productName = Jisp.getProductName(product);

        this.imageUrl = this.getImage(product);

        this.brandNameText = Jisp.getBrand(product);

        var breadCrumbs = Jisp.getInnerTextArr('[itemprop="breadcrumb"] a', ['Home']);
        this.categoryNameText = Jisp.extractCategoryName(breadCrumbs);

        return Jisp.getResult(this);
    };
})();