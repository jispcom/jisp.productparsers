window.parser_missguided_co_uk = new (function () {
    Jisp.init(this);

    this.placeholderImageUrl = "https://jisp.com/Content/img/placeholders/missguided.co.uk.png";

    this.ready = function () {
        return true;
    };

    this.checkChanges = function () {
        return null;
    };

    this.getPageInfo = function () {

        this.isProductPage = Jisp.isExistsElement('[itemtype="http://schema.org/Product"].product-view, [itemtype="https://schema.org/Product"].product-view');

        var product = Jisp.getProductElement();
        if (!product || !this.isProductPage) {
            product = document;
        }

        this.productName = Jisp.getProductName(product);

        this.imageUrl = Jisp.getAttribute('.product-images-list__item--active img[title]', 'src');
        if (!this.imageUrl) {
            this.imageUrl = Jisp.getImage(product);
        }

        var priceInnerText = Jisp.getPrice(product);
        this.priceAmount = Jisp.extractPrice(priceInnerText);
        var currencyInnerText = Jisp.extractCurrency(priceInnerText);
        if (!currencyInnerText) {
            currencyInnerText = Jisp.extractCurrency(Jisp.getCurrency(product));
        }
        this.priceCurrency = currencyInnerText;

        this.brandNameText = Jisp.getBrand(product);

        var breadCrumbs = Jisp.getBreadCrumbs(document);
        this.categoryNameText = Jisp.extractCategoryName(breadCrumbs);

        return Jisp.getResult(this);
    };
})();