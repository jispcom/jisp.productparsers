window.parser_ebuyer_com = new (function () {
    Jisp.init(this);

    this.placeholderImageUrl = "https://jisp.com/Content/img/placeholders/ebuyer.com.png";

    this.ready = function () {
        return true;
    };

    this.checkChanges = function () {
        return null;
    };

    this.getPageInfo = function() {

        this.isProductPage = this.isProductPage ||
            (Jisp.isExistsElement('.product-details') && (document.location.hostname === 'm.ebuyer.com'));

        if (!this.isProductPage) {
            this.url = document.location.href;
        }

        var product = Jisp.getProductElement();
        if (product) {
            this.productName = Jisp.getInnerText('.product-titlea');
            if (!this.productName) {
                this.productName = Jisp.getInnerText('.product-title');
            }

            var image = $('#gallery-carousel .item.active .js-carousel-img');
            if (image) {
                image = image.first()[0];

                if (image) {
                    this.imageUrl = image.getAttribute('src');
                }
            }

            if (!this.imageUrl) {
                image = $('[itemprop="image"][src]:not([src=""])');
                if (image) {
                    image = image.first()[0];

                    if (image) {
                        this.imageUrl = image.getAttribute('src');
                    }
                }
            }

            if (this.imageUrl) {
                if (this.imageUrl.indexOf('//') === 0) {
                    this.imageUrl = document.location.protocol + '//' + this.imageUrl.substring(2);
                }
            }

            if (!this.imageUrl) {
                this.imageUrl = Jisp.getMetaName(document, "og:image");
            }
            
            var priceInnerText = Jisp.getPrice(product);
            this.priceAmount = Jisp.extractPrice(priceInnerText);
            var currencyInnerText = Jisp.getMeta(document, 'itemprop="priceCurrency"');
            this.priceCurrency = Jisp.extractCurrency(currencyInnerText);

            this.brandNameText = Jisp.getAttribute('[itemprop="brand"] [itemprop="name"]', 'content');
            
            //var breadCrumbs = [];
            //breadCrumbs.push(Jisp.getMeta(document, 'itemprop="category"'));
            var breadCrumbs = Jisp.getInnerTextArr('[itemtype="http://schema.org/BreadcrumbList"] [itemtype="http://schema.org/ListItem"] [itemprop="name"], [itemtype="https://schema.org/BreadcrumbList"] [itemtype="https://schema.org/ListItem"] [itemprop="name"]', []);
            this.categoryNameText = Jisp.extractCategoryName(breadCrumbs); 
        }
        else {
            this.except = true;
        }

        return Jisp.getResult(this);
    };
})();