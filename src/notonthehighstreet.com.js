window.parser_notonthehighstreet_com = new (function () {
    Jisp.init(this);

    this.url = Jisp.ignoreSharpUrl(this.url);

    this.placeholderImageUrl = "https://jisp.com/Content/img/placeholders/notonthehighstreet.com.png";

    this.ready = function () {
        return true;
    };

    this.checkChanges = function () {
        return null;
    };

    this.getPageInfo = function () {

        this.isProductPage = Jisp.isExistsElement('#product');

        this.productName = Jisp.getMetaProperty(document, "og:title");
        if (!this.productName) {
            this.productName = Jisp.getInnerText('.product_title');
        }

        this.imageUrl = Jisp.getAttribute('.product_main_image_container img.shown_image', 'src');
        if (!this.imageUrl) {
            this.imageUrl = Jisp.getMetaProperty(document, "og:image");
        }

        var priceInnerText = Jisp.getAttribute('#price .currency_GBP', 'data-current-price');
        if (!priceInnerText) {
            priceInnerText = Jisp.getMetaName(document, "product:price:amount");
        }

        this.priceAmount = Jisp.extractPrice(priceInnerText);
        var currencyInnerText = Jisp.getMetaName(document, "product:price:currency");
        if (!currencyInnerText) {
            currencyInnerText = Jisp.getInnerText('#price .currency_GBP .price_unit');
        }
        this.priceCurrency = Jisp.extractCurrency(currencyInnerText);

        this.brandNameText = Jisp.getInnerText('#product_header .by a');

        var breadCrumbs = [];//Jisp.getInnerTextArr('[itemprop="breadcrumb"] a', ['HOMEPAGE']);
        this.categoryNameText = Jisp.extractCategoryName(breadCrumbs); 

        return Jisp.getResult(this);
    };
})();