window.parser_zara_com = new (function () {
    Jisp.init(this);

    this.placeholderImageUrl = "https://jisp.com/Content/img/placeholders/zara.com.png";

    this.ready = function () {
        var imageUrl = Jisp.getAttribute('#main-images img', 'src');

        return ((imageUrl != null) && (imageUrl != '') && ( imageUrl.indexOf('data:') != 0));
    };

    this.checkChanges = function () {
        return null;
    };

    this.getProductName = function (selector) {
        var elements = $(selector);
        if (elements) {
            for (var i = 0; i < elements.length; i++) {
                var elementInnerText = elements[i].innerHTML;
                if (elementInnerText) {
                    return elementInnerText.trim().replace(/<span.*>[\w\W]{1,}(.*?)[\w\W]{1,}<\/span>/gi, "");
                }
            }
        }
        return null;
    }

    this.getPrice = function () {
        var elements = $('#product .right .price').not('#product .related-product .price');
        if (elements) {
            for (var i = 0; i < elements.length; i++) {
                var elementInnerText = elements[i].innerText;
                if (elementInnerText) {
                    return elementInnerText.trim().replace(',', '.').replace(String.fromCharCode(160), '');
                }
            }
        }
        return null;
    }

    this.getPageInfo = function () {

        var existsList = Jisp.isExistsElement('.product-list');
        var product = $('#product')[0];
        if (product && (!existsList)) {
            this.isProductPage = true;

            this.productName = this.getProductName('#product header h1');
            if (!this.productName) {
                this.productName = this.getProductName('#product ._bundle-detail-actions .bundle-item-description .name');
            }

            if (!this.productName) {
                this.productName = Jisp.getInnerText('#product header');
            }

            this.imageUrl = Jisp.getAttribute('#main-images img', 'src');
            if ((this.imageUrl) && (this.imageUrl.indexOf('data:') == 0)) {
                this.imageUrl = Jisp.getAttribute('#main-images a._seoImg', 'href');
                if ((this.imageUrl) && (this.imageUrl.indexOf('data:') == 0)) {
                    this.imageUrl = null;
                }
            }
            this.imageUrl = Jisp.addProtocolToUrl(this.imageUrl);

            var priceInnerText = this.getPrice();
            this.priceAmount = Jisp.extractPrice(priceInnerText);
            this.priceCurrency = Jisp.extractCurrency(priceInnerText);

            this.brandNameText = 'Zara';

            var breadCrumbs = Jisp.getInnerTextArr('[itemtype="http://data-vocabulary.org/Breadcrumb"] [itemprop="url"] [itemprop="title"]', ['ZARA', 'View all']);
            this.categoryNameText = Jisp.extractCategoryName(breadCrumbs);
        }
        else {
            this.except = true;
        }

        return Jisp.getResult(this);
    };
})();