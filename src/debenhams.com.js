window.parser_debenhams_com = new (function () {
    Jisp.init(this);

    this.placeholderImageUrl = "https://jisp.com/Content/img/placeholders/debenhams.com.png";

    this.ready = function () {
        return true;
    };

    this.checkChanges = function () {
        return null;
    };

    this.getBreadCrumbs = function (product) {
        var breadCrumbAll = Jisp.getInnerTextArr('#breadcrumb_cat a');

        if (breadCrumbAll.length == 0)
        {
            var categoryText = Jisp.getMetaProperty(product, "category");
            if (categoryText) {
                var arr = categoryText.split('|');
                breadCrumbAll.push(arr[0]);
            }

            var subcategoryText = Jisp.getMetaProperty(product, "subcategory");
            if (subcategoryText) {
                var arr = subcategoryText.split('|');
                breadCrumbAll.push(arr[0]);
            }
        }
            
        return breadCrumbAll;
    }

    this.getPageInfo = function () {

        var typeName = Jisp.getAttribute('[property="og:type"]', 'content');
        typeName = (typeName) ? typeName.toLowerCase() : "";
        if (typeName === "product") {
            this.isProductPage = true;

            this.productName = Jisp.getMetaProperty(document, "short_description");

            this.imageUrl = Jisp.getAttribute('#prodImg img', 'src');
            if (!this.imageUrl) {
                this.imageUrl = Jisp.getImage(document);
            }
            
            var priceInnerText = Jisp.getMetaName(document, 'twitter:data1');
            if (priceInnerText) {
                priceInnerText = priceInnerText.toLowerCase().replace('now', '');
            }
            this.priceAmount = Jisp.extractPrice(priceInnerText);
            var currencyInnerText = Jisp.getAttribute('[itemprop="priceCurrency"]', 'content');
            this.priceCurrency = Jisp.extractCurrency(currencyInnerText);
            
            this.brandNameText = Jisp.getMetaProperty(document, "brand");

            var breadCrumbs = this.getBreadCrumbs(document);
            this.categoryNameText = Jisp.extractCategoryName(breadCrumbs);
        }
        else {
            this.except = true;
        }

        return Jisp.getResult(this);
    };
})();