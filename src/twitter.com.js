window.parser_twitter_com = new (function () {
    Jisp.init(this);

    this.ready = function () {
        var imageUrl = this.getImage();
        return (!!imageUrl);
    };

    this.checkChanges = function () {
        return null;
    };

    this.getProductName = function () {
        var iframe = $(".tweet iframe");
        if (iframe) {
            var img = iframe.contents().find('.TwitterCard-title');

            for (var i = 0; i < img.length; i++) {
                var attrValue = img[i].innerText;
                if (attrValue) {
                    return attrValue.trim();
                }
            }
        }

        var elements = $('.permalink .tweet .tweet-text');

        if ((!elements) || ((elements) && (elements.length === 0)))
        {
            elements = $('.Gallery .tweet-text');
        }

        if (elements) {
            for (var i = 0; i < elements.length; i++) {
                var elementInnerText = elements[i].innerHTML;
                if (elementInnerText.indexOf('<') >= 0) {
                    return elementInnerText.substr(0, elementInnerText.indexOf('<')).trim();
                }
            }
        }

        return null;
    };

    this.getImage = function () {
        var iframe = $(".tweet iframe");
        if (iframe) {
            var img = iframe.contents().find('.SummaryCard-image img');

            for (var i = 0; i < img.length; i++) {
                var attrValue = img[i].getAttribute('src');
                if (attrValue) {
                    return attrValue.trim();
                }
            }
        }

        var imgUrl = Jisp.getAttribute('.Gallery .Gallery-media img', 'src');

        if (!imgUrl) {
            imgUrl = Jisp.getAttribute('#permalink-overlay-body [data-element-context="platform_photo_card"] img', 'src');
        }

        return imgUrl;
    };

    this.getPageInfo = function() {

        var product = Jisp.getProductElement();
        if (!product) {
            product = document;
        }

        this.productName = this.getProductName();
        if (!this.productName) {
            this.productName = Jisp.getProductName(product);
        }

        this.imageUrl = this.getImage();
        if (!this.imageUrl) {
            this.imageUrl = Jisp.getImage(product);
        }

        var priceInnerText = Jisp.getPrice(product);
        this.priceAmount = Jisp.extractPrice(priceInnerText);
        var currencyInnerText = Jisp.extractCurrency(priceInnerText);
        if(!currencyInnerText){
            currencyInnerText = Jisp.extractCurrency(Jisp.getCurrency(product));
        }
        this.priceCurrency = currencyInnerText;

        this.brandNameText = Jisp.getBrand(product);

        var breadCrumbs = Jisp.getBreadCrumbs(document);
        this.categoryNameText = Jisp.extractCategoryName(breadCrumbs);

        return Jisp.getResult(this);
    };
})();