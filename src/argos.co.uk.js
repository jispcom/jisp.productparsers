window.parser_argos_co_uk = new (function () {
    Jisp.init(this);
    this.url = Jisp.ignoreSharpUrl(this.url);

    this.placeholderImageUrl = "https://jisp.com/Content/img/placeholders/argos.co.uk.png";

    this.ready = function () {
        return true;
    };

    this.checkChanges = function () {
        return null;
    };

    this.getImage = function() {
        var images = $('.image #main img').filter(function() {
            return $(this).css('display') == 'block';
        });

        if (images.length == 0) {
            images = $('.product-info-container #main img').filter(function() {
                return $(this).css('display') == 'block';
            });
        }

        if (images.length == 0) {
            images = $('.media-player-colosseum .media-player-items li.active img');
        }

        for (var i = 0; i < images.length; i++) {
            var findedImageUrl = images[i].getAttribute('src');
            if (findedImageUrl) {
                return findedImageUrl;
            }
        }
        return null;
    };

    this.getPageInfo = function () {

        this.isProductPage = Jisp.isExistsElement('[itemtype="http://schema.org/Product"].pdp-main, [itemtype="https://schema.org/Product"].pdp-main');
        
        this.productName = Jisp.getInnerText('#pdpProduct h1'); 
        if(!this.productName){
            this.productName = Jisp.getInnerText('[data-el="pdp-product-title"]');
        }

        if (!this.productName) {
            this.productName = Jisp.getInnerText('.product-name-main [itemprop="name"]');
        }

        this.imageUrl = this.getImage();
        if (this.imageUrl && this.imageUrl.indexOf('//') === 0) {
            this.imageUrl = 'http:' + this.imageUrl;
        }
        
        var priceInnerText = Jisp.getInnerText('#pdpPricing .price');
        if (!priceInnerText) {
            priceInnerText = Jisp.getInnerText('[data-el="pdp-price"]');
        }
        if (!priceInnerText) {
            priceInnerText = Jisp.getAttribute('[itemtype="http://schema.org/Product"] .pdp-pricing-module [itemprop="price"], [itemtype="https://schema.org/Product"] .pdp-pricing-module [itemprop="price"]', 'content');
        }

        this.priceAmount = Jisp.extractPrice(priceInnerText);

        this.priceCurrency = Jisp.extractCurrency(priceInnerText);
        if (!this.priceCurrency) {
            var currencyValue = Jisp.getAttribute('[itemtype="http://schema.org/Product"] .pdp-pricing-module [itemprop="priceCurrency"], [itemtype="https://schema.org/Product"] .pdp-pricing-module [itemprop="priceCurrency"]', 'content');
            this.priceCurrency = Jisp.extractCurrency(currencyValue);
        }

        this.brandNameText = Jisp.getInnerText('.pdp-view-brand-main');
        if (!this.brandNameText) {
            this.brandNameText = Jisp.getInnerText('[itemprop="brand"]');
        }

        var breadCrumbs = Jisp.getInnerTextArr('#breadcrumb a', ['Home']);
        if (breadCrumbs.length == 0) {
            breadCrumbs = Jisp.getInnerTextArr('.breadcrumb a', []);
        }
        this.categoryNameText = Jisp.extractCategoryName(breadCrumbs);

        return Jisp.getResult(this);
    };
})();