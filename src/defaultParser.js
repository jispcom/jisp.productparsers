window.parser_defaultParser= new (function () {
    this.ready = function () {
        return true;
    };

    this.checkChanges = function () {
        return null;
    };

    this.getPageInfo = function() {
        var url = document.location.href;
        var title = document.getElementsByTagName("title")[0].innerHTML;
        var imageUrl = 'https://www.jisp.com/Content/images/jisp-logo@2x.png';

        return {
            except: true,
            url: url,
            title: title,
            imageUrl: imageUrl,
            price: null,
            currency: null,
            websiteBrand: null,
            websiteCategory: null
        };
    };
})();