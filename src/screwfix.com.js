window.parser_screwfix_com = new (function () {
    Jisp.init(this);

    this.placeholderImageUrl = "https://jisp.com/Content/img/placeholders/screwfix.com.png";

    this.ready = function () {
        return true;
    };

    this.checkChanges = function () {
        return null;
    };

    this.getPrice = function (product) {
        var prices = $('[itemprop="price"]', product);

        for (var i = 0; i < prices.length; i++) {
            var productNameTextFinded = prices[i].getAttribute('content');
            if (productNameTextFinded) {
                return productNameTextFinded;
            }
        }

        return null;
    }

    this.getCurrency = function () {
        var elements = $('[itemprop="priceCurrency"]');
        for (var i = 0; i < elements.length; i++) {
            var elementInnerText = elements[i].getAttribute('content');
            if (elementInnerText) {
                return elementInnerText.trim();
            }
        }
        return null;
    }

    this.getBrand = function () {
        var elements = $('#product_selling_attributes_table th').filter(function () {
            return $(this).text().toLowerCase().indexOf('Brand'.toLowerCase()) >= 0;
        });
        elements = elements.next();

        if (elements.length > 0) {
            for (var i = 0; i < elements.length; i++) {
                var elementInnerText = elements[i].innerText;
                if (elementInnerText) {
                    return elementInnerText;
                }
            }
        }

        elements = $('.brand');
        if (elements.length > 0) {
            var elementInnerText = elements[0].getAttribute('alt');
            if (elementInnerText) {
                return elementInnerText;
            }
        }
        
        return null;
    }

    this.getPageInfo = function() {

        var product = Jisp.getProductElement();
        if (product) {
            this.productName = Jisp.getProductName(product);

            this.imageUrl = Jisp.getAttribute('#product-slider-for .slick-active img', 'src');
            if (!this.imageUrl) {
                this.imageUrl = Jisp.getMetaProperty(document, "og:image");
            }

            var priceInnerText = this.getPrice(document);
            this.priceAmount = Jisp.extractPrice(priceInnerText);  
            var currencyInnerText = this.getCurrency();
            this.priceCurrency = Jisp.extractCurrency(currencyInnerText);

            this.brandNameText = this.getBrand();
            
            var breadCrumbs = Jisp.getInnerTextArr('[itemtype="http://data-vocabulary.org/Breadcrumb"] a', ['Home']);
            this.categoryNameText = Jisp.extractCategoryName(breadCrumbs); 
        }
        else {
            this.except = true;
        }

        return Jisp.getResult(this);
    };
})();