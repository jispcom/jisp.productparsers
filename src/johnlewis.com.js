window.parser_johnlewis_com = new (function () {
    Jisp.init(this);
    if (!this.isProductPage) {
        this.url = document.location.href;
    }
    this.url = Jisp.ignoreSharpUrl(this.url);

    this.placeholderImageUrl = "https://jisp.com/Content/img/placeholders/johnlewis.com.png";

    this.ready = function () {
        return true;
    };

    this.checkChanges = function () {
        return null;
    };

    this.getBreadCrumbs = function(product) {
        var breadCrumbAll = [];
        var breadCrumbs = $('ol > li > a', $('[itemprop="breadcrumb"]', product));

        for (var i = 0; i < breadCrumbs.length; i++) {

            if ((breadCrumbs[i].href == "/") || (breadCrumbs[i].innerText == "Home Page")) {
                continue;
            }

            var breadCrumbInnerText = breadCrumbs[i].innerText;
            if (breadCrumbInnerText) {

                if ($(breadCrumbs[i]).parent().hasClass("last")) {
                    //if (breadCrumbInnerText.indexOf('View all ') === 0) {
                    breadCrumbInnerText = breadCrumbInnerText.replace('View all ', '');
                    //}
                }

                breadCrumbAll.push(breadCrumbInnerText);
            }
        }

        return breadCrumbAll;
    };

    this.getPageInfo = function() {

        var product = Jisp.getProductElement();
        if (product) {

            this.productName = Jisp.getInnerText('#prod-title [itemprop="name"]');

            var imageInnerText = Jisp.getAttribute('[itemprop="image"]', 'content');

            if (!imageInnerText) {
                var imageInnerTextElement = $('#prod-media-player .carousel .image').filter(function() {
                    return $(this).css('display') === 'list-item';
                })[0];
                if (imageInnerTextElement) {
                    var imgElement = $('img', $(imageInnerTextElement))[0];
                    if (imgElement) {
                        var attrValue = imgElement.getAttribute('src');
                        if (attrValue) {
                            imageInnerText = attrValue.trim();
                        }
                    }
                }
            }

            if (!imageInnerText) {
                imageInnerText = Jisp.getAttribute('[itemprop="image"]', 'src');
            }

            if (imageInnerText) {
                if (imageInnerText.indexOf("//") == 0) {
                    imageInnerText = "http:" + imageInnerText;
                }

                this.imageUrl = imageInnerText;
            }

            var priceInnerText = Jisp.getPrice(product);
            if (!priceInnerText) {
                priceInnerText = Jisp.getInnerText('#prod-price .price');
            }
            this.priceAmount = Jisp.extractPrice(priceInnerText);
            if ((!this.priceAmount) && (priceInnerText)) {
                priceInnerText = priceInnerText.toLowerCase();
                var nowIndex = priceInnerText.indexOf('now');
                if (nowIndex >= 0) {
                    priceInnerText = priceInnerText.substring(nowIndex + 3);
                    this.priceAmount = Jisp.extractPrice(priceInnerText);
                }
            }
            this.priceCurrency = Jisp.extractCurrency(priceInnerText);
            if (!this.priceCurrency) {
                priceInnerText = Jisp.getInnerText('#prod-price .price');
                this.priceCurrency = Jisp.extractCurrency(priceInnerText);
            }

            this.brandNameText = Jisp.getBrand(product);
            
            var breadCrumbs = this.getBreadCrumbs(document);
            this.categoryNameText = Jisp.extractCategoryName(breadCrumbs); 
        }
        else {
            this.except = true;
        }

        return Jisp.getResult(this);
    };
})();