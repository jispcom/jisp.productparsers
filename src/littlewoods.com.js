window.parser_littlewoords_com = new (function () {
    Jisp.init(this);

    this.placeholderImageUrl = "https://jisp.com/Content/img/placeholders/littlewoords.com.png";

    this.ready = function () {
        var image = Jisp.getAttribute('.amp-selected img.amp-just-image', 'src');
        return (image != null);
    };

    this.checkChanges = function () {
        return null;
    };

    this.getBrand = function () {
        var brandName = null;
        var brands = $('.productHeading');
        for (var i = 0; i < brands.length; i++) {
            var brandNameInnerHTML = brands[i].innerHTML;

            if (brandNameInnerHTML) {
                brandName = brandNameInnerHTML.replace(/<span.*>[\w\W]{1,}(.*?)[\w\W]{1,}<\/span>/gi, "");
            }
        }

        return brandName;
    }

    this.getPageInfo = function() {

        var product = Jisp.getProductElement();
        if (!product) {
            product = document;
        }

        this.productName = Jisp.getMetaName(document, "twitter:title");

        this.imageUrl = Jisp.getAttribute('.amp-selected img.amp-just-image', 'src');

        var priceInnerText = Jisp.getPrice(product);
        this.priceAmount = Jisp.extractPrice(priceInnerText);
        var currencyInnerText = Jisp.extractCurrency(priceInnerText);
        if(!currencyInnerText){
            currencyInnerText = Jisp.extractCurrency(Jisp.getCurrency(product));
        }
        this.priceCurrency = currencyInnerText;

        this.brandNameText = this.getBrand();

        var breadCrumbs = Jisp.getInnerTextArr('#breadcrumb a', ['Home', 'Next']);
        this.categoryNameText = Jisp.extractCategoryName(breadCrumbs);

        return Jisp.getResult(this);
    };
})();