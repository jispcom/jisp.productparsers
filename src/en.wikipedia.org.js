window.parser_en_wikipedia_org = new (function () {
    Jisp.init(this);

    this.ready = function () {
        return true;
    };

    this.checkChanges = function () {
        return null;
    };

    this.getPageInfo = function() {

        var product = Jisp.getProductElement();
        if (!product) {
            product = document;
        }

        this.productName = Jisp.getInnerText('#firstHeading');
        if (!this.productName) {
            this.productName = Jisp.getProductName(product);
        }

        this.imageUrl = Jisp.getAttribute('#bodyContent .thumb .image img, #bodyContent .vcard .image img, #bodyContent .infobox .image img, #bodyContent .vcard img[usemap]', 'src');
        
        if (!this.imageUrl) {
            this.imageUrl = Jisp.getImage(product);
        }

        if (this.imageUrl) {
            if (this.imageUrl.indexOf('//') === 0) {
                this.imageUrl ='https:' + this.imageUrl;
            }
        }

        // set default image to icon
        if (!this.imageUrl) {
            this.imageUrl = 'https://en.wikipedia.org/static/images/project-logos/enwiki.png';
        }

        var priceInnerText = Jisp.getPrice(product);
        this.priceAmount = Jisp.extractPrice(priceInnerText);
        var currencyInnerText = Jisp.extractCurrency(priceInnerText);
        if(!currencyInnerText){
            currencyInnerText = Jisp.extractCurrency(Jisp.getCurrency(product));
        }
        this.priceCurrency = currencyInnerText;

        this.brandNameText = Jisp.getBrand(product);

        var breadCrumbs = Jisp.getBreadCrumbs(document);
        this.categoryNameText = Jisp.extractCategoryName(breadCrumbs);

        return Jisp.getResult(this);
    };
})();