function initJispParser () {

    var hostNames = [
                    "www.amazon.co.uk",
                    "www.ebay.co.uk",
                    "www.argos.co.uk",
                    "www.next.co.uk",
                    "www3.next.co.uk",
                    "www.asos.com",
                    "www.johnlewis.com",
                    "www.marksandspencer.com",
                    "www.currys.com",
                    "www.currys.co.uk",
                    "www.boots.com",
                    "www.debenhams.com",
                    "www.very.co.uk",
                    "www.ikea.com",
                    "www.newlook.com",
                    "www.houseoffraser.co.uk",
                    "www.diy.com",
                    "www.screwfix.com",
                    "www.notonthehighstreet.com",
                    "www2.hm.com",
                    "www.pcworld.co.uk",
                    "www.topshop.com",
                    "www.ebuyer.com",
                    "www.zara.com",
                    "www.jdsports.co.uk",
                    "www.littlewoods.com",
                    "www.lookupbubbles.com",
                    "www.boohoo.com",
                    "www.tkmaxx.com",
                    "www.selfridges.com",
                    "www.homebase.co.uk",
                    "www.maplin.co.uk",
                    "www.halfords.com",
                    "www.riverisland.com",
                    "www.tesco.com",
                    "www.missguided.co.uk",
                    "www.mandmdirect.com",
                    "www.boden.co.uk",
                    "en.wikipedia.org",
                    "www.youtube.com",
                    "vimeo.com",
                    "www.dailymotion.com",
                    "www.imdb.com",
                    "twitter.com",
                    "www.jisp.com"
    ];

    var aliases = {};
    aliases['m.topshop.com'] = 'www.topshop.com';
    aliases['eu.riverisland.com'] = 'www.riverisland.com';
    aliases['m2.hm.com'] = 'www2.hm.com';
    aliases['m.next.co.uk'] = 'www.next.co.uk';
    aliases['m.boots.com'] = 'www.boots.com';
    aliases['m.asos.com'] = 'www.asos.com';
    aliases['m.debenhams.com'] = 'www.debenhams.com';
    aliases['m.ikea.com'] = 'www.ikea.com';
    aliases['m.newlook.com'] = 'www.newlook.com';
    aliases['m.ebuyer.com'] = 'www.ebuyer.com';
    aliases['m.jdsports.co.uk'] = 'www.jdsports.co.uk';
    aliases['stage.jisp.com'] = 'www.jisp.com';
    aliases['shopper-stage.jisp.com'] = 'www.jisp.com';
    aliases['shopper.jisp.com'] = 'www.jisp.com';

    var hostname = document.location.hostname;
    hostname = aliases[hostname] || hostname;

    var host = (hostNames.indexOf(hostname) >= 0) ?
        hostname.replace('www.', '').replace('www2.', '').replace('www3.', '').replace(/\./g, '_') :
            'defaultProductParser';

    return window["parser_" + host] || window["parser_defaultProductParser"];
};

window.jisp_parser = initJispParser();