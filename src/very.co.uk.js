window.parser_very_co_uk = new (function () {
    Jisp.init(this);
    this.url = Jisp.ignoreSharpUrl(this.url);

    this.placeholderImageUrl = "https://jisp.com/Content/img/placeholders/very.co.uk.png";

    this.ready = function () {
        var image = this.getImageUrl();
        return (!!image);
    };

    this.checkChanges = function () {
        return null;
    };

    this.getImageUrl = function() {
        var imageUrl = Jisp.getAttribute('.amp-selected img.amp-just-image', 'src');
        if (!imageUrl) {
            var imageUrlEncoded = Jisp.getMetaProperty(document, "og:image");
            if (imageUrlEncoded) {
                imageUrl = decodeURIComponent(imageUrlEncoded);
            }
        }

        return imageUrl;
    };

    this.getBrand = function() {
        var brandNameTextArr = [];
        var brands = $('#moreFrom-brandStoreLink a');

        if (brands.length > 0) {
            for (var i = 0; i < brands.length; i++) {
                var brandNameInner = brands[i].innerText;

                if (brandNameInner) {
                    brandNameTextArr.push(brandNameInner.trim());
                }
            }
        } else {
            brands = $('.productHeading');
            if (brands.length > 0) {
                var brandNameInner = brands[0].innerHTML;
                if (brandNameInner) {
                    brandNameTextArr.push(brandNameInner.replace(/<span.*>[\w\W]{1,}(.*?)[\w\W]{1,}<\/span>/gi, "")
                        .trim());
                }
            }
        }

        return brandNameTextArr.join(',');
    };

    this.getPageInfo = function() {

        var product = Jisp.getProductElement();
        if (product) {
            this.productName = Jisp.getMetaProperty(document, "og:title");

            this.imageUrl = this.getImageUrl();

            var priceInnerText = Jisp.getMetaProperty(document, "product:price:amount");
            this.priceAmount = Jisp.extractPrice(priceInnerText);
            var currencyInnerText = Jisp.getMetaProperty(document, "og:price:currency");
            this.priceCurrency = Jisp.extractCurrency(currencyInnerText);

            this.brandNameText = this.getBrand();
            
            var breadCrumbs = Jisp.getInnerTextArr('[itemtype="http://schema.org/BreadcrumbList"] [itemtype="http://schema.org/ListItem"] [itemprop="item"] [itemprop="name"]', ['Home']);
            this.categoryNameText = Jisp.extractCategoryName(breadCrumbs); 
        }
        else {
            this.except = true;
        }

        return Jisp.getResult(this);
    };
})();