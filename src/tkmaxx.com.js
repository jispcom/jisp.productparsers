window.parser_tkmaxx_com = new (function () {
    Jisp.init(this);
    this.url = Jisp.ignoreSharpUrl(this.url);

    this.placeholderImageUrl = "https://jisp.com/Content/img/placeholders/tkmaxx.com.png";

    this.ready = function () {
        return true;
    };

    this.checkChanges = function () {
        return null;
    };

    this.getPageInfo = function() {

        if (!this.isProductPage) {
            this.url = document.location.href;
        }

        var product = Jisp.getProductElement();
        if (product) {
            this.productName = Jisp.getProductName(product);

            this.imageUrl = Jisp.getAttribute('#productdetail-image img', 'src');
            if (!this.imageUrl) {
                this.imageUrl = Jisp.getMetaProperty(document, "og:image");
            }

            var priceInnerText = Jisp.getPrice(product);
            this.priceAmount = Jisp.extractPrice(priceInnerText);
            this.priceCurrency = Jisp.extractCurrency(priceInnerText);

            this.brandNameText = Jisp.getInnerText('.product-brand');
            
            var breadCrumbs = Jisp.getInnerTextArr('.crumbtrail a.crumbtrail-anchor', []);
            breadCrumbs = breadCrumbs.map(function (name) {
                return name.replace('View All:', '').trim();
            });
            this.categoryNameText = Jisp.extractCategoryName(breadCrumbs); 
        }
        else {
            this.except = true;
        }

        return Jisp.getResult(this);
    };
})();