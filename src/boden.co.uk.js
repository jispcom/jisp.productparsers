window.parser_boden_co_uk = new (function () {
    Jisp.init(this);

    this.placeholderImageUrl = "https://jisp.com/Content/img/placeholders/boden.co.uk.png";

    this.ready = function () {
        return true;
    };

    this.checkChanges = function () {
        return null;
    };

    this.getPageInfo = function() {

        this.isProductPage = Jisp.isExistsElement('#pm-product');

        var product = Jisp.getProductElement();
        if (product) {
            this.productName = Jisp.getInnerText('#pm-productDetail [itemprop="name"]');
            if (!this.productName) {
                this.productName = Jisp.getMetaProperty(document, "og:title");
            }

            this.imageUrl = Jisp.getAttribute('[itemprop="image"][src]:not([src=""])', 'src');
            if (!this.imageUrl) {
                this.imageUrl = Jisp.getImage(document);
            }

            var priceInnerText = Jisp.getInnerText('#pm-product #pm-productPrices .pdpNowPrice');
            this.priceAmount = Jisp.extractPrice(priceInnerText);
            this.priceCurrency = Jisp.extractCurrency(priceInnerText);

            this.brandNameText = Jisp.getMeta(document, 'itemprop="brand"');

            var breadCrumbs = Jisp.getInnerTextArr('#breadcrumb a', ['Home']);
            this.categoryNameText = Jisp.extractCategoryName(breadCrumbs);
        }
        else {
            this.except = true;
        }

        return Jisp.getResult(this);
    };
})();