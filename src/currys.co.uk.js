window.parser_currys_co_uk = new (function () {
    Jisp.init(this);
    this.url = Jisp.ignoreSharpUrl(this.url);

    this.placeholderImageUrl = "https://jisp.com/Content/img/placeholders/currys.co.uk.png";

    this.title = null;
    var titleElement = document.getElementsByTagName("title");
    if ((titleElement) && titleElement.length > 0) {
        titleElement = document.getElementsByTagName("title")[0];
        if (titleElement) {
            this.title = titleElement.innerText;
        }
    }

    this.ready = function () {
        return true;
    };

    this.checkChanges = function () {
        return null;
    };

    this.getImage = function () {
        var imageUrl = null;

        var image = $('#product-main .swiper-slide-active .product-image');
        if (!image) {
            image = $('[itemprop="image"][src]:not([src=""])');
        }

        if (image) {
            image = image.first()[0];
        }

        if (image) {
            imageUrl = image.getAttribute('src');
        }

        if (!imageUrl) {
            imageUrl = Jisp.getMetaName(document, "twitter:image");
        }

        if (!imageUrl) {
            imageUrl = Jisp.getMetaProperty(document, "og:image");
        }

        if (!imageUrl) {
            imageUrl = Jisp.getMetaName(document, "og:image");
        }

        return (imageUrl) ? imageUrl : null;
    };

    this.getPageInfo = function() {

        var product = Jisp.getProductElement();

        this.isProductPage = (product) || Jisp.isExistsElement('.product-page');

        if (!product) {
            product = document;
        }

        this.productName = Jisp.getMetaProperty(document, "og:title");

        this.imageUrl = this.getImage(product);

        var priceInnerText = Jisp.getPrice(product);
        if (!priceInnerText) {
            priceInnerText = Jisp.getInnerText('.product-page [data-key="current-price"]');
        }
        this.priceAmount = Jisp.extractPrice(priceInnerText);
        var currencyInnerText = Jisp.extractCurrency(priceInnerText);
        if(!currencyInnerText){
            currencyInnerText = Jisp.extractCurrency(Jisp.getCurrency(product));
        }
        this.priceCurrency = currencyInnerText;

        this.brandNameText = Jisp.getBrand(product);

        var breadCrumbs = Jisp.getBreadCrumbs(document);
        this.categoryNameText = Jisp.extractCategoryName(breadCrumbs);

        return Jisp.getResult(this);
    };
})();