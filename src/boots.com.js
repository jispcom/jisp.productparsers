window.parser_boots_com = new (function () {
    Jisp.init(this);

    this.url = Jisp.ignoreSharpUrl(this.url);

    this.placeholderImageUrl = "https://jisp.com/Content/img/placeholders/boots.com.png";

    this.ready = function () {
        var image = this.getImage();
        return image != null;
    };

    this.checkChanges = function () {
        return null;
    };

    this.getBrand = function (product) {
        var brandNameText = null;
        var brands = $('.brandBox .brandLogo img[alt]', product);
        for (var i = 0; i < brands.length; i++) {
            brandNameText = brands[i].alt;
        }

        if (brandNameText == null) {
            brands = $('#brandHeader a[href]', product);
            for (var i = 0; i < brands.length; i++) {
                
                var brandUrl = brands[i].href;  // /en/Wahl/
                if (brandUrl) {
                    var brandParts = brandUrl.split('/');
                    for (var j = 0; j < brandParts.length; j++) {
                        if(brandParts[j])
                        {
                            brandNameText = brandParts[j];
                        }
                    }
                }
            }
        }

        return brandNameText;
    }

    this.getImage = function () {
        var image = null;

        var img = document.querySelectorAll('.s7thumb[state="selected"]')[0];
        if (img) {
            var style = window.getComputedStyle(img);

            var imgUrl = null;

            for (var i = 0; i < style.length; i++) {
                var prop = style[i];
                if (prop === 'background-image') {
                    imgUrl = style.getPropertyValue(prop);
                    break;
                }
            }

            if ((imgUrl) && (imgUrl !== 'undefined')) {
                imgUrl = String(imgUrl);

                //startsWith not supported in phantomjs
                if (imgUrl.substr(0, 'url("'.length) === 'url("') {
                    imgUrl = imgUrl.substr(5).replace('")', '');
                    image = imgUrl;
                }

                if (!image) {
                    if (imgUrl.substr(0, 'url('.length) === 'url(') {
                        imgUrl = imgUrl.substr(4).replace(')', '');
                        image = imgUrl;
                    }
                }
            }
        }

        if (image) {
            if (/[^\s]*&wid=[\d]*&hei=[\d]*/.test(image)) {
                image = image.replace(/&wid=[\d]*&hei=[\d]*/, "&wid=2000&hei=2000");
            }
        }

        return image;
    }

    this.getPageInfo = function () {

        this.isProductPage = Jisp.isExistsElement('#estore_productpage_template_container') ||
        (Jisp.isExistsElement('.productDetails') && (document.location.hostname === 'm.boots.com'));

        if (!this.isProductPage) {
            this.url = document.location.href;
        }

        this.productName = Jisp.getInnerText('#estore_productpage_template_container [itemprop="name"]');

        this.imageUrl = this.getImage();

        var priceInnerText = Jisp.getInnerText('#PDP_productPrice'); 
        this.priceAmount = Jisp.extractPrice(priceInnerText);
        this.priceCurrency = Jisp.extractCurrency(priceInnerText);

        this.brandNameText = this.getBrand(document);

        var breadCrumbs = Jisp.getInnerTextArr('#widget_breadcrumb a', ['Home']);
        this.categoryNameText = Jisp.extractCategoryName(breadCrumbs);

        if (!this.except) {
            this.url = Jisp.ignoreQueryUrl(this.url);
        }

        return Jisp.getResult(this);
    };
})();