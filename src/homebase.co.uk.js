window.parser_homebase_co_uk = new (function () {
    Jisp.init(this);
    this.url = Jisp.ignoreSharpUrl(this.url);

    this.placeholderImageUrl = "https://jisp.com/Content/img/placeholders/homebase.co.uk.png";

    this.ready = function () {
        return true;
    };

    this.checkChanges = function () {
        return null;
    };

    this.getPageInfo = function() {

        var product = Jisp.getProductElement();
        if (!product) {
            product = document;
        }

        this.productName = Jisp.getProductName(product);

        if (this.isProductPage) {
            var findedImageUrl = Jisp.getAttribute('#main img.cycle-slide-active', 'src');
            if (!findedImageUrl) {
                findedImageUrl = Jisp.getImage(product);
            }

            if (findedImageUrl) {
                if (findedImageUrl.indexOf("//") == 0) {
                    findedImageUrl = "http:" + findedImageUrl;
                }
            }
            this.imageUrl = findedImageUrl;
        }

        var priceInnerText = Jisp.getPrice(product);
        this.priceAmount = Jisp.extractPrice(priceInnerText);
        var currencyInnerText = Jisp.extractCurrency(priceInnerText);
        if(!currencyInnerText){
            currencyInnerText = Jisp.extractCurrency(Jisp.getCurrency(product));
        }
        this.priceCurrency = currencyInnerText;

        this.brandNameText = Jisp.getBrand(product);

        var breadCrumbs = Jisp.getBreadCrumbs(document);
        this.categoryNameText = Jisp.extractCategoryName(breadCrumbs);

        return Jisp.getResult(this);
    };
})();