window.parser_ikea_com = new (function () {
    Jisp.init(this);
    this.url = Jisp.ignoreSharpUrl(this.url);

    this.placeholderImageUrl = "https://jisp.com/Content/img/placeholders/ikea.com.png";

    this.ready = function () {
        return true;
    };

    this.checkChanges = function () {
        return null;
    };

    this.getBreadCrumbs = function (product) {
        var breadCrumbAll = [];

        var categoryText = Jisp.getMetaName(product, "IRWStats.categoryLocal");
        if (categoryText) {
            breadCrumbAll.push(categoryText);
        }

        var subcategoryText = Jisp.getMetaName(product, "IRWStats.subCategoryLocal");
        if (subcategoryText) {
            breadCrumbAll.push(subcategoryText);
        }

        var chapterText = Jisp.getMetaName(product, "IRWStats.chapterLocal");
        if (chapterText) {
            breadCrumbAll.push(chapterText);
        }

        return breadCrumbAll;
    }

    this.getPageInfo = function() {
        this.isProductPage = this.isProductPage; //&& Jisp.isExistsElement('#productInfoWrapper1');

        var product = Jisp.getProductElement();
        if (this.isProductPage) {
            var name = Jisp.getInnerText('#productNameProdInfo');
            var type = Jisp.getInnerText('#productTypeProdInfo');
            this.productName = ((name) ? name + ' ' : '') + ((type) ? type : '');
            if (!this.productName) {
                this.productName = Jisp.getInnerText('#schemaProductName');
            }

            if (!this.productName) {
                var productDescr = Jisp.getInnerText('[itemtype="http://schema.org/Product"] [itemprop="name"] .pie-description, [itemtype="https://schema.org/Product"] [itemprop="name"] .pie-description');
                var productBrand = Jisp.getInnerText('[itemtype="http://schema.org/Product"] [itemprop="name"] .global-name, [itemtype="https://schema.org/Product"] [itemprop="name"] .global-name');
                this.productName = (productDescr || '') + ' ' + (productBrand || '');
                //this.productName = Jisp.getInnerText('[itemtype="http://schema.org/Product"] .global-name');
            }

            if (this.productName) {
                this.productName = this.productName.replace(/\n/g, "").replace(/\t/g, "");
            }

            this.imageUrl = Jisp.getAttribute('#productImg', 'src');
            if (!this.imageUrl) {
                var images = $('[itemtype="http://schema.org/Product"] [data-carousel="true"] li[aria-hidden="false"] img, [itemtype="https://schema.org/Product"] [data-carousel="true"] li[aria-hidden="false"] img').filter(function () {
                    return ($(this).css('display') == 'inline') && ($(this).attr("width") > 1);
                });

                for (var i = 0; i < images.length; i++) {
                    var findedImageUrl = images[i].getAttribute('src');
                    if (findedImageUrl) {
                        this.imageUrl = findedImageUrl;
                    }
                }
            }

            if (!this.imageUrl) {
                this.imageUrl = Jisp.getImage();
            }

            if (this.imageUrl) {
                if (this.imageUrl.indexOf("/") == 0) {
                    this.imageUrl = document.location.origin + this.imageUrl;
                }
            }
      
            var priceInnerText = Jisp.getPrice(product);
            this.priceAmount = Jisp.extractPrice(priceInnerText);
            this.priceCurrency = Jisp.extractCurrency(priceInnerText);

            if (!this.priceCurrency) {
                var currencyInnerText = Jisp.getCurrency(product);
                this.priceCurrency = Jisp.extractCurrency(currencyInnerText);
            }

            this.brandNameText = Jisp.getBrand(product);
            if (!this.brandNameText) {
                this.brandNameText = Jisp.getMetaName(document, 'manufacturer');
            }

            var breadCrumbs = Jisp.getInnerTextArr('[itemtype="http://schema.org/BreadcrumbList"] [itemtype="http://schema.org/ListItem"] [itemprop="url"], [itemtype="http://schema.org/BreadcrumbList"] [itemtype="http://schema.org/ListItem"] [itemprop="url"]', ['home']);
            this.categoryNameText = Jisp.extractCategoryName(breadCrumbs);
        }
        else {
            this.except = true;
        }

        return Jisp.getResult(this);
    };
})();