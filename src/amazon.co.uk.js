window.parser_amazon_co_uk = new (function () {
    Jisp.init(this);

    this.placeholderImageUrl = "https://jisp.com/Content/img/placeholders/amazon.co.uk.png";

    this.ready = function () {
        var image = this.getImage(document);
        if (image) {
            return (image.trim().indexOf('data:') != 0);
        }
        else {
            return false;
        }
    };

    this.checkChanges = function () {
        return null;
    };

    this.getProductName = function(product) {
        var productNames = $('#productTitle', product);

        if (productNames.length == 0) {
            productNames = $('#title_feature_div', product);
        }

        if (productNames.length == 0) {
            productNames = $('#btAsinTitle', product);
        }

        if (this.isProductPage) {
            if (productNames.length == 0) {
                productNames = $('h1', product);
            }
        }

        for (var i = 0; i < productNames.length; i++) {
            var productNameTextFinded = productNames[i].innerText;
            if (productNameTextFinded) {
                return productNameTextFinded;
            }
        }

        return null;
    };

    this.getImage = function(product) {

        var images = $('.image.selected img', product);

        if (images.length == 0) {
            images = $('#landingImage', product);
        }

        if (images.length == 0) {
            images = $('#main-image', product);
        }

        if (images.length == 0) {
            //http://www.amazon.co.uk/gp/product/111894691X/ref=s9_hps_bw_g14_i4?pf_rd_m=A3P5ROKL5A1OLE&pf_rd_s=merchandised-search-4&pf_rd_r=0X44GYJ0NMWHJJNWGC4Y&pf_rd_t=101&pf_rd_p=823142447&pf_rd_i=266239
            images = $('#imgBlkFront', product);
        }

        if (images.length == 0) {
            images = $('#ppd-left img', product);
        }

        if (images.length == 0) {
            images = $('#js-masrw-main-image', product);
        }

        //gift cards pages https://www.amazon.co.uk/Thank-You-Teacher-Printable-Amazon-co-uk/dp/B00DW8FYI0/ref=sr_1_27?s=gift-cards&ie=UTF8&qid=1469716559&sr=1-27
        if (images.length == 0) {
            images = $('#gc-live-preview-Designs img', product);
        }

        //ebook pages https://www.amazon.co.uk/Secret-1-Bestselling-Author-ebook/dp/B01D57H430/ref=tmm_kin_swatch_0?_encoding=UTF8&qid=&sr=
        if (images.length == 0) {
            images = $('#ebooks-img-canvas img', product);
        }

        //https://www.amazon.co.uk/dp/B00ESJDS62/ref=twister_B00ESJDR9A?_encoding=UTF8&psc=1
        if (images.length == 0) {
            images = $('#zeroes-coin-image', product);
        }

        //https://www.amazon.co.uk/Hateful-Eight-Samuel-L-Jackson/dp/B01E0AI0RU
        if (images.length == 0) {
            images = $('.dp-img-bracket img', product);
        }

        //if (Array.isArray(images)) {
            for (var i = 0; i < images.length; i++) {
                var findedImageUrl = images[i].getAttribute('src');
                if (findedImageUrl) {
                    return findedImageUrl;
                }
            }
        //} else {
        //    if (images) {
        //        if (typeof images['getAttribute'] === 'function') {
        //            var oneImageUrl = images.getAttribute('src');
        //            if (oneImageUrl) {
        //                return oneImageUrl;
        //            }
        //        }

        //        //var oneImageUrl = images.getAttribute('src');
        //        //if (oneImageUrl) {
        //        //    return oneImageUrl;
        //        //}
        //    }
        //}

        return null;
    };

    this.getPrice = function(product) {
        var prices = $('#priceblock_dealprice', product);

        if (prices.length == 0) {
            prices = $('#priceblock_ourprice', product);
        }

        if (prices.length == 0) {
            prices = $('#priceblock_saleprice', product);
        }
        if (prices.length == 0) {
            prices = $('#actualPriceValue', product);

            for (var i = 0; i < prices.length; i++) {
                var priceTextFinded = prices[i].innerText;
                if (priceTextFinded) {
                    return priceTextFinded;
                }
            }
        }
        if (prices.length == 0) {
            prices = $('.a-color-price', $('[data-feature-name="olp"]', product));
        }

        if (prices.length == 0) {
            //http://www.amazon.co.uk/gp/product/111894691X/ref=s9_hps_bw_g14_i4?pf_rd_m=A3P5ROKL5A1OLE&pf_rd_s=merchandised-search-4&pf_rd_r=0X44GYJ0NMWHJJNWGC4Y&pf_rd_t=101&pf_rd_p=823142447&pf_rd_i=266239
            prices = $('.a-color-price', $('#buybox', product));

            for (var i = 0; i < prices.length; i++) {
                var priceTextFinded = prices[i].innerText;
                if (priceTextFinded) {
                    return priceTextFinded.replace(/<p.*>[\w\W]{1,}(.*?)[\w\W]{1,}<\/p>/gi, "");
                }
            }
        }

        if (prices.length == 0) {
            //https://www.amazon.co.uk/Samsung-Galaxy-32GB-SIM-Free-Smartphone/dp/B01C5OIIF2/ref=sr_1_7?s=telephone&ie=UTF8&qid=1473068615&sr=1-7
            prices = $('#unqualifiedBuyBox .a-color-price');
        }

        //http://www.amazon.co.uk/gp/product/B009O1P1YQ/ref=s9_al_bw_g75_i2?pf_rd_m=A3P5ROKL5A1OLE&pf_rd_s=merchandised-search-5&pf_rd_r=156GN1VA9FXHC6VRVW1S&pf_rd_t=101&pf_rd_p=535038447&pf_rd_i=60036031


        if (prices.length == 0) {
            prices = $('#priceMessagingToBuy', product);
        }

        if (prices.length == 0) {
            prices = $('#priceBlock .banjoPrice', product);
        }

        if (prices.length == 0) {
            prices = $('#MediaMatrix li.selected .a-color-price', product);
        }

        if (prices.length == 0) {
            prices = $('#guild-buybox-container .guild_priceblock_ourprice', product);
        }

        for (var i = 0; i < prices.length; i++) {
            var priceTextFinded = prices[i].innerText;
            if (priceTextFinded) {
                return priceTextFinded;
            }
        }

        return null;
    };


    this.getBrand = function(product) {
        var brandNameTextArr = [];
        var brands = $('#brand', product);
        for (var i = 0; i < brands.length; i++) {
            var brandNameInner = brands[i].innerText;

            if (brandNameInner) {
                brandNameTextArr.push(brandNameInner);
            }
        }

        return brandNameTextArr.join(',');
    };

    this.getBreadCrumbs = function(product) {
        var breadCrumbAll = [];
        var breadCrumbs = $('#wayfinding-breadcrumbs_container', product);

        if (breadCrumbs.length > 0) {
            var backLink = $('a#breadcrumb-back-link', breadCrumbs);

            if (backLink.length > 0) {
                breadCrumbs = null;
            } else {
                breadCrumbs = $('a', breadCrumbs);
            }
        } else {
            breadCrumbs = null;
        }

        if (breadCrumbs == null) {
            if (Jisp.isExistsElement('#dmusic_buybox_container') ||
                Jisp.isExistsElement('[data-feature-name="dmusicTracklist"]')) {
                breadCrumbAll.push('Music Downloads');
                return breadCrumbAll;
            }
        }

        if (breadCrumbs == null) {
            // find breadcrumbs http://www.amazon.co.uk/gp/product/B006Y7DBHC/ref=s9_hps_bw_g201_i2?pf_rd_m=A3P5ROKL5A1OLE&pf_rd_s=merchandised-search-4&pf_rd_r=1HG53H6N3TW8VEMM5R65&pf_rd_t=101&pf_rd_p=669514967&pf_rd_i=60041031
            breadCrumbs = $('div.bucket', product);

            var breadCrumbElement = null;

            var ids = [
                "quickPromoBucketContent", "fallbacksession-sims-feature", "A9AdsMiddleBoxTop", "sims_fbt", "vtpsims",
                "productDescription"
            ];

            for (var i = 0; i < breadCrumbs.length; i++) {
                if (ids.indexOf(breadCrumbs[i].id) >= 0) {
                    continue;
                }

                var nestedElements = $('div.content > ul > li > a', breadCrumbs[i]);
                if (nestedElements.length > 0) {
                    breadCrumbElement = breadCrumbs[i];
                    break;
                }
            }

            if (breadCrumbElement != null) {
                breadCrumbs = $('div.content > ul > li', breadCrumbElement).first();
                breadCrumbs = $('a', breadCrumbs);
            } else {
                breadCrumbs = null;
            }
        }

        if (breadCrumbs == null) {
            //http://www.amazon.co.uk/CROSSHATCH-PLIXXIE-JACKET-PADDED-DESIGNER/dp/B018VPSY2Q/ref=sr_1_2?s=clothing&ie=UTF8&qid=1457624889&sr=1-2&keywords=coat
            breadCrumbs = $('a', $('#browse_feature_div', product));
        }

        if (breadCrumbs != null) {
            for (var i = 0; i < breadCrumbs.length; i++) {
                var breadCrumbInner = breadCrumbs[i].innerText;

                if (breadCrumbInner) {
                    breadCrumbAll.push(breadCrumbInner.trim());
                }
            }
        }

        return breadCrumbAll;
    };

    this.getPageInfo = function () {

        var amazonProductID = (this.url) ? this.url.match(/\/dp\/[\d+,\w+]{10}/) : null;
        if (!(amazonProductID)) {
            amazonProductID = (document.location.href) ? document.location.href.match(/\/dp\/[\d+,\w+]{10}/) : null;
        }
        if (!(amazonProductID)) {
            amazonProductID = (document.location.href) ? document.location.href.match(/\/product\/[\d+,\w+]{10}/) : null;
        }
        if (!(amazonProductID)) {

            var productCodeElement = $('input[name="a"]');
            var productCodeInMobile = (productCodeElement) ? productCodeElement.attr('value') : null;
            var isProductPageInMobile = (productCodeInMobile) ? /\d/.test(productCodeInMobile) : false;


            var elementExists = Jisp.isExistsElement('#dp-container');
            amazonProductID = (document.location.href && (elementExists || isProductPageInMobile)) ? document.location.href.match(/\/[\d+,\w+]{10}/) : null;
        }
        this.isProductPage = (amazonProductID) ? /\d/.test(amazonProductID) : false;

        if (!this.isProductPage) {
            this.url = document.location.href;
        }

        this.productName = this.getProductName(document);

        this.imageUrl = this.getImage(document);
        if (this.imageUrl) {
            this.imageUrl = this.imageUrl.trim();
        }

        var priceInnerText = this.getPrice(document);
        this.priceAmount = Jisp.extractPrice(priceInnerText);
        this.priceCurrency = Jisp.extractCurrency(priceInnerText);

        var brandName = this.getBrand(document);
        if (brandName) {
            brandName = brandName.trim();
            if (brandName.indexOf('<') < 0) {
                this.brandNameText = brandName;
            }
        }

        var breadCrumbs = this.getBreadCrumbs(document);
        this.categoryNameText = Jisp.extractCategoryName(breadCrumbs);

        return Jisp.getResult(this);
    };
})();