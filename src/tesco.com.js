window.parser_tesco_com = new (function () {
    Jisp.init(this);

    this.placeholderImageUrl = "https://jisp.com/Content/img/placeholders/tesco.com.png";

    this.ready = function () {
        return true;
    };

    this.checkChanges = function () {
        return null;
    };

    this.getPageInfo = function () {

        var product = Jisp.getProductElement();
        if (!product) {
            product = document;
        }

        this.productName = Jisp.getProductName(product);

        var elements = $('#prodZoomView .s7staticimage img').not('#prodZoomView .s7staticimage img[style*="visibility: hidden"]');
        if (elements) {
            for (var i = 0; i < elements.length; i++) {
                var attrValue = elements[i].getAttribute('src');
                if (attrValue) {
                    this.imageUrl = attrValue.trim();
                }
            }
        }

        if (!this.imageUrl) {
            this.imageUrl = Jisp.getImage(product);
        }

        var priceInnerText = Jisp.getPrice(product);
        this.priceAmount = Jisp.extractPrice(priceInnerText);
        var currencyInnerText = Jisp.extractCurrency(priceInnerText);
        if (!currencyInnerText) {
            currencyInnerText = Jisp.extractCurrency(Jisp.getCurrency(product));
        }
        this.priceCurrency = currencyInnerText;

        this.brandNameText = Jisp.getBrand(product);

        var breadCrumbs = Jisp.getBreadCrumbs(document);
        this.categoryNameText = Jisp.extractCategoryName(breadCrumbs);

        return Jisp.getResult(this);
    };
})();