window.parser_diy_com = new (function () {
    Jisp.init(this);

    if (!this.isProductPage) {
        this.url = document.location.href;
    }

    this.placeholderImageUrl = "https://jisp.com/Content/img/placeholders/diy.com.png";

    this.ready = function () {
        return true;
    };

    this.checkChanges = function () {
        return null;
    };

    this.getBrand = function () {
        var elements = $('[aria-labelledby="product-details"] .media-body table th').filter(function () {
            return $(this).text().toLowerCase().indexOf('Brand'.toLowerCase()) >= 0;
        });
        elements = elements.next();
        for (var i = 0; i < elements.length; i++) {
            var elementInnerText = elements[i].innerText;
            if (elementInnerText) {
                return elementInnerText;
            }
        }
        return null;
    }

    this.getPageInfo = function() {

        var product = Jisp.getProductElement();
        if (product) {
            this.productName = Jisp.getProductName(product);

            this.imageUrl = Jisp.getMetaProperty(document, "og:image");

            var priceInnerText = Jisp.getPrice(product); //<meta itemprop="priceCurrency" content="GBP">
            console.log(priceInnerText);
            this.priceAmount = Jisp.extractPrice(priceInnerText);
            this.priceCurrency = Jisp.extractCurrency(priceInnerText);

            this.brandNameText = this.getBrand();
            
            var breadCrumbs = Jisp.getInnerTextArr('[itemprop="breadcrumb"] a', ['Home']);
            breadCrumbs = breadCrumbs.map(function (name) {
                return name.replace('[...]', '').trim();
            });
            this.categoryNameText = Jisp.extractCategoryName(breadCrumbs); 
        }
        else {
            this.except = true;
        }

        return Jisp.getResult(this);
    };
})();