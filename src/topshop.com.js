window.parser_topshop_com = new (function () {
    Jisp.init(this);
    if (this.url == document.location.href) {
        var customCanonicalUrl = Jisp.getInnerText('#un_pdp_canonical');
        if (customCanonicalUrl) {
            this.url = customCanonicalUrl;
        }
    }

    this.placeholderImageUrl = "https://jisp.com/Content/img/placeholders/topshop.com.png";

    this.ready = function () {
        var ready = false;
        if (Jisp.isExistsElement('#cboxLoadedContent')) {
            var productName = Jisp.getInnerText('#cboxLoadedContent .product_name');
            var productPrice = Jisp.getInnerText('#cboxLoadedContent .product_price');
            ready = (!!productName) && (!!productPrice);
        } else {
            var breadCrumbs = Jisp.getInnerTextArr('[itemtype="http://data-vocabulary.org/Breadcrumb"] a', ['Home']);
            ready = (breadCrumbs.length > 0);
        }
        
        return ready;
    };

    this.checkChanges = function () {
        var productName = Jisp.getInnerText('#cboxLoadedContent .product_name');

        return productName;
    };

    this.getPageInfo = function () {

        var isModalWindow = Jisp.isExistsElement('#cboxLoadedContent');
        this.isProductPage = Jisp.isExistsElement('#product-detail') || isModalWindow;

        if (isModalWindow) {
            var productUrl = Jisp.getAttribute('#cboxLoadedContent #fullProductBtn a', 'href');
            if (productUrl)
                this.url = productUrl;
        }

        this.productName = Jisp.getInnerText('#cboxLoadedContent .product_name');
        if (!this.productName) {
            this.productName = Jisp.getMetaProperty(document, "og:description");
        }

        this.imageUrl = Jisp.getAttribute('.product_hero__image li.current img', 'src');
        if (!this.imageUrl) {
            this.imageUrl = Jisp.getMetaProperty(document, "og:image");
        }

        var priceInnerText = Jisp.getInnerText('#cboxLoadedContent .product_price');
        if (!priceInnerText) {
            priceInnerText = Jisp.getMetaProperty(document, "og:price:amount");
        }

        this.priceAmount = Jisp.extractPrice(priceInnerText);
        this.priceCurrency = Jisp.extractCurrency(priceInnerText);
        if (!this.priceCurrency) {
            var currencyInnerText = Jisp.getMetaProperty(document, "og:price:currency");
            this.priceCurrency = Jisp.extractCurrency(currencyInnerText);
        }

        this.brandNameText = Jisp.getBrand(document);

        var breadCrumbs = Jisp.getInnerTextArr('[itemtype="http://data-vocabulary.org/Breadcrumb"] a', ['Home']);
        this.categoryNameText = Jisp.extractCategoryName(breadCrumbs);

        return Jisp.getResult(this);
    };
})();