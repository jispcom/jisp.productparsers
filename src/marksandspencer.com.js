window.parser_marksandspecner_com = new (function () {
    Jisp.init(this);
    this.url = Jisp.ignoreSharpUrl(this.url);

    this.placeholderImageUrl = "https://jisp.com/Content/img/placeholders/marksandspecner.com.png";

    this.ready = function () {
        var image = this.getImage();
        return image != null;
    };

    this.checkChanges = function () {
        var productName = this.getProductName();

        return productName;
    };

    this.getProductName = function () {

        var productName = Jisp.getInnerText('.product-title [itemprop="name"]');
        if (!productName) {
            productName = Jisp.getInnerText('.product-title [itemprop="PDPUrl"]');
        }

        return productName;
    }

    this.getBrand = function () {
        var elements = $('[data-brand]');
        for (var i = 0; i < elements.length; i++) {
            var elementInnerText = elements[i].getAttribute('data-brand');
            if (elementInnerText) {
                return elementInnerText;
            }
        }
        return null;
    }

    this.getImage = function () {
        var imageInnerText = null;

        var image = $('.s7staticimage img[src]:not([src=""])');
        if (image) {
            image = image.first()[0];
        }

        if (!image) {
            image = $('#product-detail-page img');
            if (image) {
                image = image.first()[0];
            }
        }

        if (image) {
            imageInnerText = image.getAttribute('src');
        }

        return imageInnerText;
    }
    
    this.getPageInfo = function() {

        var product = $('#product-detail-page[itemtype="http://schema.org/Product"], #product-detail-page[itemtype="https://schema.org/Product"]')[0];
        if (product) {
            this.isProductPage = true;
            this.productName = this.getProductName();

            this.imageUrl = this.getImage();

            if (!this.imageUrl) {
                this.imageUrl = Jisp.getMetaProperty(document, "og:image");
            }

            if (this.imageUrl) {
                if (this.imageUrl.indexOf("//") == 0) {
                    this.imageUrl = "http:" + this.imageUrl;
                }
            }

            if (this.imageUrl) {
                if (/[^\s]*&wid=[\d]*&hei=[\d]*/.test(this.imageUrl)) {
                    this.imageUrl = this.imageUrl.replace(/&wid=[\d]*&hei=[\d]*/, "&wid=2000&hei=2000");
                }
            }

            var priceInnerText = Jisp.getPrice(product);
            if (!priceInnerText) {
                priceInnerText = Jisp.getAttribute('[itemtype="http://schema.org/Product"] [itemprop="price"], [itemtype="https://schema.org/Product"] [itemprop="price"]', "content");
            }
            if (!priceInnerText) {
                priceInnerText = Jisp.getInnerText('[itemtype="http://schema.org/Product"] [data-mapping="price"], [itemtype="https://schema.org/Product"] [data-mapping="price"]');
            }
            if (!priceInnerText) {
                priceInnerText = Jisp.getAttribute('[itemtype="http://schema.org/Product"] [data-mapping="price"], [itemtype="https://schema.org/Product"] [data-mapping="price"]', "data-value");
            }
            
            this.priceAmount = Jisp.extractPrice(priceInnerText);
            this.priceCurrency = Jisp.extractCurrency(priceInnerText);
            if (!this.priceCurrency) {
                var currencyInnerText = Jisp.getAttribute('[itemtype="http://schema.org/Product"] [itemprop="priceCurrency"], [itemtype="https://schema.org/Product"] [itemprop="priceCurrency"]', "content");
                this.priceCurrency = Jisp.extractCurrency(currencyInnerText);
            }

            this.brandNameText = this.getBrand();
            
            var breadCrumbs = Jisp.getInnerTextArr('.breadcrumb a', ['Home']);
            this.categoryNameText = Jisp.extractCategoryName(breadCrumbs); 
        }
        else {
            this.except = true;
        }

        return Jisp.getResult(this);
    };
})();