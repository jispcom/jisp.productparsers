window.parser_newlook_com = new (function () {
    Jisp.init(this);

    this.placeholderImageUrl = "https://jisp.com/Content/img/placeholders/newlook.com.png";

    this.ready = function () {
        return true;
    };

    this.checkChanges = function () {
        return null;
    };

    this.getPageInfo = function () {

        this.isProductPage = Jisp.isExistsElement('#productDetailForm');

        this.productName = Jisp.getMetaProperty(document, "og:title");

        this.imageUrl = Jisp.getAttribute('.main-image', 'src');
        if (!this.imageUrl) {
            this.imageUrl = Jisp.getMetaProperty(document, "og:image");
        }
        if (this.imageUrl && this.imageUrl.indexOf('//') === 0) {
            this.imageUrl = 'http:' + this.imageUrl;
        }

        var priceInnerText = Jisp.getMetaProperty(document, "og:price:amount");
        this.priceAmount = Jisp.extractPrice(priceInnerText);
        var currencyInnerText = Jisp.getMetaProperty(document, "og:price:currency");
        this.priceCurrency = Jisp.extractCurrency(currencyInnerText);

        this.brandNameText = Jisp.getBrand(document);
        if (!this.brandNameText) {
            this.brandNameText = 'New Look';
        }

        var breadCrumbs = Jisp.getInnerTextArr('.breadcrumb a', ['Back', 'Home']);
        this.categoryNameText = Jisp.extractCategoryName(breadCrumbs);


        return Jisp.getResult(this);
    };
})();