window.parser_hm_com = new (function () {
    Jisp.init(this);

    this.placeholderImageUrl = "https://jisp.com/Content/img/placeholders/hm.com.png";

    this.ready = function () {
        return true;
    };

    this.checkChanges = function () {
        return null;
    };

    this.getPageInfo = function () {

        this.isProductPage = Jisp.isExistsElement('.product-detail') || 
            ((document.location.hostname === 'm2.hm.com') && Jisp.isExistsElement('.product-detail-meta'));

        if (document.location.hostname === 'm2.hm.com') {
            if (this.isProductPage) {
                if (document.location.href) {
                    this.url = document.location.href.replace('m2.hm.com/m', 'www2.hm.com');
                }
            } else {
                this.url = document.location.href;
            }
        } else {
            if (!this.isProductPage) {
                this.url = document.location.href;
            }

            this.url = Jisp.ignoreSharpUrl(this.url);
            if (this.url.indexOf('/') === 0) {
                this.url = document.location.protocol + '//' + document.location.hostname + this.url;
            }
        }

        this.productName = Jisp.getInnerText('.product-detail .product-item-headline');
        
        if (!this.productName) {
            this.productName = Jisp.getMetaProperty(document, "og:title");
        }

        this.imageUrl = Jisp.getAttribute('.product-detail-main-image-image', 'src');
        if (!this.imageUrl) {
            this.imageUrl = Jisp.getMetaProperty(document, "og:image");
        }
        this.imageUrl = Jisp.addProtocolToUrl(this.imageUrl);

        var priceInnerText = Jisp.getInnerText('.price .price-value');
        this.priceAmount = Jisp.extractPrice(priceInnerText);
        this.priceCurrency = Jisp.extractCurrency(priceInnerText);

        this.brandNameText = 'H&M';

        var breadCrumbs = Jisp.getBreadCrumbs(document);
        this.categoryNameText = Jisp.extractCategoryName(breadCrumbs);

        return Jisp.getResult(this);
    };
})();