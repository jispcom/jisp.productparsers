window.parser_currys_com = new (function () {
    Jisp.init(this);

    this.ready = function () {
        return true;
    };

    this.checkChanges = function () {
        return null;
    };

    this.getPageInfo = function () {

        var product = Jisp.getProductElement();
        if (!product) {
            product = document;
        }

        this.productName = Jisp.getInnerText('.PCContentDesc');

        this.imageUrl = Jisp.getAttribute('.PCITableImage img', 'src');

        var priceInnerText = Jisp.getInnerText('.PCContentRow1 .PCContentYourPrc');
        this.priceAmount = Jisp.extractPrice(priceInnerText);
        var currencyInnerText = '£';
        if (!currencyInnerText) {
            currencyInnerText = Jisp.extractCurrency(Jisp.getCurrency(product));
        }
        this.priceCurrency = currencyInnerText;

        this.brandNameText = Jisp.getBrand(product);

        var breadCrumbs = Jisp.getInnerTextArr('.NavBarTable td.NavBarBar a');
        this.categoryNameText = Jisp.extractCategoryName(breadCrumbs);

        return Jisp.getResult(this);
    };
})();