window.parser_asos_com = new (function () {
    Jisp.init(this);

    this.placeholderImageUrl = "https://jisp.com/Content/img/placeholders/asos.com.png";

    this.ready = function () {
        var priceInnerText = Jisp.getInnerText('#asos-product #product-price [data-id="current-price"]');
        var productNameText = Jisp.getInnerText('#asos-product #aside-content h1');
        return !!priceInnerText && priceInnerText.length > 0 && (!!productNameText);
    };

    this.checkChanges = function () {
        return null;
    };

    this.getPageInfo = function() {

        this.isProductPage = Jisp.isExistsElement('#asos-product');

        this.productName = Jisp.getInnerText('#asos-product #aside-content h1');

        this.imageUrl = Jisp.getAttribute('#product-gallery .gallery-image', 'src');
        if (!this.imageUrl) {
            this.imageUrl = Jisp.getMetaProperty(document, "og:image");
        }

        if (this.imageUrl && this.imageUrl.indexOf('//') === 0) {
            this.imageUrl = document.location.protocol + '//' + this.imageUrl.substring(2);
        }

        var priceInnerText = Jisp.getInnerText('#asos-product #product-price [data-id="current-price"]');
        this.priceAmount = Jisp.extractPrice(priceInnerText);
        this.priceCurrency = Jisp.extractCurrency(priceInnerText);

        var brandInnerText = Jisp.getInnerText('.brand-description a');
        if (brandInnerText) {
            this.brandNameText = brandInnerText.trim();
        }

        var breadCrumbs = Jisp.getInnerTextArr('#breadcrumb li a', ['Home']);
        if (breadCrumbs != null) {
            var crumbs = breadCrumbs.map(function(name) {
                if ((name) && (name.indexOf('Search results for') === 0)) {
                    return null;
                }

                return name;
            });

            breadCrumbs = crumbs;
        }
        this.categoryNameText = Jisp.extractCategoryName(breadCrumbs);


        return Jisp.getResult(this);
    };
})();