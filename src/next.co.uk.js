window.parser_next_co_uk = new (function () {
    Jisp.init(this);

    this.placeholderImageUrl = "https://jisp.com/Content/img/placeholders/next.co.uk.png";

    this.ready = function () {
        var breadCrumbs = this.getBreadCrumbs(document);

        var priceInnerText = Jisp.getInnerText('.ProductDetail article.Selected .Price');
        if (!priceInnerText) {
            priceInnerText = this.getPrice(document);
        }
        return breadCrumbs.length > 0 && (!!priceInnerText);
    };

    this.checkChanges = function () {
        return null;
    };

    this.getPrice = function (product) {

        var prices = $('.Price', product);

        for (var i = 0; i < prices.length; i++) {

            if ($(prices[i]).parent().hasClass("StyleCopy")) {
                var priceTextFinded = prices[i].innerText;
                if (priceTextFinded) {
                    return priceTextFinded;
                }
            }  
        }

        var price = Jisp.getInnerText('.selectedPrice .price');
        if (price) {
            return price;
        }

        price = Jisp.getInnerText('[ng-app="modularApp"] .price strong');
        if (price) {
            return price;
        }

        return null;
    }

    this.getBreadCrumbs = function (product) {
        var breadCrumbAll = [];
        var breadCrumbs = $('a', $('ul.Breadcrumbs', $(".BreadcrumbNavigation", product)));

        for (var i = 0; i < breadCrumbs.length; i++) {
            var breadCrumbInner = breadCrumbs[i].innerText;

            if (breadCrumbInner) {
                breadCrumbInner = breadCrumbInner.trim();

                if ($(breadCrumbs[i]).parent().hasClass("bcHome")) {
                    continue;
                }

                //+2 filters
                if (breadCrumbInner.indexOf('+') == 0) {
                    continue;
                }

                if (breadCrumbInner.indexOf('Page ') < 0) {
                    breadCrumbAll.push(breadCrumbInner.replace(/"/g, ''));
                }
            }
        }

        return breadCrumbAll;
    }

    this.getPageInfo = function () {
        this.isProductPage = Jisp.isExistsElement('.ProductDetail');

        this.productName = Jisp.getInnerText('.ProductDetail article.Selected .Title h2');

        if (!this.productName) {
            this.productName = Jisp.getInnerText('.Title h1');
        }
        
        if (!this.productName) {
            this.productName = Jisp.getInnerText('.productDetails .details h2');
        }

        this.imageUrl = Jisp.getAttribute('#ShotView img', 'src');
        if (!this.imageUrl) {
            this.imageUrl = Jisp.getAttribute('.productDetails .images img', 'src');
        }

        var priceInnerText = Jisp.getInnerText('.ProductDetail article.Selected .Price');
        if (!priceInnerText) {
            priceInnerText = this.getPrice(document);
        }
        this.priceAmount = Jisp.extractPrice(priceInnerText);
        this.priceCurrency = Jisp.extractCurrency(priceInnerText);

        this.brandNameText = Jisp.getBrand(document);

        var breadCrumbs = this.getBreadCrumbs(document);
        this.categoryNameText = Jisp.extractCategoryName(breadCrumbs);

        return Jisp.getResult(this);
    };
})();