window.parser_houseoffraser_co_uk = new (function () {
    Jisp.init(this);

    this.placeholderImageUrl = "https://jisp.com/Content/img/placeholders/houseoffraser.co.uk.png";

    this.ready = function () {
        return true;
    };

    this.checkChanges = function () {
        return null;
    };

    this.getBreadCrumbs = function (product) {
        var breadCrumbAll = [];
        var breadCrumbs = $('.hof-breadcrumbs [itemprop="breadcrumb"]', product);

        for (var i = 0; i < breadCrumbs.length; i++) {
            if ((breadCrumbs[i].id == 'back-to-results-breadcrumb') || (breadCrumbs[i].id == 'homepage-breadcrumb')) {
                continue;
            }

            breadCrumbAll.push(breadCrumbs[i].innerText);
        }

        return breadCrumbAll;
    }

    this.getPageInfo = function () {

        var product = Jisp.getProductElement();
        if (product) {

            this.productName = Jisp.getMetaProperty(document, "og:title");

            var img = document.querySelectorAll('[itemtype="http://schema.org/Product"] .main-carousel .magnifier, [itemtype="https://schema.org/Product"] .main-carousel .magnifier')[0];
            if (img) {
                var style = window.getComputedStyle(img);
                var imgUrl = style.getPropertyValue('background');
                if (imgUrl) {
                    imgUrl = imgUrl.trim();
                    var start = imgUrl.indexOf('url("');
                    if (start > 0) {
                        imgUrl = imgUrl.substr(start + 5);
                        var end = imgUrl.indexOf('")');
                        if (end > 0) {
                            imgUrl = imgUrl.substr(0, end);
                            if (imgUrl) {
                                this.imageUrl = imgUrl;
                            }
                        }
                    }
                }
            }

            if (!this.imageUrl) {
                this.imageUrl = Jisp.getAttribute('[itemtype="http://schema.org/Product"] .main-carousel .featuredProductImage', 'src');
            }

            if (!this.imageUrl) {
                this.imageUrl = Jisp.getAttribute('[itemtype="https://schema.org/Product"] .main-carousel .featuredProductImage', 'src');
            }

            if (!this.imageUrl) {
                this.imageUrl = Jisp.getMetaProperty(document, "og:image");
            }

            var priceInnerText = Jisp.getAttribute('[itemprop="price"]', 'content');
            this.priceAmount = Jisp.extractPrice(priceInnerText);
            var currencyInnerText = Jisp.getAttribute('[itemprop="priceCurrency"]', 'content');
            this.priceCurrency = Jisp.extractCurrency(currencyInnerText);

            this.brandNameText = Jisp.getInnerText('.hof-breadcrumbs [itemprop="breadcrumb"]');

            var breadCrumbs = this.getBreadCrumbs(document);
            this.categoryNameText = Jisp.extractCategoryName(breadCrumbs);
        }
        else {
            this.except = true;
        }

        return Jisp.getResult(this);
    };
})();