var gulp = require('gulp'),
    concat = require('gulp-concat'),
    rimraf = require('rimraf'),
    streamqueue  = require('streamqueue');

function concatProductParsers() {
    rimraf.sync('./dist/jisp.productparsers.js');

    return streamqueue({ objectMode: true },
        gulp.src('./src/parserUtils.js'),
        gulp.src(['./src/*.js', '!./src/parserSelector.js', '!./src/parserUtils.js']),
        gulp.src('./src/parserSelector.js'))
        .pipe(concat('jisp.productparsers.js'))
        .pipe(gulp.dest('./dist/'));
}

gulp.task('buildScripts', function () {
    return concatProductParsers();
});